-- 1. Составьте список пользователей users, которые осуществили хотя бы один заказ orders в интернет магазине.

use Example;

DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS users;

create table users(
	id int unsigned not null auto_increment,
    name varchar(100) not null,
    PRIMARY KEY (id)
);

create table orders(
	id int unsigned not null auto_increment,
    user_id int unsigned not null,
    crated_at datetime not null default now(),
	updated_at datetime not null default now() ON UPDATE now(),
    PRIMARY KEY (id),
    KEY `fk_orders_user_id_idx` (user_id),
    CONSTRAINT `fk_orders_users` FOREIGN KEY (user_id) REFERENCES users (id)
);

insert into users (id, name)
values (1, 'Ivan'), (2, 'Petr'), (3, 'Baku');

insert into orders (user_id)
value (1), (2);

select *
from users u
left join orders o on u.id = o.user_id
where o.id IS NOT NULL;

DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS users;

-- 2. Выведите список товаров products и разделов catalogs, который соответствует товару.

drop table if exists products;
drop table if exists catalogs;

create table catalogs (
	id int unsigned not null auto_increment,
    name varchar(100) not null,
    PRIMARY KEY (id)
);

create table products (
	id int unsigned not null auto_increment,
    catalog_id int unsigned not null,
    name varchar(100) not null,
    PRIMARY KEY (id),
    KEY `fk_products_catalog_id_idx` (catalog_id),
    CONSTRAINT `fk_catalogs_products` FOREIGN KEY (catalog_id) REFERENCES catalogs (id)
);

insert catalogs (name) values ('aaa'), ('bbb');
insert products (name, catalog_id) values ('xxx', 1), ('zzz', 2);

select *
from catalogs c
join products p ON c.id = p.id;

drop table if exists products;
drop table if exists catalogs;

-- 3. (по желанию) Пусть имеется таблица рейсов flights (id, from, to) и таблица городов cities (label, name). Поля from, to и label содержат английские названия городов, поле name — русское. Выведите список рейсов flights с русскими названиями городов.

drop table if exists flights;
drop table if exists cities;


create table flights (
	id int unsigned not null auto_increment,
    `from` varchar(100) not null,
    `to` varchar(100) not null,
    PRIMARY KEY (id)
);

create table cities (
	label varchar(100) not null,
    `name` varchar(100) not null,
    PRIMARY KEY (label)
);

insert into flights (`from`,`to`)
value
('moscow', 'omsk')
, ('novgorod', 'kazan')
, ('irkutsk', 'moscow')
, ('moscow', 'omsk')
, ('omsk', 'irkutsk')
, ('moscow', 'kazan');

insert into cities (label, name)
values
('moscow', 'Москва')
,('irkutsk', 'Иркутск')
,('novgorod', 'Новгород')
,('kazan', 'Казань')
,('omsk', 'Омск');

select f.id, c_from.name, c_to.name
from flights f
join cities c_from on f.from = c_from.label
join cities c_to on f.to = c_to.label;


drop table if exists flights;
drop table if exists cities;
