USE vk2;

-- EX 1 Пусть в таблице users поля created_at и updated_at оказались незаполненными. Заполните их текущими датой и временем.
-- EX 2 Таблица users была неудачно спроектирована. Записи created_at и updated_at были заданы типом VARCHAR и в них долгое время помещались значения в формате 20.10.2017 8:10. Необходимо преобразовать поля к типу DATETIME, сохранив введённые ранее значения.

-- create bad table
DROP TABLE IF EXISTS temp_users;

CREATE TEMPORARY TABLE temp_users
SELECT id, first_name, last_name, email, phone
, DATE_FORMAT(created_at, '%d.%m.%Y %k:%i') created_at, DATE_FORMAT(updated_at, '%d.%m.%Y %k:%i') updated_at
FROM users;

select * from temp_users;

-- save old data to temp table
DROP TABLE IF EXISTS temp_users_old_data;

CREATE TEMPORARY TABLE temp_users_old_data
SELECT id, created_at, updated_at
FROM temp_users;

-- modify columns type
-- UPDATE temp_users SET created_at = NULL, updated_at = NULL;
ALTER TABLE temp_users DROP COLUMN created_at;
ALTER TABLE temp_users DROP COLUMN updated_at;
ALTER TABLE temp_users ADD COLUMN created_at DATETIME NOT NULL DEFAULT NOW();
ALTER TABLE temp_users ADD COLUMN updated_at DATETIME NOT NULL DEFAULT NOW();

UPDATE temp_users tu
JOIN temp_users_old_data tuod ON tu.id = tuod.id
SET tu.created_at = STR_TO_DATE(tuod.created_at,'%d.%m.%Y %k:%i')
, tu.updated_at = STR_TO_DATE(tuod.updated_at,'%d.%m.%Y %k:%i');

SELECT * FROM temp_users;

DROP TABLE IF EXISTS temp_users_old_data;

-- EX 3 В таблице складских запасов storehouses_products в поле value могут встречаться самые разные цифры: 0, если товар закончился и выше нуля, если на складе имеются запасы. Необходимо отсортировать записи таким образом, чтобы они выводились в порядке увеличения значения value. Однако нулевые запасы должны выводиться в конце, после всех
DROP TABLE IF EXISTS storehouses_products;
CREATE TEMPORARY TABLE storehouses_products (value int);

INSERT INTO storehouses_products
VALUES
(0)
,(0)
,(ROUND(rand()*1000))
,(ROUND(rand()*1000))
,(0)
,(ROUND(rand()*1000))
,(ROUND(rand()*1000))
,(0)
,(ROUND(rand()*1000))
,(ROUND(rand()*1000))
,(ROUND(rand()*1000));

SET @my_var = (SELECT MAX(value)+1 FROM storehouses_products);

SELECT *
FROM storehouses_products
ORDER BY CASE WHEN value = 0 THEN @my_var ELSE value END;

DROP TABLE IF EXISTS storehouses_products;

-- ex 4 (по желанию) Из таблицы users необходимо извлечь пользователей, родившихся в августе и мае. Месяцы заданы в виде списка английских названий (may, august)

SELECT *
FROM users u
JOIN profiles p ON u.id = p.user_id
WHERE DATE_FORMAT(p.birthday, '%M') IN ('May', 'August');

-- ex 5 (по желанию) Из таблицы catalogs извлекаются записи при помощи запроса. SELECT * FROM catalogs WHERE id IN (5, 1, 2); Отсортируйте записи в порядке, заданном в списке IN.

SELECT * FROM users WHERE id IN (5,1,2) ORDER BY FIELD(id, 5,1,2);

-- ex 6 Подсчитайте средний возраст пользователей в таблице users.
SELECT ROUND(AVG(TIMESTAMPDIFF(YEAR, p.birthday, now())))
FROM users u
JOIN profiles p ON u.id = p.user_id;

-- ex 7 Подсчитайте количество дней рождения, которые приходятся на каждый из дней недели. Следует учесть, что необходимы дни недели текущего года, а не года рождения.
SELECT DATE_FORMAT(p.birthday, '%W'), COUNT(*)
FROM users u
JOIN profiles p ON u.id = p.user_id
GROUP BY DATE_FORMAT(p.birthday, '%W');

-- ex 8 (по желанию) Подсчитайте произведение чисел в столбце таблицы.
DROP TABLE IF EXISTS temp_values;
CREATE TEMPORARY TABLE temp_values (value INT);

INSERT INTO temp_values(value)
VALUES (1),(2),(3),(4),(5);

SELECT ROUND(exp(SUM(log(value)))) product
FROM temp_values;

DROP TABLE IF EXISTS temp_values;
