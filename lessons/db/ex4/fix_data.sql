update community_history set is_active = 1;
update chat_history set is_active = 1;
update message_history set is_active = 1;

-- group users by chats
update messages set chat_id = case
	when chat_id%2=0 then 2
    when chat_id%3=0 then 3
    when chat_id%4=0 then 4
    when chat_id%5=0 then 5
    when chat_id%6=0 then 6
    when chat_id%7=0 then 7
    when chat_id%8=0 then 8
    when chat_id%9=0 then 9
    when chat_id%1=0 then 1
    else 1
    end;


-- clear empty chats
delete ch
from chat_history ch
join chats c on c.id = ch.chat_id
left join messages m ON c.id = m.chat_id
where m.id is null;

delete c
from chats c
left join chat_history ch ON c.id = ch.chat_id
where ch.id is null;

-- fill metadata
update media m
join users u on m.user_id = u.id
set m.metadata = CONCAT('{ "author": "', u.first_name, '" }');

-- fix friends
update friendship set friend_id = (FLOOR( 1 + RAND( ) * (select COUNT(*) from users) ));
update friendship
set friend_id = CASE
	WHEN friend_id = user_id
    THEN FLOOR( 1 + RAND( ) * (select COUNT(*) from users) )
    ELSE friend_id
    END;

-- fix friendship statuses
update friendship_statuses set name = 'friend' where id = 1;
update friendship_statuses set name = 'enemy' where id = 2;
update friendship_statuses set name = 'familiar' where id = 3;
update friendship_statuses set name = 'lover' where id = 4;
update friendship_statuses set name = 'married' where id = 5;

update friendship set status_id = (FLOOR( 1 + RAND( ) *5 ));
delete from friendship_statuses where id > 5;
