use motions2;

-- video statistics view
drop view if exists video_statistics;
create view video_statistics as
select v.id, v.name, v.description, v.storage_link, v.channel_id
	,count(distinct vl.id) likes_count
    ,count(distinct vc.id) comments_count
    ,count(distinct vb.id) bookmarks_count
    ,count(distinct vh.id) views_count
from videos v
left join video_likes vl on v.id = vl.video_id
left join video_comments vc on v.id = vc.video_id
left join video_bookmarks vb on v.id = vb.video_id
left join view_history vh on v.id = vh.video_id
group by v.id, v.name, v.description, v.storage_link, v.channel_id;

select * from video_statistics;

-- channel statistics view
drop view if exists channel_statistics;
create view channel_statistics as
select ch.id, ch.name, ch.description, ch.author_id
	,count(distinct uch.id) follower_count
    ,count(distinct v.id) video_count
from channels ch
left join users_channels uch on ch.id = uch.channel_id and uch.type = 'Подписчик'
left join videos v on ch.id = v.channel_id
group by ch.id, ch.name, ch.description, ch.author_id;

select * from channel_statistics;


