DELIMITER //
-- drop trigger user_register;
create trigger user_register after insert on users for each row
begin

    insert into users_log(event_type, message, user_id)
    value ('Вход','Регистрация пользователя и автоматический вход' , NEW.id);

end//


-- drop procedure if exists generate_likes//
create procedure generate_likes(in need_count int, in video_id int)
begin

	set @i := 0;
	while @i < need_count
    do
		insert into video_likes (user_id, video_id)
        values (1, video_id);

        set @i := @i + 1;
	end while;
end//

DELIMITER ;

call generate_likes(100, 1);


