use motions2;

CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'email',
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Пароль',
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Имя',
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Фамилия',
  `birthday` date NOT NULL COMMENT 'Дата рождения',
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Номер телефона',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`),
  KEY `birthday` (`birthday`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Пользователи';

CREATE TABLE `channels` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Наименование',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Описание',
  `author_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `FKchannels159993` (`author_id`),
  CONSTRAINT `FKchannels159993` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Каналы';

CREATE TABLE `users_channels` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `channel_id` int unsigned NOT NULL COMMENT 'ИД канала',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  `type` enum('Подписчик','Модератор','Администратор') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Тип связи канала с пользователем',
  PRIMARY KEY (`id`),
  KEY `FKusers_chan217041` (`channel_id`),
  KEY `FKusers_chan588114` (`user_id`),
  CONSTRAINT `FKusers_chan217041` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`),
  CONSTRAINT `FKusers_chan588114` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Пользователи каналов';

CREATE TABLE `users_log` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `event_type` enum('Регистрация','Вход','Выход','Восстановление пароля') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Тип лога',
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Сообщение',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `FKusers_log824512` (`user_id`),
  CONSTRAINT `FKusers_log824512` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Таблица событий, совершаемых пользователем.';

CREATE TABLE `videos` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Название видео',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Описание видео',
  `storage_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Ссылка на видео',
  `channel_id` int unsigned NOT NULL COMMENT 'ИД канала',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `FKvideos858760` (`channel_id`),
  CONSTRAINT `FKvideos858760` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Таблица видеофайлов';


CREATE TABLE `video_bookmarks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `video_id` int unsigned NOT NULL COMMENT 'ИД видео',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `FKvideo_book210307` (`video_id`),
  KEY `FKvideo_book53775` (`user_id`),
  CONSTRAINT `FKvideo_book210307` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`),
  CONSTRAINT `FKvideo_book53775` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Закладки видео';

CREATE TABLE `video_comments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `video_id` int unsigned NOT NULL COMMENT 'ИД видео',
  `message` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Комментарий',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `FKvideo_comm360510` (`video_id`),
  KEY `FKvideo_comm346998` (`user_id`),
  CONSTRAINT `FKvideo_comm346998` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKvideo_comm360510` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Комментарии к видео';

CREATE TABLE `video_likes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `video_id` int unsigned NOT NULL COMMENT 'ИД видео',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `FKvideo_like539451` (`video_id`),
  KEY `FKvideo_like275369` (`user_id`),
  CONSTRAINT `FKvideo_like275369` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKvideo_like539451` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Лайки видео';

CREATE TABLE `video_shared_links` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Ссылка на просмотр видео',
  `expiried_at` datetime DEFAULT NULL COMMENT 'Дата и время протухания ссылки',
  `video_id` int unsigned NOT NULL COMMENT 'ИД видео',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `FKvideo_shar29124` (`video_id`),
  CONSTRAINT `FKvideo_shar29124` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Короткие ссылки на просмотр видео';

CREATE TABLE `view_history` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `video_id` int unsigned NOT NULL COMMENT 'ИД видео',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  KEY `FKview_histo280640` (`video_id`),
  KEY `FKview_histo544722` (`user_id`),
  CONSTRAINT `FKview_histo280640` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`),
  CONSTRAINT `FKview_histo544722` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='История просмотров';


