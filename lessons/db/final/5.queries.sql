use motions2;

-- search users
set @email := 'blabla@bla.ru';
set @phone := '89263564412';

select *
from users u
where u.email = @email or u.phone = @phone;

-- get video comment
set @video_id := 12;

select v.id, v.name, vc.*
from videos v
join video_comments vc on v.id = vc.video_id
where v.id = @video_id;

-- get channels by user
set @user_id := 11;

select *
from channels ch
where ch.author_id = @user_id;

-- get user last login
set @user_id := 2;

select @user_id, max(ul.created_at) last_login
from users_log ul
where ul.user_id = @user_id and ul.event_type = 'Вход';

