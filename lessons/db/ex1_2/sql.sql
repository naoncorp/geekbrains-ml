-- 1. Установите СУБД MySQL. Создайте в домашней директории файл .my.cnf, задав в нем логин и пароль, который указывался при установке.
-- nano .my.cnf

-- 2. Создайте базу данных example, разместите в ней таблицу users, состоящую из двух столбцов, числового id и строкового name.
CREATE DATABASE IF NOT EXISTS Example;

USE Example;

DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
	Id SERIAL PRIMARY KEY,
    Name VARCHAR(255) NOT NULL COMMENT 'User login'
) COMMENT = 'Users';


-- 3. Создайте дамп базы данных example из предыдущего задания, разверните содержимое дампа в новую базу данных sample.
-- mysqldump Example > Example.sql
-- mysql -p Sample < Example.sql

-- 4. (по желанию) Ознакомьтесь более подробно с документацией утилиты mysqldump. Создайте дамп единственной таблицы help_keyword базы данных mysql. Причем добейтесь того, чтобы дамп содержал только первые 100 строк таблицы.
-- mysqldump mysql help_keyword --where="true limit 100" > mysql.sql