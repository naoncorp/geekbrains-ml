
-- 1. Создать все необходимые внешние ключи и диаграмму отношений.

-- attached screenshot

-- 2. Создать и заполнить таблицы лайков и постов.
USE vk2;

DROP TABLE IF EXISTS posts_likes;
DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS walls;

CREATE TABLE walls (
	id int unsigned not null auto_increment comment 'ИД',
    user_id int unsigned not null comment 'Чья стена',
    crated_at datetime not null default now() comment 'Дата создания',
	updated_at datetime not null default now() ON UPDATE now() comment 'Дата изменения',
    PRIMARY KEY (id),
    KEY `fk_walls_user_id_idx` (user_id),
    CONSTRAINT `fk_walls_users` FOREIGN KEY (user_id) REFERENCES users (id)
) COMMENT='Стены';

INSERT INTO walls (user_id)
select u.id
from users u;

CREATE TABLE posts (
  id int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  wall_id int unsigned not null comment 'ИД стены',
  message text NOT NULL COMMENT 'Текст поста',
  creator_id int unsigned NOT NULL COMMENT 'Создатель',
  crated_at datetime not null default now() comment 'Дата создания',
  updated_at datetime not null default now() ON UPDATE now() comment 'Дата изменения',
  PRIMARY KEY (id),
  KEY `fk_posts_creator_id_idx` (creator_id),
  CONSTRAINT `fk_posts_users` FOREIGN KEY (creator_id) REFERENCES users (id)
) COMMENT='Посты';

insert into posts (wall_id, message, creator_id)
select w.id, 'my test text', u.id
from walls w
join users u on w.user_id = u.id;

insert into posts (wall_id, message, creator_id)
select w.id, 'my test text 2', u.id
from walls w
join users u on w.user_id = u.id;

-- решил создавать отдельную таблицу для каждых типов лайков, так будет производительней

CREATE TABLE posts_likes (
	id int unsigned not null auto_increment comment 'ИД',
    post_id int unsigned not null comment 'ИД поста',
    creator_id int unsigned not null comment 'Кто лайкнул',
    crated_at datetime not null default now() comment 'Дата создания',
    PRIMARY KEY (id),
	KEY `fk_posts_likes_creator_id_idx` (creator_id),
    KEY `fk_posts_likes_post_id_idx` (post_id),
	CONSTRAINT `fk_posts_likes_users` FOREIGN KEY (creator_id) REFERENCES users (id),
    CONSTRAINT `fk_posts_likes_posts` FOREIGN KEY (post_id) REFERENCES posts (id)
) COMMENT='Лайки к постам';

insert into posts_likes (post_id, creator_id)
select (select id from posts order by rand() limit 1), (select id from users order by rand() limit 1)
from users;

insert into posts_likes (post_id, creator_id)
select (select id from posts order by rand() limit 1), (select id from users order by rand() limit 1)
from users;

insert into posts_likes (post_id, creator_id)
select (select id from posts order by rand() limit 1), (select id from users order by rand() limit 1)
from users;

-- не стал в рамках данного задания расширять посты медифайлами и лайками к ним
-- CREATE TABLE posts_medias
-- CREATE TABLE posts_medias_likes

-- 3. Определить кто больше поставил лайков (всего) - мужчины или женщины?
select p.gender, count(*)
from posts_likes pl
join profiles p ON pl.creator_id = p.id
group by p.gender;

-- 4. Подсчитать общее количество лайков десяти самым молодым пользователям (сколько лайков получили 10 самых молодых пользователей).
select u.id, count(*)
from users u
join walls w on u.id = w.user_id
join posts p on w.id = p.wall_id
join posts_likes pl on pl.post_id = p.id
where
u.id in
(
	select * from (
		select u.id
		from profiles p
		order by p.birthday
		limit 10
    ) as t
)
group by u.id
order by 2 DESC;

-- 5. Найти 10 пользователей, которые проявляют наименьшую активность в использовании социальной сети
select u.id,
(
	select count(*)
    from message_history mh
    where mh.is_active = 1 and mh.id_user = u.id
) +
(
	select count(*)
    from posts p
    where p.creator_id = u.id
) +
(
	select count(*)
    from posts_likes pl
    where pl.creator_id = u.id
) as activity
from users u
order by 2
limit 10;

