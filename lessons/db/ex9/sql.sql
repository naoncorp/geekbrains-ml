-- Практическое задание по теме “Транзакции, переменные, представления”

-- 1.  В базе данных shop и sample присутствуют одни и те же таблицы, учебной базы данных.
-- Переместите запись id = 1 из таблицы shop.users в таблицу sample.users. Используйте транзакции.

drop database if exists shop;
create database shop;

use shop;

create table users(
	id int unsigned not null
);

insert into users (id) value (1),(2),(3);

drop database if exists sample;
create database sample;

use sample;

create table users(
	id int unsigned not null
);

start transaction;

insert into sample.users (id)
select id
from shop.users
where id = 1;

select * from shop.users;
select * from sample.users;

commit;

-- 2. Создайте представление, которое выводит название name товарной позиции из таблицы products
-- и соответствующее название каталога name из таблицы catalogs.


drop table if exists products;
drop table if exists catalogs;

create table catalogs (
	id int unsigned not null auto_increment,
    name varchar(100) not null,
    PRIMARY KEY (id)
);

create table products (
	id int unsigned not null auto_increment,
    catalog_id int unsigned not null,
    name varchar(100) not null,
    PRIMARY KEY (id),
    KEY `fk_products_catalog_id_idx` (catalog_id),
    CONSTRAINT `fk_catalogs_products` FOREIGN KEY (catalog_id) REFERENCES catalogs (id)
);

insert catalogs (name) values ('aaa'), ('bbb');
insert products (name, catalog_id) values ('xxx', 1), ('zzz', 2);


CREATE OR REPLACE VIEW v1 AS
SELECT p.name 'product', c.name 'catalog'
FROM products p
join catalogs c on p.catalog_id = c.id;

select * from v1;

-- 3. (по желанию) Пусть имеется таблица с календарным полем created_at.
-- В ней размещены разряженые календарные записи за август 2018 года '2018-08-01', '2016-08-04', '2018-08-16' и 2018-08-17.
-- Составьте запрос, который выводит полный список дат за август, выставляя в соседнем поле значение 1,
-- если дата присутствует в исходном таблице и 0, если она отсутствует.

drop table if exists dates;
create table dates(
	created_at DATE
);

insert into dates (created_at) values ('2018-08-01'), ('2018-08-04'), ('2018-08-16'), ('2018-08-17');

drop table if exists all_dates;
create table all_dates (dt DATE);

DELIMITER //
create procedure fillTempTable()
begin
	declare currDt, endDt date;

    set @currDt := '2018-08-01';
    set @endDt := '2018-09-01';

	while @currDt < @endDt
    do
		insert into all_dates(dt) values (@currDt);
		set @currDt :=  date_add(@currDt,interval 1 day);
	end while;
end//

DELIMITER ;

CALL fillTempTable();

select ad.dt, case when d.created_at is null then 0 else 1 end
from all_dates ad
left join dates d on ad.dt = d.created_at;

-- 4. (по желанию) Пусть имеется любая таблица с календарным полем created_at.
-- Создайте запрос, который удаляет устаревшие записи из таблицы, оставляя только 5 самых свежих записей.

start transaction;

set @min_val := (select dt from (select dt from all_dates order by dt desc limit 5) as t order by dt limit 1);
select @min_val;

delete from all_dates
where dt < @min_val;

select * from all_dates;

rollback;

-- Практическое задание по теме “Хранимые процедуры и функции, триггеры"

-- Создайте хранимую функцию hello(), которая будет возвращать приветствие, в зависимости от текущего времени суток.
-- С 6:00 до 12:00 функция должна возвращать фразу "Доброе утро",
-- с 12:00 до 18:00 функция должна возвращать фразу "Добрый день", с 18:00 до 00:00 — "Добрый вечер",
-- с 00:00 до 6:00 — "Доброй ночи".

DELIMITER //
create procedure hello()
begin
	if(hour(now()) between 6 and 12) then
		select 'Good morning';
	elseif(hour(now()) between 12 and 18) then
		select 'Good day';
	else
		select 'Good afternoon';
	end if;
end//

DELIMITER ;
call hello();

-- 2. В таблице products есть два текстовых поля: name с названием товара и description с его описанием.
-- Допустимо присутствие обоих полей или одно из них.
-- Ситуация, когда оба поля принимают неопределенное значение NULL неприемлема.
-- Используя триггеры, добейтесь того, чтобы одно из этих полей или оба поля были заполнены.
-- При попытке присвоить полям NULL-значение необходимо отменить операцию.

drop table if exists products2;
create table products2(
	name varchar(100),
    description varchar(100)
);

DELIMITER //
create trigger insert_check_products2 before insert on products2
for each row
begin
	if (NEW.name is null and NEW.description is null) then
		signal sqlstate '45000' set message_text = 'error';
	end if;
end//

create trigger update_check_products2 before update on products2
for each row
begin
	if (
			(NEW.name is null and OLD.description is null)
			or
			(NEW.description is null and OLD.name is null)
        ) then
		signal sqlstate '45000' set message_text = 'error';
	end if;
end//
DELIMITER ;

start transaction;
insert into products2 (name, description) value ('hello', null);
-- insert into products2 (name, description) value (null, null); -- error
-- update products2 set name = null where name = 'hello'; -- error
rollback;

-- 3. (по желанию) Напишите хранимую функцию для вычисления произвольного числа Фибоначчи.
-- Числами Фибоначчи называется последовательность в которой число равно сумме двух предыдущих чисел.
-- Вызов функции FIBONACCI(10) должен возвращать число 55.

DELIMITER //

create function fibonacci(num int)
returns int DETERMINISTIC
begin

set @i := 1;
set @v := 0;
set @res := 1;

if (num = @v) then
	return @v;
end if;

if (num = @res) then
	return @res;
end if;

while @i < num
do
	set @prev_v = @res;
	set @res = @v + @res;
    set @v = @prev_v;
    set @i = @i+1;
end while;

return @res;

end//

DELIMITER ;

select fibonacci(10); -- 55

-- Практическое задание по теме “Администрирование MySQL” (эта тема изучается по вашему желанию)

-- Создайте двух пользователей которые имеют доступ к базе данных shop.
-- Первому пользователю shop_read должны быть доступны только запросы на чтение данных,
-- торому пользователю shop — любые операции в пределах базы данных shop.

drop user if exists usr1;
drop user if exists usr2;
CREATE USER usr1 IDENTIFIED WITH sha256_password BY 'Passadsadasd_1';
CREATE USER usr2 IDENTIFIED WITH sha256_password BY 'Passsadsada_2';


GRANT SELECT ON *.* TO usr1;
GRANT ALL ON *.* TO usr2;
GRANT GRANT OPTION ON *.* TO usr2;

show grants;

-- (по желанию) Пусть имеется таблица accounts содержащая три столбца id, name, password,
-- содержащие первичный ключ, имя пользователя и его пароль. Создайте представление username таблицы accounts,
-- предоставляющий доступ к столбца id и name. Создайте пользователя user_read,
-- который бы не имел доступа к таблице accounts, однако, мог бы извлекать записи из представления username.

drop table if exists account;
create table account (
	id int,
    name varchar(100),
    password varchar(100)
);

insert into account (id, name, password) value (1, 'Petr', 'sadasfsfd');

create view v_account as select id, name from account;

select * from account;
select * from v_account;

drop user if exists usr_read;
CREATE USER usr_read IDENTIFIED WITH sha256_password BY 'Passadsadasd_1';

GRANT SELECT ON v_account TO usr_read;

select * from mysql.user;

-- я тут умер - делать такую большую домашку)