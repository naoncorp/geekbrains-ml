use vk2;

-- Создайте таблицу logs типа Archive. Пусть при каждом создании записи в таблицах users, catalogs и products в таблицу logs помещается время и дата создания записи, название таблицы, идентификатор первичного ключа и содержимое поля name.

drop table if exists logs;
create table logs(
    created_at datetime not null default now(),
    table_name nvarchar(100) not null,
    id int unsigned not null,
    name varchar(100) not null
)
engine Archive;


DELIMITER //

drop trigger user_to_log;
create trigger user_to_log after insert on users for each row
begin

    insert into logs(table_name, id, name)
    value ('users', NEW.id, NEW.first_name);

end//

drop trigger media_to_log;
create trigger media_to_log after insert on media for each row
begin

    insert into logs(table_name, id, name)
    value ('media', NEW.id, NEW.filename);

end//

drop trigger communities_to_log;
create trigger communities_to_log after insert on communities for each row
begin

    insert into logs(table_name, id, name)
    value ('communities', NEW.id, NEW.name);

end//

DELIMITER ;


-- (по желанию) Создайте SQL-запрос, который помещает в таблицу users миллион записей.

drop table if exists tmp2_users;
create temporary table tmp2_users(
	id int unsigned not null auto_increment,
    name varchar(100) not null,
    primary key(id)
);


DELIMITER //
drop procedure if exists fill_temp_users//
create procedure fill_temp_users()
begin
	set @need := 1000000;
	set @i := 0;
	while @i < @need
    do
		insert into	tmp2_users(name)
        select CONV(FLOOR(RAND() * 99999999999999), 10, 36); -- rand string
        set @i := @i + 1;
	end while;
end//

DELIMITER ;

call fill_temp_users();




