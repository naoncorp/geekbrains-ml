use vk2;

-- 1. Проанализировать какие запросы могут выполняться наиболее
-- часто в процессе работы приложения и добавить необходимые индексы.

-- Приложению свойственно изменяться и зачастую процесс обслуживания индексов в БД имеет периодический характер
-- , а не только на этапе проектирования БД, но сейчас можно предположительно определить по каким полям будет производится поиск.
-- так же мы автоматически получем много индексов в полях внешних ключей по умолчанию

-- ТАБЛИЦА USERS
-- в данном таблице уже есть пару уникальных индексов (UNIQUE KEY) по полям phone & email, которые предположительно будут
-- использоваться при регистрации или входе пользователя в систему

-- ТАБЛИЦА PROFILES
-- Индекс для функционала поиска людей по соц сети
CREATE INDEX idx_search_profiles ON profiles(gender, birthday);

-- ТАБЛИЦЫ MESSAGES & POSTS
-- тут необходимо ограничить длину поля body, а потом добавить индекс. В противном случае придется либо искать без индекс
-- либо искать полнотекстовые решения, либо создавать индекс с ограничем по длине
-- CREATE INDEX idx_search_messages ON messages(body);
-- CREATE INDEX idx_search_posts ON posts(message);

-- ТАБЛИЦА FRIENDSHIP_STATUSES
CREATE INDEX idx_search_friendship_statuses ON friendship_statuses(name);

-- ТАБЛИЦА COUNTRIES
CREATE INDEX idx_search_countries ON countries(name);

-- ТАБЛИЦА COMMUNITIES
CREATE INDEX idx_search_communities ON communities(name);







-- 2. Задание на оконные функции
-- Построить запрос, который будет выводить следующие столбцы:
-- имя группы
-- среднее количество пользователей в группах
-- самый молодой пользователь в группе
-- самый старший пользователь в группе
-- общее количество пользователей в группе
-- всего пользователей в системе
-- отношение в процентах (общее количество пользователей в группе / всего пользователей в системе) * 100

drop table if exists tmp_users_groups;
drop table if exists tmp_users;
drop table if exists tmp_groups;

create table tmp_users(
	id int unsigned not null auto_increment,
    birthday date not null,
    PRIMARY KEY (`id`)
);

create table tmp_groups(
	id int unsigned not null auto_increment,
    name varchar(100) not null,
    PRIMARY KEY (`id`)
);

create table tmp_users_groups(
	id int unsigned not null auto_increment,
    user_id int unsigned not null,
    group_id int unsigned not null,
    PRIMARY KEY (`id`),
    KEY `fk_tmp_users_groups_idx1` (`user_id`),
    KEY `fk_tmp_users_groups_idx2` (`group_id`),
    CONSTRAINT `fk_tmp_users_groups_usr` FOREIGN KEY (`user_id`) REFERENCES `tmp_users` (`id`),
	CONSTRAINT `fk_tmp_users_groups_grp` FOREIGN KEY (`group_id`) REFERENCES `tmp_groups` (`id`)
);


insert into tmp_groups(name)
values ('aaa'), ('bbb'), ('ccc');

insert into tmp_users(birthday)
select birthday from profiles;

insert into tmp_groups(name)
values ('aaa'), ('bbb'), ('ccc');

insert into tmp_users_groups (users_id, group_id)
select id, 1
from tmp_users
order by rand()
limit 10;

insert into tmp_users_groups (users_id, group_id)
select id, 1
from tmp_users
order by rand()
limit 25;

insert into tmp_users_groups (users_id, group_id)
select id, 1
from tmp_users
order by rand()
limit 30;


  select distinct tg.name,

        (dense_rank() over (order by tu.id) + dense_rank() over (order by tu.id desc) - 1) / (dense_rank() over (order by tg.id) + dense_rank() over (order by tg.id desc) - 1)
        first_value(tu.id) over (partition by tg.id order by tu.birthday) as young,
        first_value(tu.id) over (partition by tg.id order by tu.birthday desc) as old,
        count(*) over (partition by tg.id) as ct,
        (dense_rank() over (order by tu.id) + dense_rank() over (order by tu.id desc) - 1),
        count(*) over (partition by tg.id) / (dense_rank() over (order by tu.id) + dense_rank() over (order by tu.id desc) - 1) * 100
    from tmp_groups tg
    join tmp_users_groups tug on tg.id = tug.group_id
    join tmp_users tu on tug.user_id = tu.id;



-- 3. (по желанию) Задание на денормализацию
-- Разобраться как построен и работает следующий запрос:
-- Найти 10 пользователей, которые проявляют наименьшую активность
-- в использовании социальной сети.

SELECT users.id,
COUNT(DISTINCT messages.id) +
COUNT(DISTINCT likes.id) +
COUNT(DISTINCT media.id) AS activity
FROM users
LEFT JOIN messages ON users.id = messages.from_user_id
LEFT JOIN likes ON users.id = likes.user_id
LEFT JOIN media ON users.id = media.user_id
GROUP BY users.id
ORDER BY activity
LIMIT 10;

-- Правильно-ли он построен?
-- Какие изменения, включая денормализацию, можно внести в структуру БД
-- чтобы существенно повысить скорость работы этого запроса?

-- 0. по поводу правильности запроса точно пока сказать не могу, так как прям щас не могу его запустить и проверить, но в целом сама по себе операция группировки может выполняться довольно долго на сервере. Происходит это потому что sql server сначала выполняется объединение всех таблиц, создавая огромную связанную таблицу, а потом по ней уже выполняет операцию удаления дубликатов и подсчета актуальных значений поэтому, возможно в некоторых случаях запрос отработает быстрее, если вынести подсчеты в ПОдзапросы, а не left join как сейчас. в каждом случае нужно, конечно сравнить планы запросов  

-- 1. обычно в таких случаях создается либо материализованное представление с заранее подсчитанными значениями, либо таблица в которую програмно подсчитываются эпизодически значения. Таким образом пользователь быстро получит результат, без необходимости каждый раз выполнять группировку на стороне my sql

-- 2. А самым лучшим решением будет вообще создать таблицу любых действий пользователя, в котоорую при каждом событии будет записываться строчка с датой и в таком случае опредеить самых актуальных юзеров можно будет всего по одной таблице
