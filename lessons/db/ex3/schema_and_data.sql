#
# TABLE STRUCTURE FOR: chat_history
#

DROP TABLE IF EXISTS `chat_history`;

CREATE TABLE `chat_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `chat_id` int(10) unsigned NOT NULL COMMENT 'ИД чата',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания записи',
  `user_id` int(10) unsigned NOT NULL COMMENT 'ИД пользователя',
  `is_active` tinyint(4) NOT NULL COMMENT 'Последняя ли запись по данному чату',
  PRIMARY KEY (`id`),
  KEY `fk_chat_idx` (`chat_id`),
  KEY `fk_users_idx` (`user_id`),
  CONSTRAINT `fk_chat_history_chat` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`),
  CONSTRAINT `fk_chat_history_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='История чата';

INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (1, 1, '2005-07-15 18:10:33', 1, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (2, 2, '2008-10-18 14:07:36', 2, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (3, 3, '1974-08-25 08:41:41', 3, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (4, 4, '1988-01-04 16:29:09', 4, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (5, 5, '1971-05-09 00:56:27', 5, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (6, 6, '2007-03-30 13:53:24', 6, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (7, 7, '2018-06-11 16:54:49', 7, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (8, 8, '1995-12-30 16:37:22', 8, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (9, 9, '2015-01-22 05:54:17', 9, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (10, 10, '1971-02-09 11:45:21', 10, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (11, 11, '1970-09-09 12:59:21', 11, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (12, 12, '1980-03-02 14:08:52', 12, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (13, 13, '2000-05-14 20:18:28', 13, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (14, 14, '2010-12-27 15:06:34', 14, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (15, 15, '1978-10-23 01:20:23', 15, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (16, 16, '1998-10-08 04:42:42', 16, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (17, 17, '1986-12-22 19:18:01', 17, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (18, 18, '1992-06-01 07:36:30', 18, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (19, 19, '1997-10-22 23:47:11', 19, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (20, 20, '2012-12-07 22:10:01', 20, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (21, 21, '1976-03-15 06:05:03', 21, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (22, 22, '1981-08-19 08:02:13', 22, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (23, 23, '1987-02-26 00:04:41', 23, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (24, 24, '1998-06-08 23:01:01', 24, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (25, 25, '2003-03-12 12:44:30', 25, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (26, 26, '2004-02-11 19:57:19', 26, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (27, 27, '2018-06-24 00:38:05', 27, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (28, 28, '1988-12-02 00:14:45', 28, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (29, 29, '2008-09-17 05:20:22', 29, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (30, 30, '2006-03-16 18:13:46', 30, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (31, 31, '2000-11-16 21:03:42', 31, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (32, 32, '1989-06-19 10:29:15', 32, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (33, 33, '1974-02-03 21:04:15', 33, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (34, 34, '1978-01-02 10:34:12', 34, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (35, 35, '1979-05-26 07:42:19', 35, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (36, 36, '1998-03-20 16:15:47', 36, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (37, 37, '1973-10-29 16:39:44', 37, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (38, 38, '2007-01-31 11:29:48', 38, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (39, 39, '1978-06-14 18:10:29', 39, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (40, 40, '1993-07-03 18:11:26', 40, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (41, 41, '1996-05-13 12:33:17', 41, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (42, 42, '2000-03-08 14:15:39', 42, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (43, 43, '1987-06-25 01:15:41', 43, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (44, 44, '1996-10-06 18:35:53', 44, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (45, 45, '2005-07-04 07:14:57', 45, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (46, 46, '2011-10-08 10:31:50', 46, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (47, 47, '1976-08-26 02:45:18', 47, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (48, 48, '1998-03-09 07:27:42', 48, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (49, 49, '1979-07-18 18:59:53', 49, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (50, 50, '1985-04-15 01:31:42', 50, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (51, 51, '1987-05-28 12:04:49', 51, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (52, 52, '1989-07-09 16:56:32', 52, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (53, 53, '1981-05-02 06:23:40', 53, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (54, 54, '1980-07-04 02:12:14', 54, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (55, 55, '1987-02-26 20:42:41', 55, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (56, 56, '1989-03-21 13:28:30', 56, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (57, 57, '1978-04-27 06:48:58', 57, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (58, 58, '2008-12-15 18:58:44', 58, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (59, 59, '1991-01-20 22:37:44', 59, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (60, 60, '1972-10-25 09:53:56', 60, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (61, 61, '2015-07-24 14:09:45', 61, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (62, 62, '1989-01-04 00:46:43', 62, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (63, 63, '1999-01-16 02:33:15', 63, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (64, 64, '1976-01-30 03:07:46', 64, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (65, 65, '1994-07-15 20:46:04', 65, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (66, 66, '1980-07-01 09:27:03', 66, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (67, 67, '2003-12-01 19:20:52', 67, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (68, 68, '2017-10-28 17:59:29', 68, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (69, 69, '2009-10-30 14:54:22', 69, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (70, 70, '1996-04-03 13:10:03', 70, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (71, 71, '1975-12-02 20:37:53', 71, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (72, 72, '1981-08-03 21:38:03', 72, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (73, 73, '2019-08-27 09:35:13', 73, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (74, 74, '2018-12-20 00:06:57', 74, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (75, 75, '1973-04-20 08:25:06', 75, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (76, 76, '2017-09-11 00:06:08', 76, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (77, 77, '1999-11-24 11:08:43', 77, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (78, 78, '1987-09-10 07:24:44', 78, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (79, 79, '1995-08-05 05:47:38', 79, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (80, 80, '2014-06-15 02:26:04', 80, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (81, 81, '1977-11-28 05:16:41', 81, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (82, 82, '1984-10-20 12:17:23', 82, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (83, 83, '1989-02-17 17:13:11', 83, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (84, 84, '2017-01-19 14:54:27', 84, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (85, 85, '2014-11-03 16:57:30', 85, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (86, 86, '1970-02-22 14:04:46', 86, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (87, 87, '2016-08-31 11:35:03', 87, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (88, 88, '1981-03-07 14:49:01', 88, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (89, 89, '1976-01-29 10:34:38', 89, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (90, 90, '1975-03-05 00:55:13', 90, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (91, 91, '2005-07-31 08:48:49', 91, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (92, 92, '1977-06-01 03:28:15', 92, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (93, 93, '1990-09-30 18:23:16', 93, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (94, 94, '1978-02-11 17:07:39', 94, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (95, 95, '2005-02-20 10:48:42', 95, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (96, 96, '1974-11-10 04:35:48', 96, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (97, 97, '1994-02-05 05:08:38', 97, 1);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (98, 98, '2010-09-08 13:31:41', 98, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (99, 99, '2014-04-01 00:13:56', 99, 0);
INSERT INTO `chat_history` (`id`, `chat_id`, `created_at`, `user_id`, `is_active`) VALUES (100, 100, '1987-09-09 11:11:39', 100, 0);


#
# TABLE STRUCTURE FOR: chats
#

DROP TABLE IF EXISTS `chats`;

CREATE TABLE `chats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Чаты';

INSERT INTO `chats` (`id`, `name`) VALUES (1, 'rerum');
INSERT INTO `chats` (`id`, `name`) VALUES (2, 'et');
INSERT INTO `chats` (`id`, `name`) VALUES (3, 'soluta');
INSERT INTO `chats` (`id`, `name`) VALUES (4, 'et');
INSERT INTO `chats` (`id`, `name`) VALUES (5, 'incidunt');
INSERT INTO `chats` (`id`, `name`) VALUES (6, 'iusto');
INSERT INTO `chats` (`id`, `name`) VALUES (7, 'ut');
INSERT INTO `chats` (`id`, `name`) VALUES (8, 'atque');
INSERT INTO `chats` (`id`, `name`) VALUES (9, 'velit');
INSERT INTO `chats` (`id`, `name`) VALUES (10, 'nihil');
INSERT INTO `chats` (`id`, `name`) VALUES (11, 'odio');
INSERT INTO `chats` (`id`, `name`) VALUES (12, 'nihil');
INSERT INTO `chats` (`id`, `name`) VALUES (13, 'sit');
INSERT INTO `chats` (`id`, `name`) VALUES (14, 'aut');
INSERT INTO `chats` (`id`, `name`) VALUES (15, 'et');
INSERT INTO `chats` (`id`, `name`) VALUES (16, 'sapiente');
INSERT INTO `chats` (`id`, `name`) VALUES (17, 'quia');
INSERT INTO `chats` (`id`, `name`) VALUES (18, 'qui');
INSERT INTO `chats` (`id`, `name`) VALUES (19, 'aperiam');
INSERT INTO `chats` (`id`, `name`) VALUES (20, 'dicta');
INSERT INTO `chats` (`id`, `name`) VALUES (21, 'quidem');
INSERT INTO `chats` (`id`, `name`) VALUES (22, 'quod');
INSERT INTO `chats` (`id`, `name`) VALUES (23, 'ab');
INSERT INTO `chats` (`id`, `name`) VALUES (24, 'soluta');
INSERT INTO `chats` (`id`, `name`) VALUES (25, 'voluptate');
INSERT INTO `chats` (`id`, `name`) VALUES (26, 'neque');
INSERT INTO `chats` (`id`, `name`) VALUES (27, 'molestiae');
INSERT INTO `chats` (`id`, `name`) VALUES (28, 'autem');
INSERT INTO `chats` (`id`, `name`) VALUES (29, 'consequuntur');
INSERT INTO `chats` (`id`, `name`) VALUES (30, 'voluptas');
INSERT INTO `chats` (`id`, `name`) VALUES (31, 'quaerat');
INSERT INTO `chats` (`id`, `name`) VALUES (32, 'et');
INSERT INTO `chats` (`id`, `name`) VALUES (33, 'molestiae');
INSERT INTO `chats` (`id`, `name`) VALUES (34, 'ut');
INSERT INTO `chats` (`id`, `name`) VALUES (35, 'dolores');
INSERT INTO `chats` (`id`, `name`) VALUES (36, 'eos');
INSERT INTO `chats` (`id`, `name`) VALUES (37, 'quas');
INSERT INTO `chats` (`id`, `name`) VALUES (38, 'quasi');
INSERT INTO `chats` (`id`, `name`) VALUES (39, 'dolorem');
INSERT INTO `chats` (`id`, `name`) VALUES (40, 'dolor');
INSERT INTO `chats` (`id`, `name`) VALUES (41, 'expedita');
INSERT INTO `chats` (`id`, `name`) VALUES (42, 'sunt');
INSERT INTO `chats` (`id`, `name`) VALUES (43, 'non');
INSERT INTO `chats` (`id`, `name`) VALUES (44, 'magnam');
INSERT INTO `chats` (`id`, `name`) VALUES (45, 'qui');
INSERT INTO `chats` (`id`, `name`) VALUES (46, 'omnis');
INSERT INTO `chats` (`id`, `name`) VALUES (47, 'dolorem');
INSERT INTO `chats` (`id`, `name`) VALUES (48, 'ullam');
INSERT INTO `chats` (`id`, `name`) VALUES (49, 'sapiente');
INSERT INTO `chats` (`id`, `name`) VALUES (50, 'eum');
INSERT INTO `chats` (`id`, `name`) VALUES (51, 'quasi');
INSERT INTO `chats` (`id`, `name`) VALUES (52, 'ut');
INSERT INTO `chats` (`id`, `name`) VALUES (53, 'reiciendis');
INSERT INTO `chats` (`id`, `name`) VALUES (54, 'sed');
INSERT INTO `chats` (`id`, `name`) VALUES (55, 'impedit');
INSERT INTO `chats` (`id`, `name`) VALUES (56, 'corporis');
INSERT INTO `chats` (`id`, `name`) VALUES (57, 'velit');
INSERT INTO `chats` (`id`, `name`) VALUES (58, 'et');
INSERT INTO `chats` (`id`, `name`) VALUES (59, 'nulla');
INSERT INTO `chats` (`id`, `name`) VALUES (60, 'ut');
INSERT INTO `chats` (`id`, `name`) VALUES (61, 'nihil');
INSERT INTO `chats` (`id`, `name`) VALUES (62, 'iusto');
INSERT INTO `chats` (`id`, `name`) VALUES (63, 'aut');
INSERT INTO `chats` (`id`, `name`) VALUES (64, 'quae');
INSERT INTO `chats` (`id`, `name`) VALUES (65, 'neque');
INSERT INTO `chats` (`id`, `name`) VALUES (66, 'quia');
INSERT INTO `chats` (`id`, `name`) VALUES (67, 'ipsa');
INSERT INTO `chats` (`id`, `name`) VALUES (68, 'ut');
INSERT INTO `chats` (`id`, `name`) VALUES (69, 'autem');
INSERT INTO `chats` (`id`, `name`) VALUES (70, 'vel');
INSERT INTO `chats` (`id`, `name`) VALUES (71, 'modi');
INSERT INTO `chats` (`id`, `name`) VALUES (72, 'maiores');
INSERT INTO `chats` (`id`, `name`) VALUES (73, 'ut');
INSERT INTO `chats` (`id`, `name`) VALUES (74, 'sapiente');
INSERT INTO `chats` (`id`, `name`) VALUES (75, 'beatae');
INSERT INTO `chats` (`id`, `name`) VALUES (76, 'maiores');
INSERT INTO `chats` (`id`, `name`) VALUES (77, 'enim');
INSERT INTO `chats` (`id`, `name`) VALUES (78, 'repellat');
INSERT INTO `chats` (`id`, `name`) VALUES (79, 'aperiam');
INSERT INTO `chats` (`id`, `name`) VALUES (80, 'harum');
INSERT INTO `chats` (`id`, `name`) VALUES (81, 'deserunt');
INSERT INTO `chats` (`id`, `name`) VALUES (82, 'aut');
INSERT INTO `chats` (`id`, `name`) VALUES (83, 'nemo');
INSERT INTO `chats` (`id`, `name`) VALUES (84, 'quidem');
INSERT INTO `chats` (`id`, `name`) VALUES (85, 'et');
INSERT INTO `chats` (`id`, `name`) VALUES (86, 'sunt');
INSERT INTO `chats` (`id`, `name`) VALUES (87, 'aut');
INSERT INTO `chats` (`id`, `name`) VALUES (88, 'cumque');
INSERT INTO `chats` (`id`, `name`) VALUES (89, 'officiis');
INSERT INTO `chats` (`id`, `name`) VALUES (90, 'tempora');
INSERT INTO `chats` (`id`, `name`) VALUES (91, 'velit');
INSERT INTO `chats` (`id`, `name`) VALUES (92, 'sequi');
INSERT INTO `chats` (`id`, `name`) VALUES (93, 'similique');
INSERT INTO `chats` (`id`, `name`) VALUES (94, 'accusamus');
INSERT INTO `chats` (`id`, `name`) VALUES (95, 'id');
INSERT INTO `chats` (`id`, `name`) VALUES (96, 'vel');
INSERT INTO `chats` (`id`, `name`) VALUES (97, 'cumque');
INSERT INTO `chats` (`id`, `name`) VALUES (98, 'sed');
INSERT INTO `chats` (`id`, `name`) VALUES (99, 'vel');
INSERT INTO `chats` (`id`, `name`) VALUES (100, 'quia');


#
# TABLE STRUCTURE FOR: cities
#

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Наименование',
  `country_id` int(10) unsigned NOT NULL COMMENT 'Дата создания',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_countries_1_idx` (`country_id`),
  CONSTRAINT `fk_countries_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Города';

INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (1, 'Harberton', 1);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (2, 'Lockmanborough', 2);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (3, 'North Matildachester', 3);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (4, 'Kubside', 4);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (5, 'Hackettmouth', 5);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (6, 'Lake Verlie', 6);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (7, 'Lake Dario', 7);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (8, 'West Maribel', 8);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (9, 'Hahnfurt', 9);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (10, 'West Adriana', 10);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (11, 'South Jaiden', 11);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (12, 'Trompborough', 12);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (13, 'Joaquinburgh', 13);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (14, 'New Johan', 14);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (15, 'East Audreystad', 15);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (16, 'East Eribertoburgh', 16);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (17, 'North Carlotta', 17);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (18, 'Heidenreichport', 18);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (19, 'Andersonmouth', 19);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (20, 'Alverahaven', 20);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (21, 'Hertamouth', 21);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (22, 'Whiteborough', 22);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (23, 'Ramonmouth', 23);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (24, 'Antwonchester', 24);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (25, 'Bernhardshire', 25);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (26, 'North Waldo', 26);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (27, 'Ceasarland', 27);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (28, 'East Verla', 28);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (29, 'New Darrylport', 29);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (30, 'Raynorshire', 30);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (31, 'Ondrickaside', 31);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (32, 'Ernieborough', 32);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (33, 'Kyrachester', 33);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (34, 'Port Xavierville', 34);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (35, 'South Clarissaberg', 35);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (36, 'Stephonton', 36);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (37, 'Willfort', 37);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (38, 'Swaniawskishire', 38);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (39, 'Bergnaumbury', 39);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (40, 'Lynchchester', 40);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (41, 'East Felicityton', 41);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (42, 'Whitestad', 42);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (43, 'Port Rylanmouth', 43);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (44, 'Margaritaland', 44);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (45, 'Baileyborough', 45);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (46, 'Eichmannland', 46);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (47, 'Violetland', 47);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (48, 'East Janymouth', 48);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (49, 'South Jarvisborough', 49);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (50, 'East Katherine', 50);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (51, 'DuBuquehaven', 51);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (52, 'South Roselynfurt', 52);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (53, 'West Johnnyport', 53);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (54, 'Freedaborough', 54);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (55, 'New Retachester', 55);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (56, 'Port Keshawnton', 56);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (57, 'Donnyside', 57);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (58, 'Bernhardmouth', 58);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (59, 'Baileyshire', 59);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (60, 'North Duane', 60);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (61, 'Wilkinsonburgh', 61);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (62, 'West Svenstad', 62);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (63, 'Beahanmouth', 63);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (64, 'Lake Flaviohaven', 64);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (65, 'Ondrickaville', 65);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (66, 'East Unique', 66);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (67, 'South Jayme', 67);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (68, 'West Tommie', 68);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (69, 'South Adelia', 69);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (70, 'Angelicaport', 70);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (71, 'Port Janeborough', 71);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (72, 'Rasheedfurt', 72);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (73, 'Joannechester', 73);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (74, 'New Judgehaven', 74);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (75, 'Enafurt', 75);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (76, 'Johnstonchester', 76);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (77, 'South Willamouth', 77);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (78, 'Lake Kellie', 78);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (79, 'Derickville', 79);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (80, 'North Pedro', 80);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (81, 'Brandyland', 81);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (82, 'Katherynport', 82);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (83, 'New Creolamouth', 83);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (84, 'North Asia', 84);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (85, 'Orvalville', 85);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (86, 'Eltamouth', 86);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (87, 'North Noemy', 87);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (88, 'Kennithview', 88);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (89, 'New Makayla', 89);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (90, 'McLaughlinbury', 90);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (91, 'New Vickiechester', 91);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (92, 'New Ernestinechester', 92);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (93, 'North Stanford', 93);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (94, 'Amelystad', 94);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (95, 'Lake Desmond', 95);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (96, 'Wizamouth', 96);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (97, 'Coleport', 97);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (98, 'Medafort', 98);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (99, 'West Brookshire', 99);
INSERT INTO `cities` (`id`, `name`, `country_id`) VALUES (100, 'Port Oran', 100);


#
# TABLE STRUCTURE FOR: communities
#

DROP TABLE IF EXISTS `communities`;

CREATE TABLE `communities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор сроки',
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Название группы',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Группы';

INSERT INTO `communities` (`id`, `name`) VALUES (66, 'a');
INSERT INTO `communities` (`id`, `name`) VALUES (72, 'accusamus');
INSERT INTO `communities` (`id`, `name`) VALUES (23, 'alias');
INSERT INTO `communities` (`id`, `name`) VALUES (24, 'aliquid');
INSERT INTO `communities` (`id`, `name`) VALUES (63, 'amet');
INSERT INTO `communities` (`id`, `name`) VALUES (86, 'animi');
INSERT INTO `communities` (`id`, `name`) VALUES (90, 'assumenda');
INSERT INTO `communities` (`id`, `name`) VALUES (40, 'atque');
INSERT INTO `communities` (`id`, `name`) VALUES (7, 'aut');
INSERT INTO `communities` (`id`, `name`) VALUES (27, 'autem');
INSERT INTO `communities` (`id`, `name`) VALUES (60, 'blanditiis');
INSERT INTO `communities` (`id`, `name`) VALUES (31, 'consequatur');
INSERT INTO `communities` (`id`, `name`) VALUES (16, 'corrupti');
INSERT INTO `communities` (`id`, `name`) VALUES (92, 'culpa');
INSERT INTO `communities` (`id`, `name`) VALUES (3, 'cum');
INSERT INTO `communities` (`id`, `name`) VALUES (39, 'deleniti');
INSERT INTO `communities` (`id`, `name`) VALUES (55, 'dicta');
INSERT INTO `communities` (`id`, `name`) VALUES (17, 'distinctio');
INSERT INTO `communities` (`id`, `name`) VALUES (58, 'dolor');
INSERT INTO `communities` (`id`, `name`) VALUES (48, 'dolorem');
INSERT INTO `communities` (`id`, `name`) VALUES (9, 'doloremque');
INSERT INTO `communities` (`id`, `name`) VALUES (18, 'dolores');
INSERT INTO `communities` (`id`, `name`) VALUES (85, 'doloribus');
INSERT INTO `communities` (`id`, `name`) VALUES (35, 'ea');
INSERT INTO `communities` (`id`, `name`) VALUES (98, 'earum');
INSERT INTO `communities` (`id`, `name`) VALUES (56, 'eius');
INSERT INTO `communities` (`id`, `name`) VALUES (4, 'eligendi');
INSERT INTO `communities` (`id`, `name`) VALUES (88, 'enim');
INSERT INTO `communities` (`id`, `name`) VALUES (78, 'esse');
INSERT INTO `communities` (`id`, `name`) VALUES (54, 'est');
INSERT INTO `communities` (`id`, `name`) VALUES (14, 'et');
INSERT INTO `communities` (`id`, `name`) VALUES (91, 'eum');
INSERT INTO `communities` (`id`, `name`) VALUES (47, 'eveniet');
INSERT INTO `communities` (`id`, `name`) VALUES (49, 'ex');
INSERT INTO `communities` (`id`, `name`) VALUES (87, 'excepturi');
INSERT INTO `communities` (`id`, `name`) VALUES (34, 'expedita');
INSERT INTO `communities` (`id`, `name`) VALUES (100, 'facere');
INSERT INTO `communities` (`id`, `name`) VALUES (65, 'harum');
INSERT INTO `communities` (`id`, `name`) VALUES (32, 'hic');
INSERT INTO `communities` (`id`, `name`) VALUES (44, 'id');
INSERT INTO `communities` (`id`, `name`) VALUES (30, 'illum');
INSERT INTO `communities` (`id`, `name`) VALUES (97, 'impedit');
INSERT INTO `communities` (`id`, `name`) VALUES (46, 'in');
INSERT INTO `communities` (`id`, `name`) VALUES (41, 'incidunt');
INSERT INTO `communities` (`id`, `name`) VALUES (21, 'ipsam');
INSERT INTO `communities` (`id`, `name`) VALUES (81, 'iste');
INSERT INTO `communities` (`id`, `name`) VALUES (61, 'iure');
INSERT INTO `communities` (`id`, `name`) VALUES (73, 'iusto');
INSERT INTO `communities` (`id`, `name`) VALUES (84, 'labore');
INSERT INTO `communities` (`id`, `name`) VALUES (74, 'laboriosam');
INSERT INTO `communities` (`id`, `name`) VALUES (57, 'magnam');
INSERT INTO `communities` (`id`, `name`) VALUES (8, 'magni');
INSERT INTO `communities` (`id`, `name`) VALUES (83, 'modi');
INSERT INTO `communities` (`id`, `name`) VALUES (62, 'molestiae');
INSERT INTO `communities` (`id`, `name`) VALUES (1, 'molestias');
INSERT INTO `communities` (`id`, `name`) VALUES (96, 'mollitia');
INSERT INTO `communities` (`id`, `name`) VALUES (71, 'natus');
INSERT INTO `communities` (`id`, `name`) VALUES (5, 'necessitatibus');
INSERT INTO `communities` (`id`, `name`) VALUES (80, 'neque');
INSERT INTO `communities` (`id`, `name`) VALUES (68, 'nisi');
INSERT INTO `communities` (`id`, `name`) VALUES (59, 'nobis');
INSERT INTO `communities` (`id`, `name`) VALUES (43, 'non');
INSERT INTO `communities` (`id`, `name`) VALUES (94, 'nostrum');
INSERT INTO `communities` (`id`, `name`) VALUES (52, 'nulla');
INSERT INTO `communities` (`id`, `name`) VALUES (67, 'numquam');
INSERT INTO `communities` (`id`, `name`) VALUES (79, 'odio');
INSERT INTO `communities` (`id`, `name`) VALUES (82, 'odit');
INSERT INTO `communities` (`id`, `name`) VALUES (93, 'officia');
INSERT INTO `communities` (`id`, `name`) VALUES (69, 'officiis');
INSERT INTO `communities` (`id`, `name`) VALUES (53, 'optio');
INSERT INTO `communities` (`id`, `name`) VALUES (26, 'placeat');
INSERT INTO `communities` (`id`, `name`) VALUES (45, 'quae');
INSERT INTO `communities` (`id`, `name`) VALUES (70, 'quas');
INSERT INTO `communities` (`id`, `name`) VALUES (2, 'qui');
INSERT INTO `communities` (`id`, `name`) VALUES (38, 'quia');
INSERT INTO `communities` (`id`, `name`) VALUES (89, 'quis');
INSERT INTO `communities` (`id`, `name`) VALUES (77, 'quo');
INSERT INTO `communities` (`id`, `name`) VALUES (99, 'quos');
INSERT INTO `communities` (`id`, `name`) VALUES (19, 'ratione');
INSERT INTO `communities` (`id`, `name`) VALUES (33, 'repudiandae');
INSERT INTO `communities` (`id`, `name`) VALUES (28, 'rerum');
INSERT INTO `communities` (`id`, `name`) VALUES (75, 'saepe');
INSERT INTO `communities` (`id`, `name`) VALUES (10, 'sed');
INSERT INTO `communities` (`id`, `name`) VALUES (20, 'sequi');
INSERT INTO `communities` (`id`, `name`) VALUES (37, 'sint');
INSERT INTO `communities` (`id`, `name`) VALUES (13, 'sit');
INSERT INTO `communities` (`id`, `name`) VALUES (11, 'sunt');
INSERT INTO `communities` (`id`, `name`) VALUES (29, 'suscipit');
INSERT INTO `communities` (`id`, `name`) VALUES (6, 'tempora');
INSERT INTO `communities` (`id`, `name`) VALUES (51, 'tempore');
INSERT INTO `communities` (`id`, `name`) VALUES (76, 'temporibus');
INSERT INTO `communities` (`id`, `name`) VALUES (50, 'tenetur');
INSERT INTO `communities` (`id`, `name`) VALUES (36, 'totam');
INSERT INTO `communities` (`id`, `name`) VALUES (15, 'ut');
INSERT INTO `communities` (`id`, `name`) VALUES (42, 'vel');
INSERT INTO `communities` (`id`, `name`) VALUES (25, 'veritatis');
INSERT INTO `communities` (`id`, `name`) VALUES (12, 'voluptas');
INSERT INTO `communities` (`id`, `name`) VALUES (64, 'voluptate');
INSERT INTO `communities` (`id`, `name`) VALUES (22, 'voluptatem');
INSERT INTO `communities` (`id`, `name`) VALUES (95, 'voluptatibus');


#
# TABLE STRUCTURE FOR: communities_users
#

DROP TABLE IF EXISTS `communities_users`;

CREATE TABLE `communities_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на группу',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на пользователя',
  `created_at` datetime DEFAULT current_timestamp() COMMENT 'Время создания строки',
  PRIMARY KEY (`id`),
  KEY `fk_communities_users_idx` (`user_id`),
  KEY `fk_communities_users_comm_idx` (`community_id`),
  CONSTRAINT `fk_communities_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_communities_users_comm` FOREIGN KEY (`community_id`) REFERENCES `communities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Участники групп, связь между пользователями и группами';

INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (1, 1, 1, '1998-07-15 20:12:12');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (2, 2, 2, '2000-10-08 17:25:59');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (3, 3, 3, '2000-09-24 02:18:18');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (4, 4, 4, '1986-04-08 04:56:15');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (5, 5, 5, '1996-11-20 15:45:16');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (6, 6, 6, '1982-04-11 07:07:08');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (7, 7, 7, '2003-06-20 10:07:04');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (8, 8, 8, '2015-08-18 02:40:57');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (9, 9, 9, '2014-01-23 11:14:19');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (10, 10, 10, '1978-12-05 14:50:16');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (11, 11, 11, '1988-04-08 07:47:29');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (12, 12, 12, '1973-10-02 03:29:06');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (13, 13, 13, '2019-03-24 14:30:32');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (14, 14, 14, '2009-12-19 20:30:40');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (15, 15, 15, '2017-10-25 06:11:01');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (16, 16, 16, '1974-03-04 08:31:37');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (17, 17, 17, '2009-06-17 09:35:47');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (18, 18, 18, '2013-04-06 03:17:16');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (19, 19, 19, '1978-05-17 14:47:07');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (20, 20, 20, '2004-04-22 21:30:35');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (21, 21, 21, '2007-09-02 20:36:50');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (22, 22, 22, '1990-12-11 23:35:21');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (23, 23, 23, '2007-12-28 04:50:19');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (24, 24, 24, '1974-12-16 16:16:31');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (25, 25, 25, '2019-12-28 17:28:50');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (26, 26, 26, '2013-06-10 23:10:16');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (27, 27, 27, '1987-09-04 11:21:57');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (28, 28, 28, '1992-07-27 08:32:49');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (29, 29, 29, '1986-02-20 11:36:17');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (30, 30, 30, '2001-08-29 12:00:30');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (31, 31, 31, '1994-09-10 02:40:43');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (32, 32, 32, '1973-09-07 21:58:48');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (33, 33, 33, '2009-11-06 14:45:30');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (34, 34, 34, '2003-06-30 14:59:08');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (35, 35, 35, '2004-05-18 15:32:02');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (36, 36, 36, '2012-07-12 20:57:02');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (37, 37, 37, '1986-09-10 07:43:56');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (38, 38, 38, '2009-11-20 16:57:14');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (39, 39, 39, '1982-11-01 00:52:19');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (40, 40, 40, '1986-08-21 19:26:37');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (41, 41, 41, '1995-02-10 05:57:59');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (42, 42, 42, '1991-06-08 15:34:10');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (43, 43, 43, '2014-04-23 18:43:03');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (44, 44, 44, '1978-11-29 18:59:24');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (45, 45, 45, '1990-05-31 18:37:19');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (46, 46, 46, '1985-01-17 11:10:35');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (47, 47, 47, '1993-01-18 04:34:05');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (48, 48, 48, '1999-10-05 02:55:30');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (49, 49, 49, '1971-08-09 00:38:13');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (50, 50, 50, '1982-09-09 04:50:43');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (51, 51, 51, '2007-03-16 23:26:41');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (52, 52, 52, '1990-10-14 22:30:57');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (53, 53, 53, '1986-12-23 09:05:14');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (54, 54, 54, '1996-05-03 18:21:40');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (55, 55, 55, '1995-09-16 03:17:24');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (56, 56, 56, '1989-10-14 20:53:07');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (57, 57, 57, '1988-11-12 07:42:18');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (58, 58, 58, '1977-06-05 16:35:22');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (59, 59, 59, '1996-05-20 03:49:51');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (60, 60, 60, '2017-04-02 00:01:45');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (61, 61, 61, '1992-07-08 18:28:10');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (62, 62, 62, '1988-07-28 10:29:41');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (63, 63, 63, '1974-11-25 21:58:16');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (64, 64, 64, '1977-12-04 03:37:52');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (65, 65, 65, '2001-10-30 16:07:13');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (66, 66, 66, '1977-02-16 23:05:24');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (67, 67, 67, '2018-11-20 01:27:09');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (68, 68, 68, '1989-11-05 16:39:21');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (69, 69, 69, '2010-02-07 00:12:28');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (70, 70, 70, '2001-02-17 09:26:37');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (71, 71, 71, '2001-08-23 12:48:16');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (72, 72, 72, '2003-09-04 21:04:11');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (73, 73, 73, '1997-01-23 12:58:00');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (74, 74, 74, '1975-08-06 22:26:48');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (75, 75, 75, '1975-03-27 02:02:03');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (76, 76, 76, '1981-01-07 13:40:51');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (77, 77, 77, '2007-02-21 14:21:25');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (78, 78, 78, '1977-08-24 16:08:13');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (79, 79, 79, '2014-11-20 23:47:33');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (80, 80, 80, '1977-09-25 09:34:32');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (81, 81, 81, '1997-01-13 11:23:12');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (82, 82, 82, '2012-09-28 00:42:29');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (83, 83, 83, '1984-11-27 10:51:33');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (84, 84, 84, '1989-03-08 15:55:09');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (85, 85, 85, '2006-06-19 16:18:36');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (86, 86, 86, '1973-06-03 03:45:21');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (87, 87, 87, '2018-09-13 07:29:01');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (88, 88, 88, '1973-07-25 15:21:46');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (89, 89, 89, '1984-12-28 13:19:49');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (90, 90, 90, '1999-05-13 06:36:38');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (91, 91, 91, '1994-05-12 13:53:49');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (92, 92, 92, '1997-09-08 17:59:40');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (93, 93, 93, '2001-03-25 08:35:35');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (94, 94, 94, '1975-10-06 23:52:48');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (95, 95, 95, '2014-07-10 19:27:50');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (96, 96, 96, '1996-05-28 22:16:01');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (97, 97, 97, '2012-05-08 22:25:56');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (98, 98, 98, '1971-06-16 02:28:17');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (99, 99, 99, '1999-10-10 17:05:47');
INSERT INTO `communities_users` (`id`, `community_id`, `user_id`, `created_at`) VALUES (100, 100, 100, '2008-01-30 07:33:59');


#
# TABLE STRUCTURE FOR: community_history
#

DROP TABLE IF EXISTS `community_history`;

CREATE TABLE `community_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int(10) unsigned NOT NULL COMMENT 'ИД пользователя',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания записи',
  `community_id` int(10) unsigned NOT NULL COMMENT 'ИД группы',
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_community_history_user_idx` (`user_id`),
  KEY `fk_community_history_community_idx` (`community_id`),
  CONSTRAINT `fk_community_history_community` FOREIGN KEY (`community_id`) REFERENCES `communities` (`id`),
  CONSTRAINT `fk_community_history_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (1, 1, '1987-08-09 04:08:14', 1, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (2, 2, '2004-04-22 00:54:23', 2, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (3, 3, '1970-12-27 15:04:35', 3, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (4, 4, '1986-12-05 23:43:39', 4, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (5, 5, '1979-11-01 02:45:19', 5, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (6, 6, '1990-06-02 20:24:18', 6, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (7, 7, '2018-03-23 19:31:52', 7, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (8, 8, '2005-02-06 03:10:46', 8, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (9, 9, '2010-05-19 05:17:03', 9, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (10, 10, '2006-08-28 20:19:31', 10, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (11, 11, '2016-11-25 21:00:34', 11, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (12, 12, '1997-02-19 04:03:35', 12, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (13, 13, '1999-12-25 09:46:59', 13, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (14, 14, '2013-11-04 02:46:56', 14, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (15, 15, '2016-10-15 06:50:03', 15, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (16, 16, '2015-10-09 08:12:28', 16, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (17, 17, '1998-10-06 06:52:54', 17, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (18, 18, '1987-09-03 02:57:09', 18, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (19, 19, '2006-04-05 06:54:04', 19, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (20, 20, '1971-08-27 09:19:12', 20, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (21, 21, '2017-10-15 18:31:23', 21, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (22, 22, '2008-07-02 13:04:29', 22, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (23, 23, '1978-11-01 04:23:37', 23, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (24, 24, '1973-05-20 03:06:53', 24, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (25, 25, '1987-03-09 20:07:23', 25, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (26, 26, '2003-12-11 04:12:14', 26, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (27, 27, '1990-06-10 03:10:04', 27, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (28, 28, '2000-09-20 10:26:47', 28, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (29, 29, '2009-03-24 17:15:20', 29, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (30, 30, '1975-03-27 03:24:23', 30, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (31, 31, '2007-08-23 23:00:51', 31, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (32, 32, '2008-11-17 09:21:50', 32, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (33, 33, '1981-06-30 11:34:26', 33, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (34, 34, '1981-09-12 11:35:55', 34, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (35, 35, '2013-08-11 06:45:41', 35, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (36, 36, '2003-02-27 02:50:05', 36, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (37, 37, '2008-05-23 23:15:57', 37, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (38, 38, '1972-07-11 03:47:22', 38, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (39, 39, '1997-12-16 02:45:30', 39, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (40, 40, '2008-02-04 18:28:55', 40, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (41, 41, '1977-03-17 16:16:33', 41, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (42, 42, '1992-01-07 16:45:16', 42, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (43, 43, '2003-04-06 05:57:20', 43, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (44, 44, '1983-05-13 10:22:53', 44, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (45, 45, '1997-07-15 07:25:16', 45, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (46, 46, '1995-01-21 05:59:24', 46, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (47, 47, '2018-09-10 04:12:45', 47, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (48, 48, '1977-01-17 18:38:12', 48, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (49, 49, '1980-05-23 22:21:25', 49, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (50, 50, '2020-04-29 12:00:43', 50, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (51, 51, '1974-07-09 22:02:28', 51, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (52, 52, '1983-02-03 05:12:54', 52, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (53, 53, '1993-01-11 14:27:35', 53, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (54, 54, '2001-11-16 11:19:02', 54, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (55, 55, '2005-08-24 17:33:22', 55, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (56, 56, '1977-07-20 14:14:43', 56, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (57, 57, '2015-07-21 02:22:28', 57, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (58, 58, '1991-10-01 08:19:50', 58, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (59, 59, '2008-04-01 22:31:53', 59, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (60, 60, '1979-05-30 04:57:33', 60, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (61, 61, '2012-01-28 00:24:34', 61, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (62, 62, '1986-08-26 14:56:42', 62, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (63, 63, '1979-11-04 08:04:44', 63, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (64, 64, '1984-03-14 22:44:09', 64, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (65, 65, '1988-07-16 23:22:49', 65, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (66, 66, '1983-01-04 01:35:16', 66, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (67, 67, '2008-05-26 01:10:23', 67, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (68, 68, '1977-01-15 07:20:46', 68, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (69, 69, '1986-05-08 17:54:11', 69, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (70, 70, '1990-03-10 19:13:14', 70, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (71, 71, '1972-08-09 03:38:08', 71, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (72, 72, '1981-07-14 09:10:56', 72, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (73, 73, '1979-02-24 00:35:34', 73, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (74, 74, '1998-06-20 23:10:47', 74, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (75, 75, '1972-09-24 02:07:03', 75, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (76, 76, '1971-05-26 12:16:37', 76, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (77, 77, '1980-09-06 07:54:27', 77, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (78, 78, '2005-12-09 01:03:47', 78, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (79, 79, '1990-08-24 02:40:51', 79, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (80, 80, '2004-02-07 18:46:53', 80, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (81, 81, '2017-10-28 13:51:58', 81, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (82, 82, '2009-06-08 07:07:37', 82, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (83, 83, '1979-04-15 07:20:52', 83, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (84, 84, '2018-11-11 10:39:15', 84, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (85, 85, '2011-02-24 12:49:13', 85, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (86, 86, '2020-05-10 09:11:25', 86, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (87, 87, '2004-12-19 10:09:14', 87, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (88, 88, '1983-11-21 14:51:26', 88, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (89, 89, '2012-01-25 21:04:08', 89, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (90, 90, '1991-08-06 03:56:21', 90, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (91, 91, '2017-11-05 22:15:58', 91, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (92, 92, '2017-12-10 06:20:01', 92, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (93, 93, '1999-03-24 17:31:20', 93, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (94, 94, '2020-03-27 08:00:18', 94, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (95, 95, '1988-09-24 19:01:20', 95, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (96, 96, '1994-10-04 02:19:23', 96, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (97, 97, '2005-03-31 12:55:43', 97, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (98, 98, '1974-06-07 09:14:58', 98, 1);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (99, 99, '1993-02-11 06:57:46', 99, 0);
INSERT INTO `community_history` (`id`, `user_id`, `created_at`, `community_id`, `is_active`) VALUES (100, 100, '1972-11-13 06:32:13', 100, 0);


#
# TABLE STRUCTURE FOR: countries
#

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Страны';

INSERT INTO `countries` (`id`, `name`) VALUES (50, 'Afghanistan');
INSERT INTO `countries` (`id`, `name`) VALUES (96, 'American Samoa');
INSERT INTO `countries` (`id`, `name`) VALUES (9, 'Antigua and Barbuda');
INSERT INTO `countries` (`id`, `name`) VALUES (99, 'Aruba');
INSERT INTO `countries` (`id`, `name`) VALUES (80, 'Austria');
INSERT INTO `countries` (`id`, `name`) VALUES (34, 'Barbados');
INSERT INTO `countries` (`id`, `name`) VALUES (36, 'Bhutan');
INSERT INTO `countries` (`id`, `name`) VALUES (76, 'Bolivia');
INSERT INTO `countries` (`id`, `name`) VALUES (56, 'Botswana');
INSERT INTO `countries` (`id`, `name`) VALUES (40, 'Brazil');
INSERT INTO `countries` (`id`, `name`) VALUES (84, 'Cambodia');
INSERT INTO `countries` (`id`, `name`) VALUES (8, 'Cameroon');
INSERT INTO `countries` (`id`, `name`) VALUES (81, 'Canada');
INSERT INTO `countries` (`id`, `name`) VALUES (100, 'Cayman Islands');
INSERT INTO `countries` (`id`, `name`) VALUES (87, 'Central African Republic');
INSERT INTO `countries` (`id`, `name`) VALUES (21, 'Chile');
INSERT INTO `countries` (`id`, `name`) VALUES (42, 'Comoros');
INSERT INTO `countries` (`id`, `name`) VALUES (30, 'Cook Islands');
INSERT INTO `countries` (`id`, `name`) VALUES (12, 'Costa Rica');
INSERT INTO `countries` (`id`, `name`) VALUES (35, 'Cote d\'Ivoire');
INSERT INTO `countries` (`id`, `name`) VALUES (51, 'Djibouti');
INSERT INTO `countries` (`id`, `name`) VALUES (85, 'Dominica');
INSERT INTO `countries` (`id`, `name`) VALUES (38, 'Germany');
INSERT INTO `countries` (`id`, `name`) VALUES (92, 'Gibraltar');
INSERT INTO `countries` (`id`, `name`) VALUES (95, 'Greece');
INSERT INTO `countries` (`id`, `name`) VALUES (46, 'Greenland');
INSERT INTO `countries` (`id`, `name`) VALUES (52, 'Guadeloupe');
INSERT INTO `countries` (`id`, `name`) VALUES (27, 'Guatemala');
INSERT INTO `countries` (`id`, `name`) VALUES (55, 'Guernsey');
INSERT INTO `countries` (`id`, `name`) VALUES (24, 'Guinea-Bissau');
INSERT INTO `countries` (`id`, `name`) VALUES (69, 'Holy See (Vatican City State)');
INSERT INTO `countries` (`id`, `name`) VALUES (89, 'Honduras');
INSERT INTO `countries` (`id`, `name`) VALUES (94, 'Hong Kong');
INSERT INTO `countries` (`id`, `name`) VALUES (82, 'Iceland');
INSERT INTO `countries` (`id`, `name`) VALUES (78, 'India');
INSERT INTO `countries` (`id`, `name`) VALUES (57, 'Indonesia');
INSERT INTO `countries` (`id`, `name`) VALUES (15, 'Iran');
INSERT INTO `countries` (`id`, `name`) VALUES (23, 'Israel');
INSERT INTO `countries` (`id`, `name`) VALUES (18, 'Jersey');
INSERT INTO `countries` (`id`, `name`) VALUES (19, 'Jordan');
INSERT INTO `countries` (`id`, `name`) VALUES (47, 'Korea');
INSERT INTO `countries` (`id`, `name`) VALUES (93, 'Lao People\'s Democratic Republic');
INSERT INTO `countries` (`id`, `name`) VALUES (64, 'Latvia');
INSERT INTO `countries` (`id`, `name`) VALUES (61, 'Lebanon');
INSERT INTO `countries` (`id`, `name`) VALUES (70, 'Lesotho');
INSERT INTO `countries` (`id`, `name`) VALUES (62, 'Macao');
INSERT INTO `countries` (`id`, `name`) VALUES (83, 'Madagascar');
INSERT INTO `countries` (`id`, `name`) VALUES (45, 'Mali');
INSERT INTO `countries` (`id`, `name`) VALUES (31, 'Marshall Islands');
INSERT INTO `countries` (`id`, `name`) VALUES (86, 'Mauritania');
INSERT INTO `countries` (`id`, `name`) VALUES (16, 'Mauritius');
INSERT INTO `countries` (`id`, `name`) VALUES (73, 'Mexico');
INSERT INTO `countries` (`id`, `name`) VALUES (28, 'Montenegro');
INSERT INTO `countries` (`id`, `name`) VALUES (60, 'Montserrat');
INSERT INTO `countries` (`id`, `name`) VALUES (11, 'Morocco');
INSERT INTO `countries` (`id`, `name`) VALUES (53, 'Mozambique');
INSERT INTO `countries` (`id`, `name`) VALUES (71, 'Myanmar');
INSERT INTO `countries` (`id`, `name`) VALUES (6, 'Namibia');
INSERT INTO `countries` (`id`, `name`) VALUES (63, 'Nepal');
INSERT INTO `countries` (`id`, `name`) VALUES (10, 'Netherlands');
INSERT INTO `countries` (`id`, `name`) VALUES (17, 'Nicaragua');
INSERT INTO `countries` (`id`, `name`) VALUES (29, 'Nigeria');
INSERT INTO `countries` (`id`, `name`) VALUES (39, 'Oman');
INSERT INTO `countries` (`id`, `name`) VALUES (98, 'Pakistan');
INSERT INTO `countries` (`id`, `name`) VALUES (1, 'Palau');
INSERT INTO `countries` (`id`, `name`) VALUES (33, 'Palestinian Territory');
INSERT INTO `countries` (`id`, `name`) VALUES (59, 'Papua New Guinea');
INSERT INTO `countries` (`id`, `name`) VALUES (25, 'Philippines');
INSERT INTO `countries` (`id`, `name`) VALUES (68, 'Poland');
INSERT INTO `countries` (`id`, `name`) VALUES (7, 'Puerto Rico');
INSERT INTO `countries` (`id`, `name`) VALUES (22, 'Qatar');
INSERT INTO `countries` (`id`, `name`) VALUES (49, 'Reunion');
INSERT INTO `countries` (`id`, `name`) VALUES (79, 'Rwanda');
INSERT INTO `countries` (`id`, `name`) VALUES (5, 'Saint Kitts and Nevis');
INSERT INTO `countries` (`id`, `name`) VALUES (58, 'Saint Lucia');
INSERT INTO `countries` (`id`, `name`) VALUES (43, 'Saint Pierre and Miquelon');
INSERT INTO `countries` (`id`, `name`) VALUES (54, 'Saint Vincent and the Grenadines');
INSERT INTO `countries` (`id`, `name`) VALUES (20, 'San Marino');
INSERT INTO `countries` (`id`, `name`) VALUES (66, 'Sao Tome and Principe');
INSERT INTO `countries` (`id`, `name`) VALUES (67, 'Saudi Arabia');
INSERT INTO `countries` (`id`, `name`) VALUES (72, 'Serbia');
INSERT INTO `countries` (`id`, `name`) VALUES (41, 'Seychelles');
INSERT INTO `countries` (`id`, `name`) VALUES (4, 'Slovakia (Slovak Republic)');
INSERT INTO `countries` (`id`, `name`) VALUES (13, 'Slovenia');
INSERT INTO `countries` (`id`, `name`) VALUES (97, 'South Africa');
INSERT INTO `countries` (`id`, `name`) VALUES (32, 'Suriname');
INSERT INTO `countries` (`id`, `name`) VALUES (37, 'Svalbard & Jan Mayen Islands');
INSERT INTO `countries` (`id`, `name`) VALUES (26, 'Sweden');
INSERT INTO `countries` (`id`, `name`) VALUES (74, 'Tanzania');
INSERT INTO `countries` (`id`, `name`) VALUES (88, 'Tonga');
INSERT INTO `countries` (`id`, `name`) VALUES (3, 'Turkmenistan');
INSERT INTO `countries` (`id`, `name`) VALUES (77, 'Turks and Caicos Islands');
INSERT INTO `countries` (`id`, `name`) VALUES (75, 'Tuvalu');
INSERT INTO `countries` (`id`, `name`) VALUES (14, 'Uganda');
INSERT INTO `countries` (`id`, `name`) VALUES (48, 'United States of America');
INSERT INTO `countries` (`id`, `name`) VALUES (91, 'United States Virgin Islands');
INSERT INTO `countries` (`id`, `name`) VALUES (65, 'Uruguay');
INSERT INTO `countries` (`id`, `name`) VALUES (44, 'Vietnam');
INSERT INTO `countries` (`id`, `name`) VALUES (90, 'Wallis and Futuna');
INSERT INTO `countries` (`id`, `name`) VALUES (2, 'Western Sahara');


#
# TABLE STRUCTURE FOR: friendship
#

DROP TABLE IF EXISTS `friendship`;

CREATE TABLE `friendship` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на инициатора дружеских отношений',
  `friend_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на получателя приглашения дружить',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на статус (текущее состояние) отношений',
  `requested_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Время отправления приглашения дружить',
  `confirmed_at` datetime DEFAULT NULL COMMENT 'Время подтверждения приглашения',
  PRIMARY KEY (`id`),
  KEY `fk_friendship_user_idx` (`user_id`),
  KEY `fk_friendship_friend_idx` (`friend_id`),
  KEY `fk_friendship_statuses_idx` (`status_id`),
  CONSTRAINT `fk_friendship_friend` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_friendship_statuses` FOREIGN KEY (`status_id`) REFERENCES `friendship_statuses` (`id`),
  CONSTRAINT `fk_friendship_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Таблица дружбы';

INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (1, 1, 1, 1, '1983-04-26 18:50:19', '1992-07-12 09:05:20');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (2, 2, 2, 2, '2008-05-17 20:45:13', '2016-09-23 12:10:27');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (3, 3, 3, 3, '2019-07-30 17:49:05', '1985-03-22 04:42:19');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (4, 4, 4, 4, '2016-12-21 07:45:37', '1997-09-21 16:25:09');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (5, 5, 5, 5, '2012-11-24 03:07:10', '1995-10-12 23:25:27');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (6, 6, 6, 6, '1978-09-15 11:16:21', '1994-05-20 05:03:10');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (7, 7, 7, 7, '2014-01-06 22:33:15', '2011-12-17 21:27:32');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (8, 8, 8, 8, '1978-05-07 22:23:39', '1973-07-16 08:42:11');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (9, 9, 9, 9, '2004-07-23 18:03:46', '1991-05-10 05:01:39');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (10, 10, 10, 10, '1994-11-25 13:55:56', '1976-08-11 18:33:17');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (11, 11, 11, 11, '2010-02-17 22:25:38', '2016-08-31 08:35:45');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (12, 12, 12, 12, '1995-02-22 15:04:50', '1990-05-19 04:35:11');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (13, 13, 13, 13, '1976-08-18 11:26:54', '1976-01-03 18:12:16');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (14, 14, 14, 14, '1977-12-15 00:11:50', '1977-11-19 18:00:06');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (15, 15, 15, 15, '1995-07-11 12:24:18', '1970-01-05 21:49:15');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (16, 16, 16, 16, '2017-03-28 17:45:28', '2011-03-15 01:04:50');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (17, 17, 17, 17, '2000-08-16 17:31:22', '1996-09-08 23:10:14');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (18, 18, 18, 18, '1982-05-04 12:28:45', '2013-06-05 16:26:40');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (19, 19, 19, 19, '2012-11-20 10:28:05', '2009-04-13 08:50:34');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (20, 20, 20, 20, '1992-06-20 04:10:56', '1970-08-03 10:24:46');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (21, 21, 21, 21, '1982-08-10 18:50:30', '1999-08-08 14:00:15');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (22, 22, 22, 22, '2012-12-25 19:44:39', '2012-06-10 10:27:12');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (23, 23, 23, 23, '2014-07-05 04:30:47', '1992-01-26 00:54:24');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (24, 24, 24, 24, '2008-11-11 09:08:23', '1978-07-12 14:18:20');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (25, 25, 25, 25, '1977-05-02 00:46:27', '1984-02-11 13:34:37');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (26, 26, 26, 26, '2012-10-28 20:02:34', '1996-09-01 09:50:50');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (27, 27, 27, 27, '1999-12-09 22:59:35', '1997-04-08 00:02:46');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (28, 28, 28, 28, '1996-06-21 13:14:02', '1972-11-08 14:02:57');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (29, 29, 29, 29, '2006-09-04 15:47:22', '1997-03-03 00:50:03');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (30, 30, 30, 30, '1989-07-05 04:18:55', '2019-03-14 12:02:06');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (31, 31, 31, 31, '1984-09-11 00:06:37', '2002-10-10 17:39:21');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (32, 32, 32, 32, '2005-02-13 16:39:09', '1992-07-13 14:17:29');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (33, 33, 33, 33, '2014-01-01 05:15:02', '1994-05-11 17:19:37');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (34, 34, 34, 34, '1995-12-29 01:06:03', '1987-07-14 17:27:38');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (35, 35, 35, 35, '1972-08-30 20:42:25', '2007-07-13 06:43:28');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (36, 36, 36, 36, '1989-06-03 21:58:41', '2009-01-16 04:49:48');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (37, 37, 37, 37, '1981-03-26 08:57:02', '1991-05-03 00:35:11');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (38, 38, 38, 38, '1980-04-26 16:59:32', '1981-02-25 17:57:40');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (39, 39, 39, 39, '2013-03-08 20:37:09', '1986-01-07 21:38:46');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (40, 40, 40, 40, '1975-06-13 06:07:21', '2019-12-17 00:22:11');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (41, 41, 41, 41, '2013-02-08 18:28:24', '1992-05-24 15:11:32');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (42, 42, 42, 42, '1970-01-24 00:39:58', '1996-02-27 23:23:35');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (43, 43, 43, 43, '2004-04-28 06:46:45', '1988-11-14 09:41:24');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (44, 44, 44, 44, '1985-07-01 10:08:38', '1982-02-08 20:50:16');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (45, 45, 45, 45, '1975-04-16 14:25:10', '1992-06-28 07:49:11');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (46, 46, 46, 46, '2005-12-27 14:08:35', '1977-07-01 17:04:55');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (47, 47, 47, 47, '1976-04-09 15:46:31', '2017-10-05 08:16:31');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (48, 48, 48, 48, '1977-01-21 17:57:47', '1984-03-03 04:17:03');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (49, 49, 49, 49, '2011-10-04 10:04:39', '2009-08-19 23:54:33');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (50, 50, 50, 50, '1989-10-08 08:57:42', '2008-06-02 07:38:42');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (51, 51, 51, 51, '1992-03-06 14:14:03', '2015-12-29 22:15:00');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (52, 52, 52, 52, '1988-03-16 10:49:25', '1978-07-07 14:32:33');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (53, 53, 53, 53, '2008-08-01 18:36:46', '1979-10-23 15:25:34');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (54, 54, 54, 54, '1986-11-13 19:17:23', '2007-03-20 03:07:40');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (55, 55, 55, 55, '1996-04-02 09:09:45', '2014-03-31 12:03:28');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (56, 56, 56, 56, '1987-03-31 04:05:13', '2009-03-07 10:07:09');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (57, 57, 57, 57, '2010-01-17 15:34:22', '1983-11-12 08:46:12');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (58, 58, 58, 58, '1978-07-01 11:53:53', '1988-10-05 03:44:49');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (59, 59, 59, 59, '1978-05-15 01:53:01', '2007-04-04 15:17:32');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (60, 60, 60, 60, '2006-06-07 20:21:39', '2011-12-13 20:17:23');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (61, 61, 61, 61, '1995-06-13 13:47:51', '1971-01-01 21:45:51');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (62, 62, 62, 62, '2007-05-07 17:57:54', '2015-12-03 01:48:55');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (63, 63, 63, 63, '1984-07-23 02:14:03', '1980-11-22 18:20:01');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (64, 64, 64, 64, '2002-12-10 11:56:02', '2004-11-29 15:59:01');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (65, 65, 65, 65, '1992-08-06 00:32:06', '2002-11-13 01:14:36');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (66, 66, 66, 66, '2005-01-08 07:26:56', '1997-12-07 21:53:07');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (67, 67, 67, 67, '2010-04-20 12:31:53', '2018-01-18 04:16:59');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (68, 68, 68, 68, '2006-09-04 20:39:02', '1976-03-06 16:55:08');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (69, 69, 69, 69, '1986-03-11 12:48:00', '1986-09-19 16:41:23');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (70, 70, 70, 70, '2013-08-14 17:07:11', '1993-02-10 23:05:04');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (71, 71, 71, 71, '1979-11-29 13:17:48', '1992-10-19 09:14:19');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (72, 72, 72, 72, '1998-10-29 14:58:12', '1989-01-13 00:19:00');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (73, 73, 73, 73, '2017-12-11 19:26:33', '2014-05-01 01:50:09');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (74, 74, 74, 74, '1974-10-20 02:42:11', '1979-07-15 15:36:20');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (75, 75, 75, 75, '1980-12-25 01:34:08', '1991-11-10 20:33:25');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (76, 76, 76, 76, '1982-07-23 23:06:01', '2017-11-18 17:56:00');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (77, 77, 77, 77, '1973-11-14 09:42:01', '2004-08-12 00:19:18');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (78, 78, 78, 78, '2006-05-07 02:57:29', '2019-07-22 10:41:49');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (79, 79, 79, 79, '1977-10-19 08:21:35', '1976-11-30 17:08:53');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (80, 80, 80, 80, '2001-04-10 20:42:16', '2000-06-28 22:36:32');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (81, 81, 81, 81, '2014-02-20 20:58:20', '1986-02-13 13:21:43');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (82, 82, 82, 82, '1982-08-16 03:06:54', '1977-02-11 18:21:31');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (83, 83, 83, 83, '1999-09-20 03:31:23', '2020-05-23 00:55:27');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (84, 84, 84, 84, '2012-04-09 05:40:24', '1979-02-22 23:22:28');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (85, 85, 85, 85, '2017-03-19 11:13:59', '2015-06-08 06:26:17');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (86, 86, 86, 86, '2007-06-21 20:00:50', '2016-10-12 04:39:22');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (87, 87, 87, 87, '1992-12-10 10:58:53', '2012-02-27 08:06:08');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (88, 88, 88, 88, '1997-03-19 05:19:03', '1978-02-28 05:58:29');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (89, 89, 89, 89, '2019-09-26 23:19:47', '2013-02-03 12:40:27');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (90, 90, 90, 90, '2011-11-03 04:00:34', '1979-07-25 15:41:27');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (91, 91, 91, 91, '2014-08-03 18:43:20', '1984-10-16 12:37:20');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (92, 92, 92, 92, '1973-10-06 05:50:01', '2007-04-28 11:03:29');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (93, 93, 93, 93, '1993-10-16 16:09:17', '2010-11-01 21:49:29');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (94, 94, 94, 94, '2019-06-20 06:10:39', '1971-04-14 23:27:13');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (95, 95, 95, 95, '2016-08-01 00:20:19', '2000-03-21 00:45:58');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (96, 96, 96, 96, '1970-07-24 10:39:38', '1991-09-23 17:53:44');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (97, 97, 97, 97, '1999-11-25 22:33:26', '1979-10-22 03:58:46');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (98, 98, 98, 98, '1977-03-01 16:01:07', '1974-03-13 20:33:58');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (99, 99, 99, 99, '2018-09-26 10:16:20', '2002-12-20 15:37:43');
INSERT INTO `friendship` (`id`, `user_id`, `friend_id`, `status_id`, `requested_at`, `confirmed_at`) VALUES (100, 100, 100, 100, '2015-06-13 03:16:52', '1994-11-02 11:47:31');


#
# TABLE STRUCTURE FOR: friendship_statuses
#

DROP TABLE IF EXISTS `friendship_statuses`;

CREATE TABLE `friendship_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор строки',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Название статуса',
  `created_at` datetime DEFAULT current_timestamp() COMMENT 'Время создания строки',
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Время обновления строки',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Статусы дружбы';

INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (1, 'voluptates', '1987-09-11 01:56:24', '1986-10-02 08:07:30');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (2, 'neque', '1998-03-06 16:01:35', '1982-09-26 15:43:39');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (3, 'dolores', '1973-03-10 19:51:23', '1985-05-13 09:43:47');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (4, 'quos', '1995-10-16 20:39:18', '2013-10-23 23:25:10');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (5, 'quidem', '1980-03-15 14:24:50', '2005-09-13 11:17:19');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (6, 'enim', '1971-07-30 17:21:17', '1985-12-26 22:15:12');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (7, 'atque', '2015-08-20 09:50:44', '1981-08-29 06:43:31');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (8, 'sed', '1972-01-26 12:40:04', '2014-10-18 15:02:28');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (9, 'dolorem', '1981-05-04 17:19:18', '2006-06-08 03:51:23');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (10, 'nulla', '1974-03-13 05:20:33', '1984-04-10 13:23:10');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (11, 'sequi', '2017-05-07 16:12:16', '1988-12-28 10:18:48');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (12, 'ad', '1986-03-21 04:38:50', '2016-07-08 10:02:57');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (13, 'voluptas', '1989-08-20 13:33:23', '1992-11-11 21:26:59');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (14, 'officia', '1992-12-09 20:35:29', '2008-11-10 15:40:37');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (15, 'et', '2017-01-21 22:39:38', '1972-02-15 17:09:41');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (16, 'quisquam', '1992-08-05 18:57:43', '1971-10-10 08:49:03');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (17, 'deleniti', '1982-11-06 22:42:54', '1984-04-26 02:02:37');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (18, 'sint', '1995-09-30 09:02:39', '1996-08-24 11:41:48');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (19, 'minima', '1982-08-22 00:31:08', '1995-04-17 17:05:43');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (20, 'ut', '2017-03-03 22:29:22', '2006-03-03 17:12:47');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (21, 'sit', '2008-09-27 04:02:03', '2014-04-03 05:04:38');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (22, 'reiciendis', '1980-12-04 16:39:45', '1997-11-18 06:56:29');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (23, 'provident', '1999-09-02 06:35:47', '1985-11-30 08:04:08');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (24, 'autem', '1998-12-04 01:24:51', '1975-09-03 05:54:25');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (25, 'quas', '1992-12-20 00:01:23', '1990-11-05 23:22:06');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (26, 'consequatur', '1975-11-25 23:41:35', '2006-02-15 20:46:27');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (27, 'quasi', '2000-08-09 22:17:20', '1987-03-12 08:54:21');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (28, 'assumenda', '1974-04-02 05:37:58', '2020-02-24 08:29:45');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (29, 'rem', '1995-03-14 17:36:26', '1972-11-06 13:57:24');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (30, 'ipsa', '1995-01-19 04:59:00', '1994-11-11 16:39:02');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (31, 'maiores', '1980-02-03 18:00:30', '2006-06-09 14:12:05');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (32, 'eaque', '1973-04-06 09:42:54', '2000-12-18 20:28:39');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (33, 'molestiae', '2002-03-09 01:29:19', '2009-11-30 02:29:52');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (34, 'iusto', '1983-04-09 21:21:57', '2005-08-27 02:46:31');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (35, 'deserunt', '1980-04-20 21:45:34', '1990-05-30 06:45:05');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (36, 'aut', '1987-01-22 16:34:27', '1975-11-20 07:24:07');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (37, 'aspernatur', '2003-08-21 09:53:01', '2002-11-23 00:24:54');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (38, 'non', '1976-12-23 23:16:16', '1972-01-25 10:23:29');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (39, 'corporis', '2005-06-28 10:10:18', '1996-11-17 00:04:29');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (40, 'quia', '1997-01-12 16:18:57', '2014-09-08 05:30:15');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (41, 'dignissimos', '1997-04-17 13:31:02', '1982-08-01 03:39:21');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (42, 'repudiandae', '1975-08-20 17:32:31', '1979-07-27 21:11:51');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (43, 'eos', '2010-09-03 06:46:40', '2007-09-05 18:00:01');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (44, 'animi', '2004-09-22 09:52:53', '1998-09-12 21:14:27');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (45, 'cumque', '1981-01-03 08:42:52', '2007-05-01 18:58:16');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (46, 'debitis', '2018-11-05 11:28:19', '1975-02-13 05:46:47');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (47, 'repellat', '1988-11-02 18:39:12', '1988-01-06 15:24:25');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (48, 'laudantium', '2019-10-03 15:30:25', '1998-12-07 13:54:32');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (49, 'qui', '1994-02-11 14:01:50', '1984-12-20 11:50:24');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (50, 'facere', '2006-05-15 21:28:01', '2018-12-06 06:48:37');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (51, 'itaque', '1992-01-21 11:47:49', '2008-07-06 06:16:47');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (52, 'fugiat', '1974-01-08 06:57:13', '2015-02-15 05:29:24');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (53, 'corrupti', '1982-10-04 22:09:16', '1976-07-10 23:00:42');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (54, 'ratione', '2016-01-21 06:31:06', '1991-01-06 09:05:32');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (55, 'numquam', '2006-07-30 19:36:24', '2010-01-24 06:39:14');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (56, 'laborum', '2009-10-20 22:13:18', '2001-07-29 16:39:47');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (57, 'quam', '1983-09-15 15:47:46', '2011-07-26 15:11:48');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (58, 'ea', '1973-03-15 07:13:31', '1985-10-15 16:41:37');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (59, 'placeat', '1986-08-05 22:40:15', '1984-10-29 10:45:41');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (60, 'culpa', '1992-05-06 10:34:17', '2000-07-12 06:40:24');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (61, 'nobis', '2018-08-18 01:12:24', '1990-04-05 19:54:28');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (62, 'eligendi', '2008-05-25 04:50:49', '2016-11-05 16:52:05');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (63, 'tempora', '1998-12-22 13:21:10', '1986-03-24 14:56:40');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (64, 'asperiores', '1970-02-11 05:36:25', '1975-09-23 12:34:34');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (65, 'doloremque', '2008-07-31 23:10:33', '1992-10-08 22:30:32');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (66, 'excepturi', '1975-01-16 17:02:35', '1979-09-09 13:04:27');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (67, 'totam', '2005-12-21 10:02:34', '1988-10-29 17:16:53');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (68, 'tempore', '1995-10-23 04:32:49', '1996-07-28 21:10:53');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (69, 'labore', '2001-10-24 10:22:41', '1976-03-10 09:19:53');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (70, 'vero', '2003-11-06 03:12:29', '1989-11-28 15:52:26');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (71, 'est', '1986-09-28 09:45:22', '1988-09-22 02:20:09');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (72, 'quo', '1972-08-11 03:35:18', '1996-09-01 08:36:29');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (73, 'eveniet', '2017-04-05 00:19:59', '1974-08-07 00:58:28');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (74, 'possimus', '2000-12-29 02:11:08', '2016-05-28 15:29:25');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (75, 'vitae', '1977-12-19 10:11:48', '1987-10-17 22:13:45');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (76, 'quis', '1994-08-02 09:48:13', '2019-08-27 05:24:06');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (77, 'nisi', '1985-02-21 18:04:54', '1992-04-20 12:14:34');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (78, 'reprehenderit', '1986-08-20 07:23:05', '1979-05-11 19:15:39');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (79, 'unde', '1978-02-28 09:31:07', '1983-09-13 23:26:07');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (80, 'id', '1970-11-18 23:58:14', '1980-11-10 06:54:05');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (81, 'aliquid', '1979-03-08 00:36:43', '2015-02-21 11:35:55');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (82, 'voluptatem', '2009-01-28 10:25:54', '2020-02-23 13:34:03');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (83, 'optio', '1970-04-20 07:50:02', '1999-06-12 20:36:25');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (84, 'consectetur', '2019-01-23 15:38:24', '1990-07-23 02:20:46');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (85, 'dolorum', '2015-04-06 10:39:29', '2003-08-02 09:32:08');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (86, 'aliquam', '1977-01-29 08:51:27', '2009-08-13 16:46:51');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (87, 'similique', '2014-08-06 09:31:32', '2003-06-17 05:05:23');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (88, 'temporibus', '2008-02-28 09:42:50', '2006-08-17 09:22:53');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (89, 'at', '1981-12-22 07:24:26', '1981-01-24 12:01:03');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (90, 'soluta', '1990-06-09 10:41:54', '2008-02-07 14:17:35');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (91, 'quaerat', '2004-09-15 17:50:33', '2004-08-23 02:24:45');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (92, 'quod', '1990-01-21 05:26:28', '1976-07-29 11:28:59');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (93, 'recusandae', '2012-01-26 07:15:22', '2016-05-11 13:17:25');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (94, 'earum', '1990-01-21 01:09:56', '1994-03-28 19:59:15');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (95, 'quibusdam', '2006-09-19 07:58:19', '1970-01-23 20:48:21');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (96, 'doloribus', '1993-04-15 14:11:17', '1978-06-16 01:36:08');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (97, 'minus', '1982-07-15 23:41:13', '2008-03-28 15:13:05');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (98, 'nemo', '2002-09-25 10:13:35', '1993-08-15 13:41:41');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (99, 'a', '1976-10-01 21:01:28', '2009-06-26 14:19:54');
INSERT INTO `friendship_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES (100, 'omnis', '2020-04-15 11:11:42', '1983-04-27 09:31:11');


#
# TABLE STRUCTURE FOR: media
#

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор строки',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на пользователя, который загрузил файл',
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Путь к файлу',
  `size` int(11) NOT NULL COMMENT 'Размер файла',
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Метаданные файла' CHECK (json_valid(`metadata`)),
  `created_at` datetime DEFAULT current_timestamp() COMMENT 'Время создания строки',
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Время обновления строки',
  PRIMARY KEY (`id`),
  KEY `fk_media_users_idx` (`user_id`),
  CONSTRAINT `fk_media_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Медиафайлы';

INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (1, 1, 'http://lorempixel.com/640/480/', 82830, NULL, '2019-01-06 03:07:57', '1970-07-20 11:53:40');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (2, 2, 'http://lorempixel.com/640/480/', 51040553, NULL, '2001-02-04 16:20:43', '1982-05-22 03:02:11');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (3, 3, 'http://lorempixel.com/640/480/', 0, NULL, '2017-03-31 04:33:37', '1997-10-16 06:31:48');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (4, 4, 'http://lorempixel.com/640/480/', 429, NULL, '2012-04-09 01:07:38', '1972-11-22 19:37:09');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (5, 5, 'http://lorempixel.com/640/480/', 3241809, NULL, '1983-08-13 22:04:23', '1980-01-01 10:30:08');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (6, 6, 'http://lorempixel.com/640/480/', 0, NULL, '1977-12-24 11:00:13', '2002-06-05 06:13:21');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (7, 7, 'http://lorempixel.com/640/480/', 57547810, NULL, '1987-03-10 14:59:17', '2020-01-22 17:07:00');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (8, 8, 'http://lorempixel.com/640/480/', 227785, NULL, '2013-07-02 03:48:51', '2017-03-16 02:01:43');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (9, 9, 'http://lorempixel.com/640/480/', 6940, NULL, '1981-04-18 16:10:51', '2000-01-15 14:07:38');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (10, 10, 'http://lorempixel.com/640/480/', 0, NULL, '1994-01-09 01:03:18', '1992-07-28 04:55:41');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (11, 11, 'http://lorempixel.com/640/480/', 393641, NULL, '2005-11-02 23:10:55', '1994-01-04 20:22:22');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (12, 12, 'http://lorempixel.com/640/480/', 76647340, NULL, '2001-11-04 11:41:55', '1970-10-21 14:39:23');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (13, 13, 'http://lorempixel.com/640/480/', 2948457, NULL, '1988-06-03 02:29:55', '2017-06-26 03:39:18');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (14, 14, 'http://lorempixel.com/640/480/', 736082545, NULL, '2016-05-21 19:15:28', '2010-10-09 00:12:10');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (15, 15, 'http://lorempixel.com/640/480/', 142, NULL, '1971-05-03 08:26:31', '1986-03-22 21:19:29');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (16, 16, 'http://lorempixel.com/640/480/', 22290, NULL, '1988-01-05 15:35:37', '1989-05-17 08:23:18');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (17, 17, 'http://lorempixel.com/640/480/', 24099805, NULL, '2004-11-01 01:40:00', '1988-06-13 18:46:48');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (18, 18, 'http://lorempixel.com/640/480/', 0, NULL, '1975-11-05 15:33:17', '1991-03-21 02:11:49');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (19, 19, 'http://lorempixel.com/640/480/', 2, NULL, '1989-06-06 13:41:08', '1997-10-23 08:55:21');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (20, 20, 'http://lorempixel.com/640/480/', 140518, NULL, '2015-09-18 00:41:10', '1984-07-18 12:57:38');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (21, 21, 'http://lorempixel.com/640/480/', 572092955, NULL, '2004-01-22 16:54:12', '2009-04-11 13:21:52');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (22, 22, 'http://lorempixel.com/640/480/', 0, NULL, '1973-09-20 19:27:07', '1997-10-30 06:47:54');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (23, 23, 'http://lorempixel.com/640/480/', 6445, NULL, '2005-12-13 05:40:19', '1995-06-19 15:12:14');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (24, 24, 'http://lorempixel.com/640/480/', 43577, NULL, '2017-04-14 10:55:08', '1997-11-02 14:11:58');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (25, 25, 'http://lorempixel.com/640/480/', 0, NULL, '1988-06-01 21:19:49', '1978-05-11 03:08:08');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (26, 26, 'http://lorempixel.com/640/480/', 5184717, NULL, '2001-02-25 17:03:45', '2018-06-03 21:33:51');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (27, 27, 'http://lorempixel.com/640/480/', 5567, NULL, '1986-06-26 22:26:29', '2000-10-23 01:24:13');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (28, 28, 'http://lorempixel.com/640/480/', 1120640, NULL, '2003-06-09 22:32:31', '1986-11-07 11:50:39');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (29, 29, 'http://lorempixel.com/640/480/', 563804131, NULL, '2009-02-04 07:07:46', '1976-09-16 23:08:49');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (30, 30, 'http://lorempixel.com/640/480/', 98, NULL, '1975-10-28 20:52:13', '2013-08-26 05:19:47');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (31, 31, 'http://lorempixel.com/640/480/', 52000107, NULL, '1996-08-13 21:36:56', '1987-06-03 09:53:15');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (32, 32, 'http://lorempixel.com/640/480/', 1, NULL, '1993-01-03 12:17:57', '1972-12-23 23:52:29');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (33, 33, 'http://lorempixel.com/640/480/', 2194, NULL, '1999-03-28 23:49:10', '2020-06-16 19:53:16');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (34, 34, 'http://lorempixel.com/640/480/', 66, NULL, '1980-01-18 01:14:30', '1983-08-01 12:47:02');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (35, 35, 'http://lorempixel.com/640/480/', 127969517, NULL, '1984-01-07 22:34:48', '2017-05-24 05:23:27');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (36, 36, 'http://lorempixel.com/640/480/', 5316087, NULL, '2012-04-12 05:54:20', '1988-11-08 05:31:04');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (37, 37, 'http://lorempixel.com/640/480/', 0, NULL, '1973-08-23 11:08:12', '1981-02-24 20:52:53');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (38, 38, 'http://lorempixel.com/640/480/', 5358866, NULL, '2002-04-18 22:37:54', '1989-02-15 07:31:28');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (39, 39, 'http://lorempixel.com/640/480/', 134958000, NULL, '1993-05-29 03:07:20', '2012-11-05 20:30:31');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (40, 40, 'http://lorempixel.com/640/480/', 5299, NULL, '2017-02-20 01:52:07', '1974-07-23 09:03:36');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (41, 41, 'http://lorempixel.com/640/480/', 904845, NULL, '1992-12-26 12:21:13', '1988-02-27 23:18:38');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (42, 42, 'http://lorempixel.com/640/480/', 860, NULL, '1979-03-03 14:46:53', '1988-09-24 19:13:29');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (43, 43, 'http://lorempixel.com/640/480/', 10, NULL, '1982-07-29 09:25:53', '1996-05-18 10:40:13');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (44, 44, 'http://lorempixel.com/640/480/', 5294, NULL, '1979-09-10 17:13:24', '2013-05-30 20:42:48');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (45, 45, 'http://lorempixel.com/640/480/', 92376, NULL, '1994-10-08 08:33:57', '2009-11-28 20:16:19');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (46, 46, 'http://lorempixel.com/640/480/', 233734, NULL, '1981-05-08 13:54:59', '2015-05-16 04:57:21');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (47, 47, 'http://lorempixel.com/640/480/', 53445, NULL, '2018-05-10 07:10:09', '2014-06-30 12:54:21');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (48, 48, 'http://lorempixel.com/640/480/', 27608, NULL, '2013-06-18 02:18:04', '1997-05-31 03:39:30');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (49, 49, 'http://lorempixel.com/640/480/', 394762, NULL, '2004-12-07 18:28:38', '1976-05-01 09:43:20');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (50, 50, 'http://lorempixel.com/640/480/', 9, NULL, '1989-10-28 09:53:31', '1981-03-05 23:08:50');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (51, 51, 'http://lorempixel.com/640/480/', 0, NULL, '1980-06-18 13:43:00', '2005-05-08 13:35:58');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (52, 52, 'http://lorempixel.com/640/480/', 106, NULL, '1989-09-07 12:42:48', '1998-09-07 12:57:41');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (53, 53, 'http://lorempixel.com/640/480/', 0, NULL, '2017-12-29 19:53:18', '2011-11-25 16:42:23');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (54, 54, 'http://lorempixel.com/640/480/', 69084, NULL, '1982-06-23 05:14:34', '2004-06-04 10:16:24');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (55, 55, 'http://lorempixel.com/640/480/', 11437906, NULL, '2004-06-17 11:48:39', '1989-11-03 17:37:35');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (56, 56, 'http://lorempixel.com/640/480/', 31558592, NULL, '1974-06-20 01:14:21', '2007-07-29 23:57:55');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (57, 57, 'http://lorempixel.com/640/480/', 608403131, NULL, '1997-02-05 03:54:26', '1983-07-11 06:38:09');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (58, 58, 'http://lorempixel.com/640/480/', 137, NULL, '2018-12-31 15:11:31', '1978-08-22 11:39:09');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (59, 59, 'http://lorempixel.com/640/480/', 0, NULL, '1981-06-02 12:01:14', '1977-07-10 20:33:08');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (60, 60, 'http://lorempixel.com/640/480/', 82671, NULL, '2012-03-08 06:11:57', '1991-01-25 13:18:29');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (61, 61, 'http://lorempixel.com/640/480/', 530499, NULL, '2008-09-22 21:02:12', '1976-06-01 01:19:03');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (62, 62, 'http://lorempixel.com/640/480/', 6, NULL, '1980-01-20 23:29:53', '1978-06-03 07:41:09');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (63, 63, 'http://lorempixel.com/640/480/', 4, NULL, '1991-05-24 10:19:53', '1996-05-14 12:00:15');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (64, 64, 'http://lorempixel.com/640/480/', 42, NULL, '2001-01-20 02:47:36', '1971-07-20 23:57:48');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (65, 65, 'http://lorempixel.com/640/480/', 4, NULL, '2011-11-10 16:27:11', '2011-12-27 06:10:06');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (66, 66, 'http://lorempixel.com/640/480/', 81, NULL, '1977-09-09 07:51:47', '1995-02-11 08:56:47');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (67, 67, 'http://lorempixel.com/640/480/', 859, NULL, '1995-11-01 11:18:51', '1999-03-27 05:47:43');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (68, 68, 'http://lorempixel.com/640/480/', 9870, NULL, '1985-06-11 00:21:51', '1981-06-24 06:32:05');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (69, 69, 'http://lorempixel.com/640/480/', 169919287, NULL, '2010-06-26 13:24:27', '1994-11-06 22:49:19');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (70, 70, 'http://lorempixel.com/640/480/', 18398977, NULL, '1976-03-12 18:39:55', '2004-10-17 07:28:25');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (71, 71, 'http://lorempixel.com/640/480/', 257545, NULL, '1990-11-27 20:25:07', '2010-10-18 07:06:13');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (72, 72, 'http://lorempixel.com/640/480/', 988, NULL, '2015-07-30 04:33:58', '1979-04-08 10:20:16');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (73, 73, 'http://lorempixel.com/640/480/', 24604, NULL, '1995-03-10 20:35:43', '1995-03-02 15:00:23');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (74, 74, 'http://lorempixel.com/640/480/', 5408206, NULL, '1981-12-11 03:43:24', '2018-04-20 11:48:09');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (75, 75, 'http://lorempixel.com/640/480/', 65242, NULL, '1999-09-21 22:17:48', '2012-02-02 16:32:13');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (76, 76, 'http://lorempixel.com/640/480/', 1790503, NULL, '2015-01-12 17:16:52', '1990-02-25 15:13:45');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (77, 77, 'http://lorempixel.com/640/480/', 493, NULL, '1971-02-14 18:30:28', '1999-08-03 18:35:05');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (78, 78, 'http://lorempixel.com/640/480/', 65053, NULL, '1980-04-07 07:03:56', '2001-10-19 19:36:18');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (79, 79, 'http://lorempixel.com/640/480/', 37742836, NULL, '2014-09-27 17:11:55', '2013-04-15 19:25:02');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (80, 80, 'http://lorempixel.com/640/480/', 11976, NULL, '2003-04-15 23:09:00', '1970-01-10 07:59:12');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (81, 81, 'http://lorempixel.com/640/480/', 1791, NULL, '1984-05-22 21:58:52', '2010-07-22 16:52:31');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (82, 82, 'http://lorempixel.com/640/480/', 15253, NULL, '2011-09-21 17:37:02', '1988-02-10 22:15:41');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (83, 83, 'http://lorempixel.com/640/480/', 9087537, NULL, '1996-06-16 10:57:45', '1998-05-21 14:30:24');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (84, 84, 'http://lorempixel.com/640/480/', 40983, NULL, '1992-07-30 17:52:03', '2000-05-05 14:51:55');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (85, 85, 'http://lorempixel.com/640/480/', 48397139, NULL, '2002-03-02 20:48:28', '1983-12-21 09:25:51');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (86, 86, 'http://lorempixel.com/640/480/', 52258953, NULL, '1971-07-20 19:47:41', '2007-04-02 23:42:15');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (87, 87, 'http://lorempixel.com/640/480/', 7, NULL, '1980-10-30 21:10:31', '1989-01-05 17:09:37');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (88, 88, 'http://lorempixel.com/640/480/', 6242943, NULL, '2015-09-06 05:39:21', '1976-12-25 23:29:18');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (89, 89, 'http://lorempixel.com/640/480/', 373021, NULL, '2002-12-20 09:02:41', '2000-01-04 16:56:10');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (90, 90, 'http://lorempixel.com/640/480/', 587437441, NULL, '2016-03-31 07:41:01', '2002-04-30 12:55:21');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (91, 91, 'http://lorempixel.com/640/480/', 8361020, NULL, '2004-05-21 02:14:42', '1994-09-20 13:37:28');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (92, 92, 'http://lorempixel.com/640/480/', 964, NULL, '1995-09-25 04:45:07', '2011-01-16 18:31:13');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (93, 93, 'http://lorempixel.com/640/480/', 891030, NULL, '1989-02-16 03:55:42', '2010-02-22 07:56:48');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (94, 94, 'http://lorempixel.com/640/480/', 30896644, NULL, '1988-07-01 12:46:32', '1973-04-16 15:05:41');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (95, 95, 'http://lorempixel.com/640/480/', 0, NULL, '1985-05-23 17:45:13', '2006-10-31 18:09:34');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (96, 96, 'http://lorempixel.com/640/480/', 4, NULL, '1971-10-27 19:11:42', '2020-01-14 03:15:17');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (97, 97, 'http://lorempixel.com/640/480/', 46, NULL, '1995-06-26 23:06:21', '1990-07-30 10:39:57');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (98, 98, 'http://lorempixel.com/640/480/', 63976721, NULL, '1970-07-05 12:09:48', '1997-10-18 09:48:24');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (99, 99, 'http://lorempixel.com/640/480/', 95, NULL, '1990-10-14 13:33:26', '1982-02-14 01:48:26');
INSERT INTO `media` (`id`, `user_id`, `filename`, `size`, `metadata`, `created_at`, `updated_at`) VALUES (100, 100, 'http://lorempixel.com/640/480/', 92, NULL, '1985-04-29 12:15:37', '2011-07-10 17:41:12');


#
# TABLE STRUCTURE FOR: message_history
#

DROP TABLE IF EXISTS `message_history`;

CREATE TABLE `message_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `id_message` int(10) unsigned NOT NULL COMMENT 'ИД сообщения',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания записи',
  `id_user` int(10) unsigned NOT NULL COMMENT 'ИД пользователя',
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_history_message_idx` (`id_message`),
  KEY `fk_message_history_user_idx` (`id_user`),
  CONSTRAINT `fk_message_history_message` FOREIGN KEY (`id_message`) REFERENCES `messages` (`id`),
  CONSTRAINT `fk_message_history_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='История сообщений';

INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (1, 1, '1973-01-05 11:15:37', 1, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (2, 2, '1980-12-01 04:52:27', 2, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (3, 3, '2008-04-17 19:41:28', 3, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (4, 4, '2017-04-07 19:19:26', 4, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (5, 5, '2009-02-11 12:14:45', 5, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (6, 6, '1982-07-25 07:13:19', 6, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (7, 7, '2010-07-15 05:40:07', 7, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (8, 8, '1979-08-10 21:39:16', 8, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (9, 9, '2000-12-25 19:54:47', 9, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (10, 10, '2014-09-12 09:13:51', 10, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (11, 11, '1984-04-19 01:03:15', 11, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (12, 12, '2019-05-13 07:08:19', 12, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (13, 13, '2013-10-14 16:56:03', 13, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (14, 14, '1970-06-14 05:45:14', 14, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (15, 15, '1999-11-10 21:18:09', 15, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (16, 16, '2009-02-09 13:43:48', 16, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (17, 17, '1991-11-21 02:31:29', 17, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (18, 18, '2006-02-28 21:43:38', 18, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (19, 19, '2004-04-24 14:28:27', 19, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (20, 20, '1971-11-26 12:17:03', 20, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (21, 21, '1990-12-15 01:12:42', 21, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (22, 22, '1977-05-17 13:06:14', 22, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (23, 23, '2010-12-19 12:44:37', 23, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (24, 24, '2016-11-17 11:58:53', 24, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (25, 25, '1973-12-17 11:46:13', 25, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (26, 26, '1983-01-20 14:27:36', 26, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (27, 27, '1994-08-23 07:41:27', 27, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (28, 28, '2019-09-21 02:11:43', 28, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (29, 29, '2004-12-08 00:46:43', 29, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (30, 30, '1996-06-11 05:42:09', 30, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (31, 31, '2014-09-20 19:27:24', 31, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (32, 32, '2005-09-05 20:48:07', 32, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (33, 33, '2011-06-03 05:03:39', 33, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (34, 34, '1982-10-31 11:23:40', 34, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (35, 35, '1977-10-25 07:45:51', 35, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (36, 36, '2015-08-24 23:55:55', 36, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (37, 37, '2009-03-22 02:36:37', 37, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (38, 38, '2014-03-12 00:03:52', 38, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (39, 39, '1991-01-01 08:07:45', 39, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (40, 40, '2006-06-26 20:06:31', 40, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (41, 41, '2003-06-08 07:14:46', 41, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (42, 42, '2004-07-13 18:41:09', 42, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (43, 43, '2007-06-18 04:25:44', 43, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (44, 44, '1992-01-08 15:21:52', 44, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (45, 45, '2012-04-20 20:06:11', 45, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (46, 46, '2018-10-12 20:09:16', 46, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (47, 47, '1980-08-10 22:02:52', 47, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (48, 48, '2015-03-02 13:27:13', 48, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (49, 49, '1979-03-22 04:47:11', 49, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (50, 50, '1999-02-15 15:48:45', 50, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (51, 51, '2002-10-12 00:20:39', 51, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (52, 52, '1974-10-19 11:20:15', 52, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (53, 53, '1990-11-15 08:21:48', 53, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (54, 54, '2004-06-03 05:04:39', 54, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (55, 55, '1987-10-04 10:29:54', 55, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (56, 56, '2003-05-21 13:26:09', 56, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (57, 57, '1970-11-06 14:38:41', 57, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (58, 58, '2008-09-18 02:44:56', 58, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (59, 59, '2016-10-17 17:43:50', 59, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (60, 60, '1987-01-30 16:40:45', 60, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (61, 61, '1984-01-05 22:19:53', 61, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (62, 62, '2015-07-13 03:14:20', 62, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (63, 63, '1972-01-19 04:56:18', 63, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (64, 64, '2010-09-05 13:50:35', 64, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (65, 65, '2016-11-02 12:45:28', 65, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (66, 66, '1987-05-15 13:58:49', 66, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (67, 67, '1986-04-29 03:23:19', 67, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (68, 68, '1973-01-09 22:06:25', 68, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (69, 69, '2020-05-06 21:01:52', 69, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (70, 70, '1989-09-27 12:32:03', 70, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (71, 71, '2019-01-24 13:20:00', 71, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (72, 72, '2009-05-12 22:22:07', 72, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (73, 73, '1986-03-06 04:57:30', 73, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (74, 74, '2020-03-04 15:47:04', 74, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (75, 75, '2017-03-27 10:34:33', 75, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (76, 76, '2015-05-19 16:00:47', 76, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (77, 77, '1981-09-25 15:10:01', 77, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (78, 78, '1998-08-05 16:16:18', 78, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (79, 79, '1985-11-05 08:42:56', 79, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (80, 80, '2009-06-18 19:52:38', 80, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (81, 81, '2011-03-30 05:20:17', 81, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (82, 82, '1991-02-27 18:26:18', 82, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (83, 83, '1988-02-15 12:05:24', 83, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (84, 84, '2015-02-15 08:32:28', 84, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (85, 85, '2007-07-29 14:35:41', 85, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (86, 86, '2010-08-03 19:34:45', 86, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (87, 87, '2012-10-17 09:22:02', 87, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (88, 88, '2013-10-31 20:17:24', 88, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (89, 89, '1979-05-06 23:52:21', 89, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (90, 90, '1992-09-24 11:39:08', 90, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (91, 91, '1974-02-01 01:47:26', 91, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (92, 92, '1970-07-28 05:26:58', 92, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (93, 93, '2010-06-15 19:04:11', 93, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (94, 94, '1994-12-28 09:08:07', 94, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (95, 95, '1986-06-14 09:40:19', 95, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (96, 96, '2017-05-24 00:24:00', 96, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (97, 97, '1991-10-19 17:48:33', 97, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (98, 98, '1970-12-20 14:24:03', 98, 1);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (99, 99, '1990-01-13 13:40:35', 99, 0);
INSERT INTO `message_history` (`id`, `id_message`, `created_at`, `id_user`, `is_active`) VALUES (100, 100, '1972-05-15 23:28:05', 100, 0);


#
# TABLE STRUCTURE FOR: messages
#

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `chat_id` int(10) unsigned NOT NULL COMMENT 'ИД чата',
  `body` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Тело',
  `is_important` tinyint(4) NOT NULL COMMENT 'Важность',
  `is_delivered` tinyint(4) NOT NULL COMMENT 'Доставлено',
  PRIMARY KEY (`id`),
  KEY `fk_messages_chat_idx` (`chat_id`),
  CONSTRAINT `fk_messages_chat` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (1, 1, 'Blanditiis placeat ipsam nihil fuga velit. Autem nisi consequatur magnam a quod. Enim sit earum omnis velit labore ratione beatae amet. Natus ad neque blanditiis.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (2, 2, 'Fugit incidunt eum quos sunt quia facilis consequuntur. Maxime molestias et sint aut qui. Dolorem accusantium similique adipisci est.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (3, 3, 'Debitis vero natus porro provident dolor sed. Inventore explicabo labore corrupti ipsa quis doloremque soluta. Accusamus non minima eos quis. Id modi corporis est itaque.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (4, 4, 'Quo expedita eius eum consequatur aut. Sapiente ut distinctio ex laudantium. Nobis tenetur et voluptatem tempore qui unde quaerat.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (5, 5, 'Quod ut est est explicabo. Tempora id eaque sed quae voluptas ratione. Beatae qui est reprehenderit omnis. Illum quasi dicta nulla ipsam officiis ut.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (6, 6, 'Quam sed quidem vel accusamus. Et aut necessitatibus assumenda optio consequatur corporis. Rerum sit qui quis eius.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (7, 7, 'Deleniti autem optio libero mollitia id sed quia. Odio quas dolor et provident aliquid. Est officiis iste eum est nemo aliquid alias. Commodi corrupti repellat unde officiis.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (8, 8, 'Quidem velit rerum id nam rem facilis blanditiis quia. Eligendi quo esse dignissimos aut error sequi. Beatae veniam aperiam et impedit explicabo quo ut. Autem eaque non et placeat dolore.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (9, 9, 'Suscipit labore dolores ut aut. Unde animi modi eius voluptate vitae. Hic et qui optio eos sit vel. Officiis laborum sequi placeat cum ullam dolorum quis.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (10, 10, 'Dolorem dolores corporis cumque minima. Ut aliquid quis blanditiis odit nobis perferendis. Molestiae debitis eius excepturi eum dolorum dolores non.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (11, 11, 'Non repudiandae quia ea totam illum nostrum. Possimus enim ea suscipit ipsa. Deserunt quibusdam consectetur quibusdam quia amet magni asperiores incidunt. Consectetur sed enim quaerat sit quod odio.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (12, 12, 'Minus sed eum dicta. Cupiditate quis ab aut. Optio iure saepe sapiente in veritatis quo eligendi. Dolore ullam porro eaque quibusdam architecto.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (13, 13, 'Est aut quibusdam illum aut tempora. Vel cum impedit omnis accusamus est dignissimos et. Est praesentium quae omnis a. Quam quo perferendis possimus laborum omnis soluta.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (14, 14, 'Aliquid reprehenderit repudiandae et veritatis. Ipsam sint pariatur sequi deleniti voluptates omnis. Molestiae corrupti exercitationem quibusdam. Accusamus repellat provident in tempora cumque ut.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (15, 15, 'Quam autem commodi quis natus ut natus quia. Quia unde aut recusandae repudiandae. Quia nobis quod doloribus quidem modi rerum.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (16, 16, 'Odio sint molestiae est placeat aut. Est nesciunt ut eos. Aut vero error cupiditate veniam iure ut adipisci.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (17, 17, 'Eos earum nostrum voluptatem aut reiciendis. Nemo architecto et id in. Repudiandae illum tenetur vitae explicabo doloremque sed culpa.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (18, 18, 'Explicabo assumenda temporibus qui necessitatibus iusto. Hic autem sit ea sit adipisci.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (19, 19, 'Voluptatum dicta excepturi deleniti. Aliquam rerum sunt voluptate non non. Ut excepturi placeat rerum iste eveniet et. Vero ducimus aut veniam magnam explicabo recusandae.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (20, 20, 'Quo quo ut fugit sunt suscipit cumque harum est. Dolore eos dolor rerum accusamus labore nemo. Consectetur totam aut illum aliquid tempore quis.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (21, 21, 'Sed eius dicta officia. Qui aliquid laudantium sit dolores accusamus. Commodi sed impedit tempora exercitationem.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (22, 22, 'Vel natus earum eveniet dolorum aut maiores autem. Molestiae officiis et et libero at aliquam ducimus. Recusandae blanditiis amet fugit similique ipsam mollitia.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (23, 23, 'Et eum nobis quia eligendi nulla. Est dolorem voluptas voluptates maiores deserunt velit voluptatem. Et et at adipisci voluptatem ea optio. Neque soluta aut beatae consequatur earum.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (24, 24, 'Tempore cum dolorum nam ratione eligendi rerum omnis. Esse tempore quae ut consequatur corrupti iste repellendus. Qui qui ipsam laborum. Omnis distinctio et harum recusandae.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (25, 25, 'Fugit nesciunt dolorem cum quae esse ipsa corrupti. Ut sit rerum praesentium saepe. Ipsam et minus rerum et est maiores qui. In et saepe natus rerum accusamus sit quia.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (26, 26, 'Id laudantium repudiandae eum enim qui voluptatibus. Dolor occaecati nihil debitis officia. Et cum dignissimos recusandae quos.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (27, 27, 'Labore voluptas sint doloribus et id. Iure ea rerum optio blanditiis. Blanditiis autem laudantium enim consequatur aut laborum nihil ea.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (28, 28, 'At quia id officiis voluptatem blanditiis dicta. Libero voluptate blanditiis quo. Optio ut explicabo reiciendis illo voluptas.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (29, 29, 'Id accusamus eaque aut. Culpa voluptates praesentium sit a. Magni aperiam libero repellat ut occaecati.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (30, 30, 'Quod eum est dicta ut suscipit. Eligendi earum repudiandae rerum non. Fugit nulla voluptas aliquam esse nihil. Tempore natus dolorum molestiae rerum vitae deserunt enim.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (31, 31, 'Adipisci nulla corporis cupiditate quam. Quae saepe minima voluptatem ad. Velit enim voluptatem quo perferendis quaerat cum. Odio et voluptatem temporibus deleniti.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (32, 32, 'Ipsum aut esse culpa. Quam aut sapiente iure quia pariatur. Officiis asperiores ex nobis enim velit qui accusantium ut. Modi quaerat vel amet sed.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (33, 33, 'Tempore modi ut nobis voluptas. Cum enim qui eius rerum earum optio. Impedit minima provident quos.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (34, 34, 'Molestiae optio cumque ipsam. Quia magnam accusamus consectetur aliquam debitis odit. Similique in esse eius eligendi excepturi.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (35, 35, 'Ea qui quia veniam suscipit vel assumenda illum. Eum placeat amet nesciunt sit eum. Placeat officia et vel rerum.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (36, 36, 'Recusandae voluptatem magnam nihil ut. Quo non et et dolorem. Ut possimus delectus numquam velit qui. Minus fugit harum esse.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (37, 37, 'Incidunt modi omnis quisquam doloribus. Optio et adipisci eum possimus aut nesciunt. Sunt saepe occaecati corporis voluptatem.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (38, 38, 'Qui voluptatem qui repudiandae alias dolorum cum quo. Ut architecto fuga eos optio dolorum qui vel. Placeat sit adipisci sed consectetur voluptatem ipsam deserunt. Est et eos ut excepturi.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (39, 39, 'Tempora fuga itaque qui ipsum quae. Nemo repudiandae inventore aperiam exercitationem facere sed. Repellendus earum id ut dolore officia. Ut ipsa et velit eligendi aperiam est ut incidunt.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (40, 40, 'Quisquam necessitatibus occaecati tempora sit eum. Qui tempora porro dolorem deleniti recusandae enim. Ut totam aperiam eligendi nisi doloribus illum sint.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (41, 41, 'Asperiores et consequatur asperiores quia quo. Odio voluptatem blanditiis sapiente natus doloremque tempore magni. Hic et possimus dolores voluptatum aut ut. Sit dolore tenetur reiciendis veritatis.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (42, 42, 'Ea nostrum et reiciendis. Laboriosam eligendi vero nihil expedita autem. Cupiditate facilis sunt iusto incidunt provident occaecati aut qui.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (43, 43, 'Incidunt quos est voluptatem. Omnis id eum et reprehenderit quis fuga tempora error. Temporibus debitis tempora esse est. At cum veniam est voluptatum animi.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (44, 44, 'Eum id ad vero. Consequatur aut nostrum quia. Suscipit iusto animi repellendus est eos dolore.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (45, 45, 'Explicabo nihil porro quo neque alias quo et. Quis vel quam aut praesentium quia omnis. Necessitatibus et in earum.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (46, 46, 'Molestiae adipisci voluptate officiis nam in. Optio quam eaque rerum delectus corrupti voluptatem. Non sunt nostrum consequuntur esse. Eum eveniet tenetur officia aut odio ullam.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (47, 47, 'Est autem pariatur perferendis tempore. Voluptas tempore mollitia veritatis ipsum rerum voluptatem. Quae distinctio sit veniam quod quasi deserunt soluta.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (48, 48, 'Voluptatum atque enim magni aspernatur qui nihil. Nihil ipsam error nobis debitis sunt cumque. Ut rerum et quod ratione soluta.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (49, 49, 'Sed aut dignissimos et ipsam repellendus expedita cumque vel. Nisi repudiandae vel voluptas. Accusantium ut id quae sunt ullam perferendis deleniti.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (50, 50, 'Autem unde aliquam sint quaerat cum. Illo sint iusto dolorum dicta veniam reiciendis odit. Cupiditate ut tenetur fugit et.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (51, 51, 'Est sint occaecati iste quis dolorum nisi. Officia nostrum explicabo accusantium veritatis sed exercitationem neque. Eveniet nihil est nesciunt debitis voluptate nihil.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (52, 52, 'Est rerum facilis ipsum excepturi rerum est. Molestiae repellat rerum nostrum. Modi molestiae eaque aut omnis. Sint similique eum facere occaecati minus omnis.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (53, 53, 'Quia nostrum cum sapiente nemo quis aut. Facere doloremque omnis quae temporibus. Sequi alias ex quaerat molestiae et. Adipisci reprehenderit consequuntur nisi consequatur vero.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (54, 54, 'Iusto quis facilis fuga qui ducimus. Consectetur ut veniam itaque quaerat esse. Iste sunt est qui dolorem. Ut commodi sunt nostrum numquam voluptas atque.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (55, 55, 'Fugit molestiae iure omnis repudiandae. Vel harum magnam voluptatem sequi fugit id. Ea incidunt quaerat libero quos saepe. Itaque eos at voluptatibus sunt beatae.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (56, 56, 'Aut quos quam illum ex exercitationem veritatis et. Ut distinctio aut deserunt. Ut autem quibusdam sed itaque quod aperiam. Qui ad corrupti aut.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (57, 57, 'Fugiat mollitia architecto nihil. Culpa quos quo aut et maxime est ea.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (58, 58, 'Qui unde quia dolor corrupti. Aperiam totam iste sed et libero aperiam earum. Libero sunt et nostrum blanditiis eum impedit. Odio a laudantium velit architecto voluptatibus.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (59, 59, 'Aut ab a ut odit nesciunt. Modi hic culpa dolore doloribus sed. Qui nemo aut omnis consequuntur molestiae.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (60, 60, 'Praesentium odio nam laboriosam non assumenda illum. Fugiat velit officiis recusandae quia inventore. Ex vel magni quos non id.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (61, 61, 'Aut veniam quod molestiae aliquid nemo quod vero. Doloribus expedita est ipsum perferendis tenetur fugit. Aperiam nihil nam amet.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (62, 62, 'Veniam ut accusantium aliquam amet sapiente culpa qui ut. Sed cum maxime quas nisi enim possimus sequi numquam. Et non tenetur eos quis.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (63, 63, 'Dolores quia corporis officia perspiciatis voluptatem maxime cum. Iure repudiandae sed voluptate soluta. Recusandae et qui modi perspiciatis vel. Blanditiis omnis qui inventore aliquam.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (64, 64, 'Ullam adipisci aliquid vitae quos quaerat velit veniam. Sint dicta quis ut voluptatem accusantium numquam neque. Aut temporibus perspiciatis asperiores repellat ullam laudantium nesciunt.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (65, 65, 'Natus accusantium ipsam et sed veniam amet. Cum quasi voluptatem rerum dolores rerum magnam porro aut. Eum id asperiores quasi quisquam. Quas et architecto dolores unde iure quis ut.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (66, 66, 'Maiores ducimus deserunt eos aliquam perspiciatis rerum. Id id quaerat magni repellendus ad facilis voluptatibus rerum.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (67, 67, 'Dolor consequatur earum ut quis et voluptates. Similique aperiam qui est est explicabo. Perspiciatis aperiam maiores eveniet quibusdam vitae est.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (68, 68, 'Tenetur assumenda deserunt molestiae. Consequatur corrupti ratione veniam atque molestias et. Ut vel tempore quia quidem. Ullam animi rerum molestias deserunt aut dolorem quia.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (69, 69, 'Excepturi laborum sit ut architecto error iste. Aliquid eum eligendi ut voluptatibus aut. Dolores rerum sint possimus. Velit debitis dolore reprehenderit qui.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (70, 70, 'Rem est dignissimos a aperiam exercitationem. Voluptatem incidunt ut libero. Sint dolor fugiat minus aspernatur occaecati provident.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (71, 71, 'Qui qui occaecati fugiat ipsam quasi alias. Qui pariatur placeat sit maxime. Quaerat velit natus sunt maiores assumenda atque.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (72, 72, 'Ut asperiores molestias voluptatibus sed odio magnam vel. Fuga et iusto sed ipsum. Nostrum maxime enim earum aperiam voluptatum.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (73, 73, 'Consequuntur eos qui sunt cum ut recusandae commodi. Sed recusandae est beatae totam quod. Officia vero facere doloribus iure. Dolorum iusto et voluptate sed.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (74, 74, 'Fugiat enim ipsa veritatis enim. Dolorem et omnis sed repellendus nihil. Expedita reprehenderit et corporis doloremque.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (75, 75, 'Nulla ut rerum doloremque ipsum expedita explicabo non. Nemo praesentium sed voluptate. Impedit sed facere culpa maiores. Quis perspiciatis voluptatem sit nam sint vero.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (76, 76, 'Rerum tempora inventore omnis. Eius eaque molestiae aut pariatur. Officiis eum quos est modi quia ad. Aliquam dolor voluptas doloribus dolor.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (77, 77, 'Corrupti fugiat quia vel qui temporibus aut. Eum repellat autem dolorum dolorum. Soluta consequatur et eum quia. Enim quod eum placeat veritatis consequatur placeat.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (78, 78, 'Perferendis quasi sint tenetur qui numquam quasi ut deserunt. Excepturi eum necessitatibus illum suscipit id sequi recusandae. Aliquid sunt culpa et sunt ad quia iste.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (79, 79, 'Fugit voluptas adipisci molestiae voluptate nisi quia sed. Deserunt et sed minus officiis neque. Et accusamus atque nesciunt architecto vitae consequatur non commodi.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (80, 80, 'Distinctio voluptas voluptas perspiciatis excepturi. Placeat beatae ut minima quis distinctio dolore. Corporis explicabo ab quod. Natus libero autem tempore soluta nobis.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (81, 81, 'Dicta vitae ea aut natus. Minus sit vitae cum quos ratione id. Ea explicabo est molestiae enim facilis harum. Aut est qui et quidem ipsa voluptate natus.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (82, 82, 'Ratione ut nobis voluptatem iure quasi officiis consequatur. Rerum nulla vel ut incidunt officiis ab. Aliquid non cumque cupiditate.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (83, 83, 'Vitae aut reiciendis dolores pariatur voluptatum. Aliquam et omnis architecto fugiat. Occaecati laudantium numquam eos non harum.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (84, 84, 'Qui excepturi quia ex dolorem voluptas voluptate. Ullam aliquam magni magni placeat.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (85, 85, 'Nisi culpa delectus tempore est voluptas vitae ipsum. Ipsa eum consequatur minima impedit.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (86, 86, 'Minus vero sed maxime libero aut. Quia exercitationem id cum quaerat. Sit in ut nemo facere eligendi. Rem itaque deleniti esse doloremque.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (87, 87, 'Dolorem voluptatum minus quas nam et et vel neque. Culpa et ex sit blanditiis. Modi tempore quia aut. Rerum optio aut qui.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (88, 88, 'Neque ullam omnis unde voluptatem quod. Qui in voluptate in possimus. Sed sint possimus reiciendis nulla. Omnis ex in magnam eius nobis.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (89, 89, 'Aliquid facilis deleniti fugiat et ducimus rerum qui. Ea et ad neque delectus. Quasi ducimus dolorem ut iure. Nemo consequatur ea dignissimos non.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (90, 90, 'Quod cumque rerum iste sed fugit. Enim odit et est molestiae. Repellat nihil commodi numquam delectus.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (91, 91, 'Aliquid culpa vitae repellat ut non laborum. Aperiam facilis quibusdam eveniet.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (92, 92, 'Aspernatur suscipit non nihil distinctio earum cum totam. Saepe laborum est fuga porro perferendis. Suscipit quia illo a molestias enim distinctio iusto.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (93, 93, 'Laborum suscipit et corrupti. Sit eligendi omnis ut sed. Dolores non non vero exercitationem sunt voluptatem sunt cum. Molestiae illo non animi ex consequuntur exercitationem.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (94, 94, 'Aliquam doloremque accusantium minus debitis laborum. Quos molestias rerum reprehenderit iure dolor blanditiis. Eligendi inventore deserunt est nam consequatur.', 1, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (95, 95, 'Voluptatem enim qui blanditiis omnis velit commodi. Velit id consequatur et ut quae. Quo quo explicabo iure ut dolor optio blanditiis.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (96, 96, 'Aut vel iure eaque necessitatibus ipsa. Tenetur rerum exercitationem nisi iure et. Esse quisquam ut rerum quidem autem qui. Expedita vel et ut.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (97, 97, 'Dolor aut ut voluptatem at doloremque ut corrupti. Consequatur ut temporibus expedita ab. Alias natus quis aut quia harum. Consequatur incidunt non velit sed commodi dicta.', 0, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (98, 98, 'Repudiandae maxime officia impedit ab et. Ea officiis molestiae ipsa accusantium impedit ut cumque. Quam sit rerum occaecati velit qui. Exercitationem et voluptas omnis.', 1, 0);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (99, 99, 'Voluptas quis exercitationem voluptas suscipit. Laborum ut fugit corrupti nisi voluptates qui. Quia quibusdam et quam ab provident minima eveniet.', 0, 1);
INSERT INTO `messages` (`id`, `chat_id`, `body`, `is_important`, `is_delivered`) VALUES (100, 100, 'Non atque molestias non enim inventore cum corporis. Quis dolores quia voluptatum id voluptatum aliquid. Et non molestiae suscipit.', 0, 0);


#
# TABLE STRUCTURE FOR: profiles
#

DROP TABLE IF EXISTS `profiles`;

CREATE TABLE `profiles` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Ссылка на пользователя',
  `gender` char(1) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Пол',
  `birthday` date DEFAULT NULL COMMENT 'Дата рождения',
  `photo_id` int(10) unsigned NOT NULL COMMENT 'Ссылка на основную фотографию пользователя',
  `city_id` int(10) unsigned NOT NULL COMMENT 'Город проживания',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `id_UNIQUE` (`user_id`),
  KEY `fk_cities_idx` (`city_id`),
  CONSTRAINT `fk_cities` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `fk_users` FOREIGN KEY (`city_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Профили';

INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (1, 'm', '2004-02-08', 1, 1, '1997-05-20 07:25:47', '1981-03-28 00:48:22');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (2, 'm', '2009-07-05', 2, 2, '2019-12-25 08:04:50', '1979-07-18 18:43:10');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (3, 'f', '1970-11-26', 3, 3, '2003-12-06 12:40:04', '1980-04-11 09:11:54');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (4, 'm', '1976-07-02', 4, 4, '1988-05-18 04:47:39', '1994-12-17 12:51:26');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (5, 'f', '1981-06-29', 5, 5, '1988-12-21 02:08:31', '1998-06-20 09:18:48');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (6, 'f', '1983-08-28', 6, 6, '1977-02-13 01:27:15', '1972-01-16 01:12:35');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (7, 'm', '2009-06-22', 7, 7, '1985-04-24 04:29:09', '1991-06-11 00:04:20');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (8, 'f', '1999-12-16', 8, 8, '2011-02-21 03:37:00', '1995-09-02 21:41:24');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (9, 'f', '1975-05-06', 9, 9, '1994-08-14 11:50:29', '1979-07-17 07:28:37');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (10, 'm', '1978-08-31', 10, 10, '2011-01-20 23:29:32', '2005-06-21 13:31:13');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (11, 'm', '2000-07-26', 11, 11, '1993-04-17 20:43:55', '2017-01-16 06:45:11');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (12, 'm', '2003-07-26', 12, 12, '1979-07-03 13:43:18', '2014-03-20 00:13:19');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (13, 'f', '2011-09-28', 13, 13, '1985-03-16 09:32:13', '2014-06-19 23:01:41');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (14, 'f', '2017-10-22', 14, 14, '1982-07-10 04:03:24', '1980-07-20 19:44:23');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (15, 'm', '2008-07-13', 15, 15, '2012-06-03 01:57:31', '2007-01-12 12:33:01');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (16, 'm', '2013-01-01', 16, 16, '2002-08-20 23:25:36', '2008-11-10 19:22:48');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (17, 'm', '2009-10-04', 17, 17, '1986-03-25 04:46:39', '2002-01-01 09:23:16');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (18, 'm', '1975-10-09', 18, 18, '2004-02-12 11:29:19', '1981-08-28 05:09:04');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (19, 'f', '1987-05-04', 19, 19, '1976-07-26 05:21:23', '1976-02-07 01:37:25');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (20, 'm', '1986-03-27', 20, 20, '2018-03-18 11:14:59', '2006-05-17 10:25:48');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (21, 'f', '1994-07-23', 21, 21, '2013-10-25 18:25:48', '2010-10-13 00:48:17');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (22, 'm', '1995-06-19', 22, 22, '1981-07-04 23:41:30', '1979-08-20 08:26:41');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (23, 'm', '2000-08-26', 23, 23, '1973-09-17 13:47:16', '1982-11-21 12:46:12');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (24, 'f', '2012-10-12', 24, 24, '1988-06-07 17:03:29', '1999-12-04 04:20:05');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (25, 'f', '2016-02-16', 25, 25, '1990-10-19 08:51:17', '2015-08-02 12:12:03');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (26, 'm', '2011-03-20', 26, 26, '1972-12-28 16:04:37', '1997-09-04 00:03:17');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (27, 'm', '1973-01-09', 27, 27, '2006-05-24 23:15:36', '1973-06-28 23:56:04');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (28, 'f', '2019-05-19', 28, 28, '1984-09-17 04:21:50', '1992-11-12 03:55:09');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (29, 'f', '1974-02-28', 29, 29, '2000-02-10 09:50:08', '1999-07-09 23:14:16');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (30, 'f', '2002-05-14', 30, 30, '2002-01-19 06:59:56', '1997-07-29 08:46:59');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (31, 'f', '1991-07-27', 31, 31, '2008-08-23 13:10:11', '2019-02-07 01:09:08');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (32, 'm', '1986-04-01', 32, 32, '1990-05-21 10:36:44', '1980-08-27 21:02:35');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (33, 'm', '2015-03-23', 33, 33, '1981-04-14 08:03:47', '2004-01-24 23:41:18');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (34, 'm', '1971-10-21', 34, 34, '2002-09-24 22:42:35', '2017-01-01 06:00:13');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (35, 'm', '1999-02-08', 35, 35, '2008-08-17 04:34:34', '1972-08-16 09:47:55');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (36, 'm', '2012-01-26', 36, 36, '1988-04-19 20:46:47', '1986-06-19 04:23:32');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (37, 'm', '1972-03-11', 37, 37, '1976-06-20 03:12:10', '1978-07-29 10:48:16');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (38, 'f', '1981-09-21', 38, 38, '1985-01-06 08:22:16', '2000-08-11 14:48:12');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (39, 'm', '2014-06-13', 39, 39, '1973-11-17 21:15:25', '1976-07-08 15:32:09');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (40, 'm', '1993-11-05', 40, 40, '2009-11-25 22:37:09', '1982-03-02 10:15:24');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (41, 'f', '1986-02-25', 41, 41, '2008-11-28 03:02:44', '1992-05-09 22:12:00');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (42, 'f', '1992-09-08', 42, 42, '1973-10-08 14:05:34', '2018-09-06 11:10:12');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (43, 'f', '2019-08-23', 43, 43, '2019-07-25 21:40:45', '2017-01-03 15:11:18');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (44, 'm', '2009-12-02', 44, 44, '2001-06-13 05:40:55', '2004-07-11 23:24:11');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (45, 'f', '1993-08-14', 45, 45, '2009-10-27 10:14:50', '1989-07-04 19:09:35');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (46, 'f', '1997-05-10', 46, 46, '1991-09-07 09:27:19', '2004-11-15 04:49:53');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (47, 'm', '1985-06-24', 47, 47, '1978-09-26 04:58:41', '1997-03-18 05:43:36');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (48, 'm', '2002-09-27', 48, 48, '1983-03-19 05:38:47', '2014-03-13 18:16:19');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (49, 'f', '1988-06-21', 49, 49, '2006-01-22 14:32:47', '2013-11-25 01:57:23');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (50, 'm', '2000-04-21', 50, 50, '2011-01-21 04:55:48', '2011-03-18 16:37:44');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (51, 'm', '1979-11-18', 51, 51, '2000-01-17 12:56:54', '1990-10-06 07:23:20');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (52, 'f', '2003-10-04', 52, 52, '2000-08-04 16:54:16', '1977-02-24 12:35:30');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (53, 'm', '1994-01-23', 53, 53, '2005-03-19 00:07:13', '1998-03-20 22:41:54');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (54, 'f', '2010-02-08', 54, 54, '1983-09-22 12:12:11', '2001-02-06 18:03:29');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (55, 'f', '2005-08-16', 55, 55, '2007-11-13 01:14:48', '2018-03-09 21:42:40');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (56, 'm', '1988-02-08', 56, 56, '1977-12-02 23:52:56', '2006-11-23 13:15:02');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (57, 'm', '1972-07-04', 57, 57, '1971-01-18 04:55:45', '1991-04-05 08:33:15');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (58, 'm', '1999-07-26', 58, 58, '1982-08-19 10:02:36', '1987-03-16 08:56:29');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (59, 'f', '1973-09-18', 59, 59, '2000-08-27 15:38:40', '1984-10-27 20:00:04');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (60, 'f', '1987-09-21', 60, 60, '1974-08-20 23:07:36', '2007-07-17 21:42:33');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (61, 'm', '1970-10-25', 61, 61, '2004-08-11 13:06:08', '1996-06-20 06:49:01');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (62, 'm', '2009-06-18', 62, 62, '2002-09-22 15:26:35', '2014-06-27 17:12:08');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (63, 'm', '1978-03-10', 63, 63, '2012-03-29 02:38:19', '2013-07-31 08:18:30');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (64, 'm', '2006-01-07', 64, 64, '1995-08-22 15:15:50', '1976-11-27 13:32:40');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (65, 'm', '1985-11-11', 65, 65, '1975-12-01 12:31:47', '2003-11-05 15:59:50');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (66, 'm', '2018-08-25', 66, 66, '1992-08-16 18:14:40', '2009-06-22 01:27:33');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (67, 'f', '1976-03-15', 67, 67, '2005-11-16 17:15:17', '1987-06-23 17:46:45');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (68, 'f', '2005-08-24', 68, 68, '1995-02-22 12:04:46', '1994-11-28 21:40:12');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (69, 'm', '1983-05-21', 69, 69, '1974-11-06 02:02:40', '1982-11-19 00:28:13');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (70, 'm', '1997-08-01', 70, 70, '1991-08-21 23:15:59', '1997-01-26 11:17:02');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (71, 'm', '1980-12-28', 71, 71, '1994-01-03 15:37:35', '1992-05-03 06:36:22');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (72, 'm', '1991-11-15', 72, 72, '1997-01-16 12:51:22', '2015-03-01 13:42:03');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (73, 'm', '1999-08-19', 73, 73, '1989-12-22 16:46:40', '1990-08-09 17:53:23');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (74, 'f', '1982-08-13', 74, 74, '1971-06-01 02:17:39', '1990-01-07 22:08:03');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (75, 'm', '2002-04-13', 75, 75, '1990-01-13 10:05:29', '2010-08-02 21:27:43');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (76, 'm', '1997-06-27', 76, 76, '1991-08-25 10:07:27', '1987-10-20 09:42:56');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (77, 'f', '1973-12-23', 77, 77, '1974-08-15 20:33:35', '1990-11-08 10:23:17');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (78, 'f', '1994-03-20', 78, 78, '1971-05-13 07:53:28', '2010-12-25 12:29:04');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (79, 'f', '1983-02-20', 79, 79, '2018-06-30 19:43:15', '1995-12-13 14:59:19');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (80, 'm', '1977-11-12', 80, 80, '1979-07-04 04:20:50', '1985-08-07 19:43:59');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (81, 'f', '2017-09-14', 81, 81, '1996-09-24 13:29:53', '1984-12-13 13:15:23');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (82, 'm', '1986-11-29', 82, 82, '2001-11-23 11:54:30', '1974-04-17 01:11:46');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (83, 'f', '1991-03-01', 83, 83, '1976-09-25 06:58:40', '1971-09-08 03:03:00');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (84, 'm', '1980-01-04', 84, 84, '2012-03-25 23:09:00', '1982-08-18 07:26:34');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (85, 'f', '2003-09-11', 85, 85, '2009-04-23 07:18:22', '2018-10-27 15:59:46');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (86, 'm', '2007-02-05', 86, 86, '2013-02-19 01:00:02', '1985-06-27 14:04:32');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (87, 'm', '1987-11-21', 87, 87, '1995-07-29 04:00:04', '2016-01-15 11:28:40');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (88, 'm', '2013-08-09', 88, 88, '2018-07-20 16:24:32', '1972-11-09 21:22:07');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (89, 'f', '1979-09-04', 89, 89, '1997-05-02 16:11:14', '1995-06-16 19:23:38');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (90, 'f', '1995-04-05', 90, 90, '2008-09-21 17:46:20', '1973-10-02 08:20:20');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (91, 'm', '2001-09-12', 91, 91, '1996-06-04 18:36:41', '1977-10-28 21:34:33');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (92, 'm', '1976-10-14', 92, 92, '1992-12-12 12:40:29', '1977-04-15 18:24:51');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (93, 'm', '1977-05-13', 93, 93, '1998-01-24 06:49:05', '1994-01-14 12:05:15');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (94, 'm', '2000-01-23', 94, 94, '1976-12-04 13:24:34', '1983-07-30 21:49:54');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (95, 'f', '1976-09-08', 95, 95, '1974-02-03 06:48:35', '1994-12-17 09:00:15');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (96, 'f', '1989-05-04', 96, 96, '1995-11-15 21:27:38', '1990-01-02 08:30:16');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (97, 'm', '1999-04-19', 97, 97, '1992-03-20 20:22:55', '1974-05-14 22:11:50');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (98, 'f', '2000-01-22', 98, 98, '2019-10-18 17:00:46', '1987-11-02 08:45:52');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (99, 'f', '1980-02-20', 99, 99, '1983-03-13 03:01:12', '1978-02-10 00:46:00');
INSERT INTO `profiles` (`user_id`, `gender`, `birthday`, `photo_id`, `city_id`, `created_at`, `updated_at`) VALUES (100, 'f', '1993-07-15', 100, 100, '2020-04-29 22:54:15', '1974-09-04 17:44:04');


#
# TABLE STRUCTURE FOR: users
#

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор строки',
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Имя пользователя',
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Фамилия пользователя',
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Почта',
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Телефон',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`),
  KEY `name` (`first_name`,`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Пользователи';

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (1, 'Aurelia', 'Durgan', 'mavis.bahringer@example.net', '1-052-090-8144x952', '2006-02-06 06:52:08', '1994-09-04 16:58:02');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (2, 'Meggie', 'Leffler', 'gbruen@example.org', '427.053.6744', '1984-05-27 15:15:59', '2011-01-28 11:46:25');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (3, 'Adriel', 'Farrell', 'alfreda48@example.net', '1-854-359-4753x72119', '1972-02-14 14:27:03', '1976-03-19 18:17:06');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (4, 'Trevion', 'Abbott', 'morar.darwin@example.org', '770-620-4619x4016', '1994-11-14 16:21:37', '1992-09-10 16:56:24');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (5, 'Shea', 'Crooks', 'nhodkiewicz@example.org', '1-653-657-4537x168', '1987-04-27 15:19:59', '2015-12-03 23:20:52');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (6, 'Jordy', 'Gorczany', 'maybelle56@example.com', '986.831.6001x937', '1978-02-22 08:09:48', '1995-06-08 22:02:51');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (7, 'Edwardo', 'Jerde', 'riley70@example.org', '09547575255', '1978-04-06 22:43:01', '2015-09-21 06:21:59');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (8, 'Jamir', 'Halvorson', 'maeve.price@example.com', '656-349-3991', '1989-02-08 07:22:57', '1990-09-07 20:22:48');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (9, 'Tristian', 'Rice', 'shirley.fritsch@example.net', '03672707201', '1995-03-28 18:47:32', '2015-02-12 11:24:41');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (10, 'Kasandra', 'Nitzsche', 'cristina32@example.com', '(074)720-8459', '1987-07-05 20:15:11', '2004-08-05 06:06:52');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (11, 'Alia', 'Macejkovic', 'ezekiel88@example.org', '1-464-315-2761', '2002-03-15 22:03:56', '1979-12-04 09:22:17');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (12, 'Billie', 'Emard', 'destin67@example.net', '069-389-4570', '1977-01-08 04:37:15', '2020-06-20 17:30:41');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (13, 'Leatha', 'Stark', 'xrath@example.com', '(974)863-0703', '2010-03-17 03:45:21', '2001-10-09 11:35:05');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (14, 'Arlene', 'Marvin', 'keely.conn@example.net', '1-681-843-3458x797', '1982-09-10 06:17:44', '2010-06-03 00:39:16');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (15, 'Trace', 'Reynolds', 'erosenbaum@example.net', '(004)723-2622', '2006-07-30 04:16:47', '2018-02-17 16:10:26');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (16, 'Danial', 'Nolan', 'shad91@example.net', '(673)950-6875', '1972-11-01 01:32:15', '1973-01-01 01:02:10');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (17, 'Joshuah', 'Doyle', 'cruickshank.pearline@example.net', '1-044-841-9269x63361', '2016-10-28 15:49:48', '2004-02-29 01:42:57');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (18, 'Aaron', 'Legros', 'larue.daniel@example.net', '(742)198-5728x25420', '2019-01-25 16:19:21', '2011-12-04 09:20:22');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (19, 'Antoinette', 'Waelchi', 'delaney.grant@example.net', '859-354-2668', '1981-10-05 20:21:58', '2016-03-04 11:06:13');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (20, 'Tatum', 'Kihn', 'reinger.ashleigh@example.org', '939.916.2336x88978', '2006-09-28 13:26:50', '1975-07-09 17:51:47');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (21, 'Etha', 'Gutmann', 'euna.cormier@example.com', '360.090.8131x245', '2003-05-18 07:27:38', '1986-03-16 16:19:41');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (22, 'Abbie', 'Schuster', 'stokes.pablo@example.org', '(624)096-1991x962', '1981-05-17 21:34:38', '2016-06-25 05:26:22');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (23, 'Isadore', 'Monahan', 'giovanni01@example.org', '1-713-540-2773x3730', '2010-11-12 19:30:48', '2002-11-18 12:26:05');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (24, 'Bethel', 'O\'Conner', 'misty.krajcik@example.org', '1-285-550-7259x7679', '2020-02-16 13:28:53', '1998-08-04 22:26:46');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (25, 'Celine', 'Welch', 'leonie.schumm@example.org', '1-025-622-9754x2679', '1991-03-12 20:54:25', '1991-09-02 18:04:16');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (26, 'Lincoln', 'Schulist', 'nathanial37@example.com', '729.485.3564', '1981-10-11 10:53:29', '2019-04-06 08:20:01');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (27, 'Bella', 'Bartoletti', 'lbechtelar@example.org', '038.552.7881', '1982-12-28 18:51:11', '1989-09-08 14:31:05');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (28, 'Aron', 'Langworth', 'vdaugherty@example.com', '04925945524', '1998-03-26 21:21:35', '2006-08-17 05:14:14');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (29, 'Alessandra', 'Farrell', 'mbrakus@example.net', '(989)828-7917x8592', '1995-10-22 20:05:18', '1970-02-08 19:54:42');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (30, 'Fern', 'Abbott', 'zledner@example.com', '(508)733-6509x381', '1988-02-01 21:14:37', '1979-10-10 21:38:05');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (31, 'Merritt', 'Fadel', 'brendan52@example.com', '223-787-8126x05222', '2012-11-23 02:37:36', '2009-05-05 01:22:29');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (32, 'Roxane', 'Kshlerin', 'evan.langosh@example.net', '(758)087-1383x03512', '1995-12-05 06:27:36', '2011-06-21 02:53:22');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (33, 'Hilario', 'Bailey', 'vthompson@example.org', '692.114.4128', '1999-12-23 02:21:44', '2016-11-24 03:18:47');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (34, 'Titus', 'Cummings', 'tyree.waters@example.com', '254.408.4440', '2009-11-29 12:25:13', '1986-02-05 16:52:29');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (35, 'Aletha', 'Moore', 'hipolito.graham@example.com', '(447)992-5422', '2006-11-11 20:34:04', '2004-08-09 11:01:27');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (36, 'Arielle', 'Weimann', 'giles44@example.org', '089.973.8215x080', '2000-02-27 03:10:00', '1987-10-27 16:50:51');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (37, 'Tiffany', 'Hills', 'murazik.delphine@example.org', '384-140-1855x12702', '1975-09-29 07:04:58', '1978-10-08 02:58:03');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (38, 'Garland', 'Baumbach', 'jasper.moore@example.net', '409-023-6352x40039', '1997-04-06 17:15:54', '1990-12-02 08:17:20');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (39, 'Aiyana', 'Effertz', 'amos.marvin@example.net', '1-657-271-4490x1193', '1984-09-21 08:27:18', '2015-06-05 04:57:55');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (40, 'Meaghan', 'Moen', 'vincent55@example.com', '282.394.8010x467', '2010-05-02 05:42:48', '2002-10-29 13:13:25');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (41, 'Madge', 'Jakubowski', 'qbradtke@example.com', '(852)637-5867', '2002-10-23 18:07:23', '1970-06-28 23:13:57');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (42, 'Cloyd', 'Schamberger', 'nettie52@example.net', '1-596-597-0654x5862', '2017-04-13 18:15:37', '2014-11-18 07:09:19');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (43, 'Georgiana', 'Gorczany', 'mclaughlin.patience@example.com', '(877)581-4615x49724', '2003-07-28 18:12:15', '1994-04-04 03:03:32');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (44, 'Ransom', 'Wiegand', 'pearl.herman@example.net', '(132)355-0707x772', '2006-06-10 23:08:13', '1977-08-27 14:57:45');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (45, 'Kayla', 'Keeling', 'janessa.hane@example.org', '+66(2)3349111261', '2012-09-26 10:11:01', '1993-12-10 22:47:14');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (46, 'Delfina', 'Quigley', 'hyman16@example.com', '532-311-9636', '2005-12-22 06:21:04', '1987-02-15 12:56:34');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (47, 'Foster', 'Buckridge', 'weldon93@example.net', '(975)886-8516x99982', '2000-07-02 17:23:10', '1986-11-26 15:14:07');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (48, 'Charlie', 'Johnston', 'freeda.kshlerin@example.net', '146.754.6419x228', '2004-03-16 08:10:03', '1972-12-07 09:19:54');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (49, 'Karine', 'Spencer', 'zchristiansen@example.org', '(328)500-5441', '2009-04-07 03:09:00', '2019-10-17 08:03:22');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (50, 'Bobbie', 'Haag', 'isom41@example.org', '252.280.7428x2297', '2013-05-30 14:24:00', '2000-09-16 09:19:11');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (51, 'Clementine', 'Stroman', 'gulgowski.carmine@example.com', '084-507-0721x1700', '1985-03-23 22:44:19', '1989-06-13 12:47:07');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (52, 'Gabriella', 'Kovacek', 'lexus.turner@example.org', '1-482-219-3629x229', '1971-08-23 11:33:25', '1972-01-10 09:36:44');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (53, 'Hilario', 'Gutkowski', 'eparisian@example.net', '425-800-7501x61030', '1992-09-20 11:49:41', '1997-04-10 01:33:44');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (54, 'Cecil', 'Gerlach', 'cristobal30@example.com', '03271288353', '1999-12-04 15:29:50', '2015-09-11 14:50:25');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (55, 'Ursula', 'Quitzon', 'ystroman@example.net', '581.225.8363x397', '1996-07-10 18:00:35', '1981-08-07 07:56:52');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (56, 'Darius', 'Kirlin', 'kjones@example.com', '504.892.8682x1784', '1978-08-24 11:03:39', '1998-10-19 09:10:48');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (57, 'Brittany', 'Collins', 'keaton17@example.org', '(747)796-9580', '1982-06-28 08:20:54', '2020-03-24 00:32:43');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (58, 'Ryley', 'O\'Connell', 'rgerlach@example.net', '(178)129-8889', '1995-05-02 07:51:42', '1974-12-07 19:59:21');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (59, 'Omari', 'Towne', 'odenesik@example.org', '(860)916-4339x4371', '1975-09-25 08:23:10', '2003-06-26 01:02:15');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (60, 'Alexandria', 'Dare', 'kylee64@example.net', '812.959.2280x9359', '2003-04-16 14:46:57', '1991-11-09 18:26:12');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (61, 'Anabel', 'Bradtke', 'emma38@example.org', '(080)698-9003', '1991-07-04 05:39:45', '1993-10-26 06:53:21');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (62, 'Euna', 'Leannon', 'wolff.charity@example.org', '459.226.2069x218', '1989-11-25 11:37:00', '1980-04-03 21:35:09');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (63, 'Thurman', 'Glover', 'horace.heaney@example.com', '101-105-0076', '1991-09-08 23:19:06', '2010-02-15 01:00:12');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (64, 'Nash', 'Hermiston', 'natalie64@example.com', '962-667-2653x211', '2020-06-03 20:19:44', '1978-11-03 00:48:43');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (65, 'Davin', 'Bode', 'kilback.sydnie@example.com', '998-748-5016x427', '1986-02-09 14:59:20', '2008-01-05 07:41:30');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (66, 'Oral', 'Barton', 'laurence.ziemann@example.org', '1-083-276-8964x76610', '2011-01-17 21:20:28', '1970-05-05 20:48:50');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (67, 'Elda', 'Schmidt', 'jerrell.ruecker@example.org', '708.328.0228', '1985-08-01 23:15:48', '1975-03-07 21:30:38');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (68, 'Trystan', 'Kunze', 'kennedi.schneider@example.com', '359.501.1710x296', '2005-12-01 05:07:45', '1975-07-02 04:09:55');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (69, 'Madisyn', 'Dietrich', 'fswift@example.net', '1-391-558-7678', '1973-09-23 05:16:47', '1976-06-22 15:22:24');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (70, 'Marcus', 'Welch', 'rachael.dibbert@example.com', '069-771-7744', '1972-06-02 15:33:53', '1986-01-11 17:02:43');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (71, 'Leopold', 'Stracke', 'heber.kozey@example.com', '845-392-3458x354', '1988-01-25 23:14:38', '2010-02-11 13:19:01');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (72, 'Charity', 'Schoen', 'karina67@example.net', '194.727.6489', '1996-11-04 13:48:37', '2008-07-01 17:07:08');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (73, 'Jammie', 'Larkin', 'noemi.upton@example.com', '(716)185-2694', '1998-12-03 18:44:09', '1994-12-30 02:21:15');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (74, 'Garland', 'Lowe', 'mertz.krystel@example.com', '587-553-4019x823', '1980-10-12 04:01:19', '2020-01-13 11:59:58');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (75, 'Walker', 'Ratke', 'schuster.noemie@example.org', '586-388-9305', '1975-07-09 03:03:45', '1977-10-10 14:53:50');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (76, 'Imogene', 'Kshlerin', 'carolyne.johns@example.com', '1-459-349-5402x077', '2005-10-06 14:59:21', '1974-11-25 02:42:46');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (77, 'Briana', 'Nienow', 'zboncak.alyson@example.com', '+63(7)0830816649', '2018-05-07 22:19:22', '2003-01-08 10:39:01');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (78, 'Callie', 'Williamson', 'christy.von@example.org', '(835)548-3727x82357', '2001-08-19 09:10:24', '1993-05-29 19:38:42');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (79, 'Billy', 'Aufderhar', 'tremblay.karine@example.org', '100-273-6463', '1989-10-13 04:50:26', '1987-08-26 03:06:35');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (80, 'Willow', 'Terry', 'ihegmann@example.com', '1-153-198-4690', '2019-05-04 08:48:00', '1976-09-13 02:59:34');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (81, 'Zetta', 'Zboncak', 'mheathcote@example.com', '084-469-2091', '2003-11-04 10:38:31', '1987-01-29 17:17:00');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (82, 'Colt', 'Hegmann', 'rratke@example.com', '502.950.8112', '2013-04-30 21:37:16', '1982-12-07 01:41:04');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (83, 'Stanley', 'Hilpert', 'maria.abbott@example.net', '436.715.2985', '1973-06-17 15:22:11', '1972-11-14 18:43:47');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (84, 'Gordon', 'Lehner', 'alize66@example.com', '1-408-534-7762', '1977-11-25 23:07:05', '1999-04-14 16:53:10');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (85, 'Jada', 'Hilll', 'stamm.lucious@example.net', '(882)700-2806x617', '1987-04-05 14:25:25', '1997-03-10 08:31:33');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (86, 'Alda', 'Yundt', 'sidney.waters@example.com', '(601)506-8670', '1995-09-22 03:15:43', '1981-08-04 11:20:20');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (87, 'Tess', 'Bernhard', 'sigurd.stark@example.net', '912-384-8297x8361', '1991-05-31 05:22:30', '2007-03-14 20:50:36');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (88, 'Savion', 'Yost', 'ogleason@example.com', '(868)218-2283x078', '2017-03-03 17:27:57', '2017-11-20 18:42:50');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (89, 'Merl', 'Krajcik', 'jaylan27@example.net', '490.764.4361x8302', '2002-01-26 08:11:04', '2004-11-29 01:24:58');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (90, 'Clyde', 'Mayert', 'nkuvalis@example.net', '132-649-1010', '1982-07-24 13:09:02', '2011-09-13 23:01:12');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (91, 'Helena', 'White', 'isabell.schroeder@example.net', '(778)868-9956x05759', '1988-04-13 02:40:20', '1976-05-22 08:42:55');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (92, 'Sandra', 'McClure', 'dryan@example.org', '1-606-538-1127x29185', '2012-08-14 13:14:09', '1987-03-25 07:59:01');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (93, 'Neha', 'King', 'ewell.west@example.com', '1-027-020-1114x00115', '2009-06-18 13:46:31', '2017-06-13 20:38:12');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (94, 'Maeve', 'Kirlin', 'ylangosh@example.net', '1-097-397-0918', '1971-05-17 12:27:32', '1977-02-06 18:29:54');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (95, 'Bria', 'Okuneva', 'wolff.rubye@example.net', '1-601-907-7163', '1973-09-27 22:16:33', '2017-05-03 04:44:39');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (96, 'Chesley', 'Klocko', 'will.jayne@example.org', '1-200-710-4906x04657', '2005-01-23 05:35:46', '1987-08-25 01:23:04');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (97, 'Leonora', 'Gibson', 'vgaylord@example.com', '1-732-447-2817x226', '1989-04-22 21:13:52', '1983-08-23 04:26:18');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (98, 'Martina', 'Greenholt', 'halvorson.forest@example.com', '1-883-142-6602', '1974-04-09 16:27:03', '2004-08-02 13:50:43');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (99, 'Antwan', 'Sauer', 'tremaine51@example.com', '1-822-948-0487', '1971-03-30 20:10:03', '1999-11-24 06:14:01');
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `created_at`, `updated_at`) VALUES (100, 'Joanne', 'Will', 'emmerich.margarete@example.net', '1-691-479-6629x6220', '2005-02-23 11:03:21', '1990-02-22 01:45:32');


