CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор строки',
  `first_name` varchar(100) NOT NULL COMMENT 'Имя пользователя',
  `last_name` varchar(100) NOT NULL COMMENT 'Фамилия пользователя',
  `email` varchar(100) NOT NULL COMMENT 'Почта',
  `phone` varchar(100) NOT NULL COMMENT 'Телефон',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата изменения',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`),
  KEY `name` (`first_name`,`last_name`)
) COMMENT='Пользователи';

CREATE TABLE `countries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(100) NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) COMMENT='Страны';

CREATE TABLE `cities` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(100) NOT NULL COMMENT 'Наименование',
  `country_id` int unsigned NOT NULL COMMENT 'Дата создания',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_countries_1_idx` (`country_id`),
  CONSTRAINT `fk_countries_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) COMMENT 'Города';

CREATE TABLE `profiles` (
  `user_id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'Ссылка на пользователя',
  `gender` char(1) NOT NULL COMMENT 'Пол',
  `birthday` date DEFAULT NULL COMMENT 'Дата рождения',
  `photo_id` int unsigned NOT NULL COMMENT 'Ссылка на основную фотографию пользователя',
  `city_id` int unsigned NOT NULL COMMENT 'Город проживания',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `id_UNIQUE` (`user_id`),
  KEY `fk_cities_idx` (`city_id`),
  CONSTRAINT `fk_cities` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `fk_users` FOREIGN KEY (`city_id`) REFERENCES `users` (`id`)
) COMMENT='Профили';

CREATE TABLE `chats` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `name` varchar(255) NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`id`)
) COMMENT='Чаты';

CREATE TABLE `chat_history` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `chat_id` int unsigned NOT NULL COMMENT 'ИД чата',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `is_active` tinyint NOT NULL COMMENT 'Последняя ли запись по данному чату',
  PRIMARY KEY (`id`),
  KEY `fk_chat_idx` (`chat_id`),
  KEY `fk_users_idx` (`user_id`),
  CONSTRAINT `fk_chat_history_chat` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`),
  CONSTRAINT `fk_chat_history_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) COMMENT='История чата';


CREATE TABLE `messages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `chat_id` int unsigned NOT NULL COMMENT 'ИД чата',
  `body` text NOT NULL COMMENT 'Тело',
  `is_important` tinyint NOT NULL COMMENT 'Важность',
  `is_delivered` tinyint NOT NULL COMMENT 'Доставлено',
  PRIMARY KEY (`id`),
  KEY `fk_messages_chat_idx` (`chat_id`),
  CONSTRAINT `fk_messages_chat` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`)
);

CREATE TABLE `message_history` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `id_message` int unsigned NOT NULL COMMENT 'ИД сообщения',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи',
  `id_user` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `is_active` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_history_message_idx` (`id_message`),
  KEY `fk_message_history_user_idx` (`id_user`),
  CONSTRAINT `fk_message_history_message` FOREIGN KEY (`id_message`) REFERENCES `messages` (`id`),
  CONSTRAINT `fk_message_history_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) COMMENT='История сообщений';

CREATE TABLE `media` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор строки',
  `user_id` int unsigned NOT NULL COMMENT 'Ссылка на пользователя, который загрузил файл',
  `filename` varchar(255) NOT NULL COMMENT 'Путь к файлу',
  `size` int NOT NULL COMMENT 'Размер файла',
  `metadata` json DEFAULT NULL COMMENT 'Метаданные файла',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания строки',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время обновления строки',
  PRIMARY KEY (`id`),
  KEY `fk_media_users_idx` (`user_id`),
  CONSTRAINT `fk_media_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) COMMENT='Медиафайлы';

CREATE TABLE `friendship_statuses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор строки',
  `name` varchar(100) NOT NULL COMMENT 'Название статуса',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания строки',
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время обновления строки',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) COMMENT='Статусы дружбы';

CREATE TABLE `friendship` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int unsigned NOT NULL COMMENT 'Ссылка на инициатора дружеских отношений',
  `friend_id` int unsigned NOT NULL COMMENT 'Ссылка на получателя приглашения дружить',
  `status_id` int unsigned NOT NULL COMMENT 'Ссылка на статус (текущее состояние) отношений',
  `requested_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время отправления приглашения дружить',
  `confirmed_at` datetime DEFAULT NULL COMMENT 'Время подтверждения приглашения',
  PRIMARY KEY (`id`),
  KEY `fk_friendship_user_idx` (`user_id`),
  KEY `fk_friendship_friend_idx` (`friend_id`),
  KEY `fk_friendship_statuses_idx` (`status_id`),
  CONSTRAINT `fk_friendship_friend` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_friendship_statuses` FOREIGN KEY (`status_id`) REFERENCES `friendship_statuses` (`id`),
  CONSTRAINT `fk_friendship_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) COMMENT='Таблица дружбы';

CREATE TABLE `communities` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор сроки',
  `name` varchar(150) NOT NULL COMMENT 'Название группы',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) COMMENT='Группы';

CREATE TABLE `community_history` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'ИД',
  `user_id` int unsigned NOT NULL COMMENT 'ИД пользователя',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания записи',
  `community_id` int unsigned NOT NULL COMMENT 'ИД группы',
  `is_active` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_community_history_user_idx` (`user_id`),
  KEY `fk_community_history_community_idx` (`community_id`),
  CONSTRAINT `fk_community_history_community` FOREIGN KEY (`community_id`) REFERENCES `communities` (`id`),
  CONSTRAINT `fk_community_history_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
);

CREATE TABLE `communities_users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int unsigned NOT NULL COMMENT 'Ссылка на группу',
  `user_id` int unsigned NOT NULL COMMENT 'Ссылка на пользователя',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания строки',
  PRIMARY KEY (`id`),
  KEY `fk_communities_users_idx` (`user_id`),
  KEY `fk_communities_users_comm_idx` (`community_id`),
  CONSTRAINT `fk_communities_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_communities_users_comm` FOREIGN KEY (`community_id`) REFERENCES `communities` (`id`)
) COMMENT='Участники групп, связь между пользователями и группами';




