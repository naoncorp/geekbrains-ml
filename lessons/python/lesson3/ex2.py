"""

2. Реализовать функцию, принимающую несколько параметров, описывающих данные пользователя:

имя, фамилия, год рождения, город проживания, email, телефон.

Функция должна принимать параметры как именованные аргументы. Реализовать вывод данных о пользователе одной строкой.

"""


class Person:

    def __init__(self, name: str, last_name: str, year_of_birthday: int, city: str, email: str, phone: str):
        """
        Конструктор будет вместо функции по заданию
        :param name:
        :param last_name:
        :param year_of_birthday:
        :param city:
        :param email:
        :param phone: пусть будет строкой пока
        """
        self.name = name
        self.last_name = last_name
        self.year_of_birthday = year_of_birthday
        self.city = city
        self.email = email
        self.phone = phone

    def get(self):
        print(
            f"Имя: {self.name}, Фамилия: {self.last_name}, Год рождения: {self.year_of_birthday}, Город: {self.city}, "
            f"Email: {self.email}, Телефон: {self.phone}")


p = Person(name="Дмитрий", last_name="Никитин", year_of_birthday="1991", city="Тольятти", email="test@mail.ru",
           phone="844995648945")

p.get()
