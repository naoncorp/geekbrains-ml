"""

1. Реализовать функцию, принимающую два числа (позиционные аргументы) и выполняющую их деление.
Числа запрашивать у пользователя, предусмотреть обработку ситуации деления на ноль.

"""


def divide_numbers(a: int, b: int):
    """ функция деления чисел """
    try:
        return a / b
    except ZeroDivisionError as ex:
        print(f"Ошибка деления на 0: {ex}")
    except Exception as ex:
        print(f"Непредвиденная ошибка: {ex}")


while True:
    a = input("Введите первое число:\n")
    b = input("Введите второе число:\n")

    if not a.isdigit() or not b.isdigit():
        print("Вы ввели неверные значения")
        continue

    res = divide_numbers(int(a), int(b))
    print(f"Результат: {res}\n")

