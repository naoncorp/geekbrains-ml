"""

3. Реализовать функцию my_func(), которая принимает три позиционных аргумента, и возвращает сумму наибольших двух аргументов.

"""


def my_func(a: int, b: int, c: int):
    my_list = [a, b, c]
    my_list.remove(min(my_list))
    print(my_list[0] + my_list[1])


my_func(4, 2, 6)