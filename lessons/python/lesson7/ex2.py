"""

Реализовать проект расчета суммарного расхода ткани на производство одежды.
Основная сущность (класс) этого проекта — одежда, которая может иметь определенное название.
К типам одежды в этом проекте относятся пальто и костюм.
У этих типов одежды существуют параметры: размер (для пальто) и рост (для костюма). Это могут быть обычные числа: V и H,
соответственно.
Для определения расхода ткани по каждому типу одежды использовать формулы: для пальто (V/6.5 + 0.5), для костюма (2*H + 0.3).
Проверить работу этих методов на реальных данных.
Реализовать общий подсчет расхода ткани. Проверить на практике полученные на этом уроке знания: реализовать абстрактные
классы для основных классов проекта, проверить на практике работу декоратора @property.


"""


from abc import ABC, abstractmethod


class Clothes(ABC):

    def __init__(self, name):
        self.name = name

    @property
    def get_value(self):
        pass

    @abstractmethod
    def calculate_consumption(self) -> float:
        pass


class Coat(Clothes):

    def __init__(self, name, size):
        self.size = size
        super().__init__(name)

    def get_value(self):
        return self.size

    def calculate_consumption(self):
        return self.get_value()/6.5 * 0.5


class Costume(Clothes):

    def __init__(self, name, weight):
        self.weight = weight
        super().__init__(name)

    def get_value(self):
        return self.weight

    def calculate_consumption(self):
        return 2*self.get_value() + 0.3


coat = Coat("coat", 5)
costume = Costume("costume", 3)

print(coat.calculate_consumption())
print(costume.calculate_consumption())