"""
1. Поработайте с переменными, создайте несколько, выведите на экран, запросите у пользователя несколько чисел и строк и сохраните в переменные, выведите на экран.
"""

import modules.float_custom as float_custom

boxCount = 5
print("Всего коробок: ", boxCount)
name = input("Введите ваше имя:\n")
box_price = input("Введите стоимость одной коробки:\n")
box_price = box_price.replace(",", ".")  # жизнь боль :)
result = 0

if float_custom.isfloat(box_price):
    result = boxCount * float(box_price)
    print(f"{name} Итоговая сумма: {result}")
else:
    print("Ошибка! Введенное значение не является числом")
