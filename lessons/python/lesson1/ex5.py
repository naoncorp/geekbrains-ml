"""

5. Запросите у пользователя значения выручки и издержек фирмы.
Определите, с каким финансовым результатом работает фирма (прибыль — выручка больше издержек, или убыток — издержки больше выручки).

Выведите соответствующее сообщение.
Если фирма отработала с прибылью, вычислите рентабельность выручки (соотношение прибыли к выручке).

Далее запросите численность сотрудников фирмы и определите прибыль фирмы в расчете на одного сотрудника.

"""

import modules.float_custom as float_custom

# выручка
revenue = input("Введите выручку\n")

# издержки
costs = input("Введите сумму издержек\n")

if float_custom.isfloat(revenue) and float_custom.isfloat(costs):

    float_revenue = float(revenue)
    float_costs = float(costs)

    if float_revenue > float_costs:
        print("Вы работаете в прибыль")

        # рентабельность
        profitability = (float_revenue - float_costs) / float_revenue
        print(f"Рентабельность: {profitability}")

        staff_count = input("Введите кол-во сотрудников:\n")
        if staff_count.isdigit():
            if int(staff_count) <= 0:
                print("У вас не может быть 0 или меньше сотрудников")
                exit()

            # Хз правильно ли понял ТЗ, время - ночь, в следующий раз пускай приложат формулу... распоясались
            res_by_worker = (float_revenue - float_costs) / int(staff_count)
            print(f"Прибыль в расчете на одного сотрудника: {res_by_worker}")
        else:
            print("Ошибка, вы ввели нецело число")


    elif float_revenue < float_costs:
        print("Вы несете убытки")
    else:
        print("Вы ничего не зарабатываете")
else:
    print("Вы ввели неверные значения")
