"""
2. Пользователь вводит время в секундах. Переведите время в часы, минуты и секунды и выведите в формате чч:мм:сс. Используйте форматирование строк.
"""

from datetime import datetime

seconds = input("Введите время в секундах:\n")

if seconds.isdigit():
    dt = datetime.fromtimestamp(int(seconds)).strftime("%I:%M:%S")
    print(dt)
else:
    print("Ошибка! Введите число")