"""

4. Пользователь вводит целое положительное число. Найдите самую большую цифру в числе. Для решения используйте цикл while и арифметические операции.

"""


def get_max_number(a, b):
    if a < b:
        return b
    else:
        return a


while True:

    num = input("Введите целое число\n")
    if num.isdigit():
        max_int = 0
        num_int = int(num)

        max_int = get_max_number(num_int % 10, max_int)
        while num_int // 10 > 0 and max_int != 9:
            num_int //= 10
            max_int = get_max_number(num_int % 10, max_int)

        print(max_int)
    else:
        print("Вы ввели нецелое число :(")
