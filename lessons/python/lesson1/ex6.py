"""

6. Спортсмен занимается ежедневными пробежками. В первый день его результат составил a километров.
Каждый день спортсмен увеличивал результат на 10 % относительно предыдущего.
Требуется определить номер дня, на который общий результат спортсмена составить не менее b километров.
Программа должна принимать значения параметров a и b и выводить одно натуральное число — номер дня.

Например: a = 2, b = 3.

Результат:

1-й день: 2
2-й день: 2,2
3-й день: 2,42
4-й день: 2,66
5-й день: 2,93
6-й день: 3,22
Ответ: на 6-й день спортсмен достиг результата — не менее 3 км.


"""

import modules.float_custom as float_custom

day_result_text = input("Введите результат бегуна за первый день (км):")
total_result_text = input("Введите необходимый результат бегуна (км):")

if float_custom.isfloat(day_result_text) and float_custom.isfloat(total_result_text):
    day_result = float(day_result_text)
    total_result = float(total_result_text)
    day_number = 1
    while day_result < total_result:
        print(f"{day_number} день: {round(day_result, 2)}")
        day_result += day_result * 0.10
        day_number += 1

    print(f"{day_number} день: {day_result}")
    print(f"Ответ: на {day_number} день спортсмен достиг результата — не менее {total_result} км.")
else:
    print("Вы должны были ввести числа")
