"""

1. Создать программно файл в текстовом формате, записать в него построчно данные, вводимые пользователем.
Об окончании ввода данных свидетельствует пустая строка.

"""

print("Введите многострочный текст и нажмите еще раз Enter когда закончите ввод")

text = ""
while True:
    line = input("")
    if line == "":
        break

    text += f"{line}\r"

try:
    with open("files/ex1.txt", "w") as f_obj:
        print(text, file=f_obj)
except IOError:
    print(f"Ошибка: {IOError}")


