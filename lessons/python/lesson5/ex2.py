"""

2. Создать текстовый файл (не программно), сохранить в нем несколько строк,
выполнить подсчет количества строк, количества слов в каждой строке.

"""

word_count = []

try:
    with open("files/ex2.txt", "r") as f_obj:
        for line in f_obj:
            word_count.append(len(line.split(" ")))
except IOError:
    print(f"Ошибка: {IOError}")


print(f"Всего строк: {len(word_count)}")

for i, el in enumerate(word_count):
    print(f"Всего слов в строке № {i}: {el}")


