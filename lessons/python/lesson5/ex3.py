"""

3. Создать текстовый файл (не программно), построчно записать фамилии сотрудников и величину их окладов.
Определить, кто из сотрудников имеет оклад менее 20 тыс., вывести фамилии этих сотрудников.

Выполнить подсчет средней величины дохода сотрудников.

"""
from functools import reduce


class person:

    def __init__(self, name, salary):
        self.name = name
        self.salary = salary


class employes:

    def __init__(self):
        self.persons = []

    def avg_salary(self):
        res = 0;
        for item in self.persons:
            res += item.salary;
        return res / len(self.persons)

    def with_big_salary(self):
        return (p for p in self.persons if p.salary >= 20000)


employee_list = employes()
try:
    with open("files/ex3.txt", "r") as f_obj:
        for line in f_obj:
            items = line.split(" ")
            employee_list.persons.append(person(items[0], float(items[1])))
except IOError:
    print(f"Ошибка: {IOError}")

print(f"Средняя ЗП: {employee_list.avg_salary()}")

print(f"Cписок сотрудников с ЗП более 20 000 р")
for p in employee_list.with_big_salary():
    print(p.name)


