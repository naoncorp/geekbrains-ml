"""

5. Создать (программно) текстовый файл, записать в него программно набор чисел, разделенных пробелами.
Программа должна подсчитывать сумму чисел в файле и выводить ее на экран.

"""
from functools import reduce

numbers = [1, 2, 3, 4, 5]

try:
    with open("files/ex5.txt", "w") as f_obj:
        f_obj.write(" ".join((str(el) for el in numbers)))

    with open("files/ex5.txt", "r") as f_obj:
        line = f_obj.readline()
        items = line.split(" ")
        print(reduce(lambda prev_el, el: int(prev_el) + int(el), items))

except IOError:
    print(f"Ошибка: {IOError}")