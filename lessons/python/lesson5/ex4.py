"""

4. Создать (не программно) текстовый файл со следующим содержимым:

One — 1

Two — 2

Three — 3

Four — 4

Необходимо написать программу, открывающую файл на чтение и считывающую построчно данные.
При этом английские числительные должны заменяться на русские.
Новый блок строк должен записываться в новый текстовый файл.

"""

translates = {"One": "Один", "Two": "Два", "Three": "Три", "Four": "Четыре"}


def replace_func(text):
    res = text
    for tr in translates:
        res = res.replace(tr, translates[tr])
    return res


lines = []
try:
    with open("files/ex4.txt", "r") as f_obj:
        for line in f_obj:
            lines.append(replace_func(line))

    with open("files/ex4_2.txt", "w") as f_obj:
        f_obj.writelines(lines)

except IOError:
    print(f"Ошибка: {IOError}")