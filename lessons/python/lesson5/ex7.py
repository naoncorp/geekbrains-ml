"""

7. Создать (не программно) текстовый файл, в котором каждая строка должна содержать данные о фирме: название,
форма собственности, выручка, издержки.

Пример строки файла: firm_1 ООО 10000 5000.

Необходимо построчно прочитать файл, вычислить прибыль каждой компании, а также среднюю прибыль. Если фирма получила убытки,
в расчет средней прибыли ее не включать.

Далее реализовать список. Он должен содержать словарь с фирмами и их прибылями, а также словарь со средней прибылью.
Если фирма получила убытки, также добавить ее в словарь (со значением убытков).

Пример списка: [{“firm_1”: 5000, “firm_2”: 3000, “firm_3”: 1000}, {“average_profit”: 2000}].

Итоговый список сохранить в виде json-объекта в соответствующий файл.

Пример json-объекта:

[{"firm_1": 5000, "firm_2": 3000, "firm_3": 1000}, {"average_profit": 2000}]
Подсказка: использовать менеджеры контекста.

"""
import json
from functools import reduce

lines = []
res = []
try:
    with open("files/ex7.txt", "r") as f_obj:
        lines = f_obj.readlines()
except IOError:
    print(f"Ошибка: {IOError}")


companies_dict = {}

for line in lines:
    company_items = line.split(" ")
    profit = float(company_items[2]) - float(company_items[3])
    if profit <= 0:
        continue
    companies_dict[company_items[0]] = profit

res.append(companies_dict)
res.append({"average_profit": (reduce(lambda prev_el, el: prev_el + el, list(companies_dict.values())) / len(companies_dict))})

print(res)

try:
    with open("files/ex7_json.txt", "w") as f_obj:
        json.dump(res, f_obj)
except IOError:
    print(f"Ошибка: {IOError}")