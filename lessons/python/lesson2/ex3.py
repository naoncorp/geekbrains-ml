"""

3. Пользователь вводит месяц в виде целого числа от 1 до 12.
Сообщить к какому времени года относится месяц (зима, весна, лето, осень).
Напишите решения через list и через dict.

"""

winter, spring, summer, autumn = "Зима", "Весна", "Лето", "Осень"

my_list = [winter, winter, spring, spring, spring, summer, summer, summer, autumn, autumn, autumn, winter]

my_dict = {1: winter, 2: winter, 3: spring, 4: spring, 5: spring, 6: summer, 7: summer, 8: summer, 9: autumn, 10: autumn, 11: autumn, 12: winter}

while True:

    number = input("Введите номер месяца\n")
    numberInt = int(number)
    if numberInt <= 0 or numberInt > 12:
        print("Вы ввели некорректное число")
        break

    print(f"Из списка: {my_list[numberInt-1]}")
    print(f"Из справочника: {my_dict[numberInt]}")


