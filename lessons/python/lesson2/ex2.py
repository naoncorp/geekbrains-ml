"""

2. Для списка реализовать обмен значений соседних элементов,
т.е. Значениями обмениваются элементы с индексами 0 и 1, 2 и 3 и т.д.
При нечетном количестве элементов последний сохранить на своем месте.
Для заполнения списка элементов необходимо использовать функцию input().

"""

while True:

    list_input = input("Введите список значений через запятую:\n")

    my_list = list_input\
        .replace('.', '.')\
        .replace(' ', '')\
        .split(',')

    list_length = len(my_list)
    i = 0
    for item in my_list:
        if i >= list_length-1:
            break

        my_list[i], my_list[i+1] = my_list[i+1], my_list[i]
        i += 2

    print(my_list)