"""

6. *Реализовать структуру данных «Товары». Она должна представлять собой список кортежей.
Каждый кортеж хранит информацию об отдельном товаре.
В кортеже должно быть два элемента — номер товара и словарь с параметрами (характеристиками товара: название, цена, количество, единица измерения).
Структуру нужно сформировать программно, т.е. запрашивать все данные у пользователя.

Пример готовой структуры:

    [

    (1, {“название”: “компьютер”, “цена”: 20000, “количество”: 5, “eд”: “шт.”}),
    (2, {“название”: “принтер”, “цена”: 6000, “количество”: 2, “eд”: “шт.”}),
    (3, {“название”: “сканер”, “цена”: 2000, “количество”: 7, “eд”: “шт.”})
    ]

Необходимо собрать аналитику о товарах. Реализовать словарь, в котором каждый ключ — характеристика товара, например название, а значение — список значений-характеристик, например список названий товаров.

Пример:

{

    “название”: [“компьютер”, “принтер”, “сканер”],
    “цена”: [20000, 6000, 2000],
    “количество”: [5, 2, 7],
    “ед”: [“шт.”]
}

"""


# Заполнение характеристик
def input_chars():
    product_chars = {}
    while True:
        char_name = input("Введите название характеристики или просто нажмите Enter если Вы уже занесли все "
                          "характеристики:\n")

        if char_name == "":
            if len(product_chars) > 0:
                break
            else:
                continue
        elif product_chars.get(char_name) is not None:
            print("Такой элемент уже есть в словаре")
            continue

        char_value = "";
        while len(char_value) < 1:
            char_value = input("Введите значение характеристики:\n")

        product_chars[char_name] = char_value
        continue

    return product_chars


def input_logic():
    product_list = []
    print("\nДавайте сначала добавим товары и их характеристики.\n")
    while True:
        my_chars = input_chars()
        product_list.append((len(product_list) + 1, my_chars))
        if input("Хотите добавить еще один товар? да / нет\n").lower() == "да":
            continue
        else:
            break

    return product_list


while True:
    products = input_logic()

    print("Спасибо. Вот продукты, что вы завели:\n")
    print(products)

    # статистика
    stat = {}
    for product in products:
        for char_key, value in product[1].items():
            current_item = stat.get(char_key)
            if current_item is None:
                stat[char_key] = [value]
            else:
                stat[char_key].append(value)

    print("\nА вот ваша статистика по продуктам:\n")
    print(stat)
