"""

7. Реализовать проект «Операции с комплексными числами».
Создайте класс «Комплексное число», реализуйте перегрузку методов сложения и умножения комплексных чисел.
Проверьте работу проекта, создав экземпляры класса (комплексные числа) и выполнив сложение и умножение созданных экземпляров.
Проверьте корректность полученного результата.

"""


class ComplexNumber:

    def __init__(self, value: complex):
        self.value = value

    def __add__(self, other):
        return ComplexNumber(self.value + other.value)

    def __sub__(self, other):
        return ComplexNumber(self.value - other.value)

    def __mul__(self, other):
        return ComplexNumber(self.value * other.value)


a = ComplexNumber(complex(4, 5))
b = ComplexNumber(complex(3, 7))

print((a + b).value)
print((a - b).value)