"""
1. Реализовать класс «Дата», функция-конструктор которого должна принимать дату в виде строки формата «день-месяц-год».
В рамках класса реализовать два метода. Первый, с декоратором @classmethod, должен извлекать число, месяц,
год и преобразовывать их тип к типу «Число».
Второй, с декоратором @staticmethod, должен проводить валидацию числа, месяца и года (например, месяц — от 1 до 12).
Проверить работу полученной структуры на реальных данных.

"""

from datetime import datetime

class Date:

    def __new__(cls, *args, **kwargs):
        cls.date = "2020-02-10"

    @classmethod
    def parse(cls):
        print(cls.date)
        arr = cls.date.split('-')

        cls.year = int(arr[0])
        cls.month = int(arr[1])
        cls.day = int(arr[2])

        print(f"Год: {cls.year}")
        print(f"Месяц: {cls.month}")
        print(f"День: {cls.day}")

    @staticmethod
    def validate(date):
        try:
            datetime.strptime(date, "%Y-%M-%d")
        except ValueError:
            print("Некорректный формат даты")


Date("2020-02-10")
Date.parse()
Date.validate(Date.date)