"""

4. Начните работу над проектом «Склад оргтехники». Создайте класс, описывающий склад.
А также класс «Оргтехника», который будет базовым для классов-наследников.
Эти классы — конкретные типы оргтехники (принтер, сканер, ксерокс).
В базовом классе определить параметры, общие для приведенных типов.
В классах-наследниках реализовать параметры, уникальные для каждого типа оргтехники.

5. Продолжить работу над первым заданием.
Разработать методы, отвечающие за приём оргтехники на склад и передачу в определенное подразделение компании.
Для хранения данных о наименовании и количестве единиц оргтехники, а также других данных,
можно использовать любую подходящую структуру, например словарь.

6. Продолжить работу над вторым заданием.
Реализуйте механизм валидации вводимых пользователем данных. Например, для указания количества принтеров,
отправленных на склад, нельзя использовать строковый тип данных.

Подсказка: постарайтесь по возможности реализовать в проекте «Склад оргтехники» максимум возможностей, изученных на уроках по ООП.

"""

import modules.float_custom as float_custom
from itertools import groupby


class OfficeEquipment:
    def __init__(self, vin, name):
        self.vin = vin
        self.name = name

    def __str__(self):
        return f"{self.name} {self.vin}"


class Printer(OfficeEquipment):
    def __init__(self, vin):
        super().__init__(vin, "Printer")

    def pprint(self):
        print("Print")


class Scanner(OfficeEquipment):
    def __init__(self, vin):
        super().__init__(vin, "Scanner")

    def scan(self):
        print("Scan")


class MFP(OfficeEquipment):
    def __init__(self, vin):
        super().__init__(vin, "MFP")

    def copy(self):
        print("Scan")


class Warehouse:
    __storage = []

    def __new__(cls, name):
        cls.name = name

    @classmethod
    def get_list(cls):
        return cls.__storage

    @classmethod
    def add_to_storage(cls, item: OfficeEquipment):
        cls.__storage.append(item)

    @classmethod
    def send_to_customer(cls, item: OfficeEquipment, customer: str):
        cls.__storage.remove(item)
        print(f"Отправлено {customer}")

    @classmethod
    def get_statistics(cls):
        for key, group in groupby(cls.__storage, lambda x: x.name):
            print(f"{key}: {len(list(group))}")


class TypeException(Exception):
    def __init__(self, text):
        self.text = text


while True:
    try:

        vin = input("Введите VIN устройства")
        if not float_custom.isfloat(vin):
            raise TypeException("Ошибка. Введите число")

        while True:
            type = input("Введите тип устройства")

            if type == "Printer":
                Warehouse.add_to_storage(Printer(vin))
                break
            elif type == "Scanner":
                Warehouse.add_to_storage(Scanner(vin))
                break
            elif type == "MFP":
                Warehouse.add_to_storage(MFP(vin))
                break
            else:
                print("Введите корректный тип устройства")
                continue
    except TypeException as msg:
        print(msg)

    Warehouse.get_statistics()







