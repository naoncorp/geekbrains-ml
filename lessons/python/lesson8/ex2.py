"""
2. Создайте собственный класс-исключение, обрабатывающий ситуацию деления на нуль.
Проверьте его работу на данных, вводимых пользователем.
При вводе пользователем нуля в качестве делителя программа должна корректно обработать эту ситуацию и не завершиться с ошибкой.

"""


class DevideException(Exception):
    def __init__(self, text):
        self.text = text


def devide_numbers(a,b):
    if b == 0:
        raise DevideException("Ошибка деления на нуль")
    return  a / b

try:
    devide_numbers(5,0)
except DevideException as msg:
    print(f"{msg}")
