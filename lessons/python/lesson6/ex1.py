"""

Создать класс TrafficLight (светофор) и определить у него один атрибут color (цвет) и метод running (запуск).
Атрибут реализовать как приватный. В рамках метода реализовать переключение светофора в режимы: красный, желтый, зеленый.
Продолжительность первого состояния (красный) составляет 7 секунд, второго (желтый) — 2 секунды, третьего (зеленый) — на ваше усмотрение.
Переключение между режимами должно осуществляться только в указанном порядке (красный, желтый, зеленый). Проверить работу примера,
создав экземпляр и вызвав описанный метод.

Задачу можно усложнить, реализовав проверку порядка режимов, и при его нарушении выводить соответствующее сообщение и завершать скрипт.


"""
import time
from enum import Enum


class Color(Enum):
    red = 1
    yellow = 2
    green = 3


class TrafficLight:
    __color = Color.red

    def __change_color(self):
        if self.__color == Color.green:
            self.__color = Color.red
            print(str(self.__color))
            time.sleep(7)
        elif self.__color == Color.yellow:
            self.__color = Color.green
            print(str(self.__color))
            time.sleep(5)
        else:
            self.__color = Color.yellow
            print(str(self.__color))
            time.sleep(2)

    def running(self):
        while True:
            self.__change_color()


light = TrafficLight()
light.running()
