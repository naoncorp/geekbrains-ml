"""

3. Реализовать базовый класс Worker (работник), в котором определить атрибуты: name, surname, position (должность), income (доход).
Последний атрибут должен быть защищенным и ссылаться на словарь, содержащий элементы: оклад и премия, например, {"wage": wage, "bonus": bonus}.
Создать класс Position (должность) на базе класса Worker.
В классе Position реализовать методы получения полного имени сотрудника (get_full_name) и дохода с учетом премии (get_total_income).

Проверить работу примера на реальных данных (создать экземпляры класса Position, передать данные, проверить значения атрибутов, вызвать методы экземпляров).

"""


class Worker:
    name = ""
    surname = ""
    position = ""
    _income = {}

    def __init__(self, wage, bonus):
        self._income = {"wage": wage, "bonus": bonus}

    def get_full_name(self):
        return f"{self.name} {self.surname}"


class Position(Worker):

    __name = ""

    def __init__(self, wage, bonus, name):
        super().__init__(wage, bonus)
        self.__name = name

    def get_total_income(self):
        return self._income["wage"] + self._income["bonus"]


dev = Position(500, 100, "Developer")
print(dev.get_full_name())
print(dev.get_total_income())