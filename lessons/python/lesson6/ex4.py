"""

4. Реализуйте базовый класс Car. У данного класса должны быть следующие атрибуты: speed, color, name, is_police (булево).
А также методы: go, stop, turn(direction), которые должны сообщать, что машина поехала, остановилась, повернула (куда).

Опишите несколько дочерних классов: TownCar, SportCar, WorkCar, PoliceCar. Добавьте в базовый класс метод show_speed,
который должен показывать текущую скорость автомобиля.

Для классов TownCar и WorkCar переопределите метод show_speed. При значении скорости свыше 60 (TownCar) и 40 (WorkCar)
должно выводиться сообщение о превышении скорости.

Создайте экземпляры классов, передайте значения атрибутов. Выполните доступ к атрибутам, выведите результат.
Выполните вызов методов и также покажите результат.

"""

class Car:
    speed = 0
    color = ""
    name = ""
    is_police = False

    def go(self):
        print("Машина поехала")

    def stop(self):
        print("Машина остановилась")

    def turn(self, direction):
        print(f"Машина повернула {direction}")

    def show_speed(self):
        print(f"Текущая скорость: {self.speed}")


class TownCar(Car):

    def __init__(self):
        name = "TownCar"

    def show_speed(self):
        print(f"Текущая скорость: {self.speed}")
        if self.speed > 60:
            print("Осторжно, вы превысили скорость")


class WorkCar(Car):

    def __init__(self):
        self.name = "WorkCar"

    def show_speed(self):
        print(f"Текущая скорость: {self.speed}")
        if self.speed > 40:
            print("Осторжно, вы превысили скорость")


class SportCar(Car):

    def __init__(self):
        self.name = "SportCar"


class PoliceCar(Car):

    def __init__(self):
        self.name = "PoliceCar"
        self.is_police = True


# init

tc = TownCar()
tc.speed = 70
tc.show_speed()

tc = SportCar()
tc.speed = 7000
tc.show_speed()

tc = WorkCar()
tc.speed = 10
tc.show_speed()

tc = PoliceCar()
tc.speed = 70
tc.show_speed()
