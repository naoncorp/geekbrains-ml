from bs4 import BeautifulSoup as bs
import requests
from datetime import datetime
import json
from pymongo import MongoClient
from lxml import html
from urllib.parse import unquote
from selenium import webdriver
import ssl

class YoulaParser:

    def __init__(self):
        self.base_url = 'https://auto.youla.ru'

    def save_to_mongo_db(self, data):
        try:
            login = 'naoncorp'
            password = '' # не скажу :)
            client = MongoClient(f"mongodb+srv://{login}:{password}@cluster0.hwukb.mongodb.net/test?retryWrites=true&w=majority", ssl=True, ssl_cert_reqs=ssl.CERT_NONE)

            db = client.test
            collection = db.youla
            collection.insert_one(data)
        except:
            raise Exception("Ошибка подключения к MongoDB")
        finally:
            client.close()

    def parse_page(self, url):
        product = {}

        # знаю, что для данного ТЗ webdriver - это чит, но по-другому нельзя гарантировать, что данные будут корректны.
        # разработчик всегда может добавить какую-нить еще постобработку в JS, которая вообще модифицирует данные
        # В тоже время webdriver намного медленнее :(
        browser = webdriver.PhantomJS(
            executable_path='/Users/dmitrynikitin/PycharmProjects/geekbrains-ml/phantomjs/bin/phantomjs')
        browser.get(url)
        page_html = browser.page_source
        page = html.fromstring(page_html)

        # get name
        name = page.xpath("//div[@data-target='advert-title']/text()")
        if len(name) > 0:
            product['name'] = name[0]

        # get images
        script_tag = page.xpath("//head/script[contains(text(),'image')]/text()")
        if len(script_tag) > 0:
            add_info = json.loads(script_tag[0])
            product['images'] = add_info['image']

        # get desc
        desc = page.xpath("//div[@data-target='advert-info-descriptionFull']/text()")
        if len(desc) > 0:
            product['describe'] = desc[0]

        # get seller url
        seller_url = page.xpath("//a[@data-target='advert-seller-name']/@href")
        if len(seller_url) > 0:
            product['seller_url'] = 'https:' + seller_url[0]

        # get characters
        characters = page.xpath("//div[@data-target='advert']//div[contains(@class,'AdvertSpecs_row')]")

        if len(characters) > 0:
            product['characters'] = []

            for ch in characters:
                label = ch.xpath(".//div[contains(@class,'AdvertSpecs_label')]/text()")[0]
                value = ch.xpath(".//div[contains(@class,'AdvertSpecs_data')]//text()")[0]
                product['characters'].append({label: value})

        # get phone
        browser.find_element_by_xpath("//button[@data-name='showPhone']").click()
        phone = browser.find_element_by_xpath("//span[@data-target='popup-phone']")
        if phone is not None:
            product['phone'] = phone.text

        return product

    def parse_products(self):

        i = 1
        while True:
            response = requests.get(f'{self.base_url}/moskva/cars/used/?page={i}')
            page = html.fromstring(response.text)
            product_urls = page.xpath("//a[@data-target='serp-snippet-title']/@href")

            if len(product_urls) == 0:
                break

            for url in product_urls:
                # get page
                product = self.parse_page(url)
                self.save_to_mongo_db(product)

            i = i+1


yp = YoulaParser()
yp.parse_products()