import requests
import json

service = 'https://5ka.ru/api/v2'

# get categories
req = requests.get(f'{service}/categories/')
categories = json.loads(req.text)

for cat in categories:
    cat_products = {}
    cat_products['code'] = cat['parent_group_code']
    cat_products['name'] = cat['parent_group_name']
    cat_products['products'] = []

    records_per_page = 20
    page = 1
    while True:
        req = requests.get(
            f'{service}/special_offers/?records_per_page={records_per_page}&page={page}&categories={cat_products["code"]}')
        products = json.loads(req.text)

        if len(products['results']) == 0:
            break

        cat_products['products'].extend(products['results'])

        page = page + 1

    if len(cat_products['products']) > 0:
        with open(f'{cat_products["name"]}.json', 'w', encoding="UTF-8") as outfile:
            json.dump(cat_products, outfile, ensure_ascii=False)


