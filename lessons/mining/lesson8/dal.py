from pymongo import MongoClient
import ssl


class DbContext:

    def __init__(self):
        login = 'naoncorp'
        password = ''  # todo пароль
        self.client = MongoClient(
            f"mongodb+srv://{login}:{password}@cluster0.hwukb.mongodb.net/test?retryWrites=true&w=majority",
            ssl=True,
            ssl_cert_reqs=ssl.CERT_NONE)
        db = self.client.test
        self.collection = db.insta_friends

    def save_user(self, user):
        try:
            self.collection.insert_one(user)
        except:
            raise Exception("Ошибка сохранения юзера")

    def get_users_for_process(self):
        try:
            return self.collection.find({"is_processed_friends": False})
        except:
            raise Exception("Ошибка получения юзеров")

    def update_is_process(self, user):
        try:
            self.collection.update_one({"username": user}, {"$set": {'is_processed_friends': True}})
        except:
            raise Exception("Ошибка обновления документа")

    def get_users_chain(self, left_user, right_user):
        parent_user = 0
        chain = [right_user]
        current_user = right_user
        while parent_user is not None:
            user = self.collection.find_one({"friends": current_user})
            parent_user = user["parent_user"]
            chain.append(user["username"])
            current_user = user["username"]

        chain.reverse()
        return chain

    def __exit__(self, exc_type, exc_value, traceback):
        self.client.close()
