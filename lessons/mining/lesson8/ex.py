from selenium import webdriver

import time
import json

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

from lessons.mining.lesson8.dal import DbContext
from lessons.mining.lesson8.utils import cross_lists


class InstaParser:

    def login(self, browser):

        # login
        browser.get(f'https://www.instagram.com/accounts/login/')

        username_field = WebDriverWait(browser, 10) \
            .until(EC.presence_of_element_located((By.CSS_SELECTOR, "input[name=username]")))

        username_field.send_keys("naoncorp") # todo логин

        password_field = browser.find_element_by_css_selector('input[name=password]')
        password_field.send_keys("") # todo пароль

        browser.find_element_by_css_selector('button[type=submit]').click()
        time.sleep(3)

    def parse_user_targets(self, user, driver, type):
        driver.get(f'https://www.instagram.com/{user}/?hl=ru')

        # open modal
        followers_link = WebDriverWait(driver, 10) \
            .until(EC.presence_of_element_located((By.CSS_SELECTOR, f"a[href*={type}]")))

        followers_link.click()

        user_targets = []
        count_prev = -1
        while count_prev != len(user_targets):
            count_prev = len(user_targets)

            WebDriverWait(driver, 10) \
                .until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[role=dialog]")))

            WebDriverWait(driver, 10) \
                .until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[role=dialog] span a")))
            followers = driver.find_elements_by_css_selector('div[role=dialog] span a')

            for fw in followers[len(user_targets):]:
                #user_url = fw.get_attribute('href')
                username = fw.text

                user_targets.append(username)

            WebDriverWait(driver, 10) \
                .until(EC.presence_of_element_located((By.CSS_SELECTOR, f'a[title="{user_targets[-1]}"]')))
            target = driver.find_element_by_css_selector(f'a[title="{user_targets[-1]}"]')
            target.send_keys(Keys.PAGE_DOWN)
            time.sleep(1)

        # close dialog
        driver.find_element_by_css_selector('div[role="dialog"] button').click()

        return user_targets

    def save_user_friends(self, user, parent_user, driver):

        followers = self.parse_user_targets(user, driver, 'followers')
        following = self.parse_user_targets(user, driver, 'following')

        # cross arrays
        friends = cross_lists(followers, following)

        # save to mongo db
        DbContext().save_user({"username": user, "is_processed_friends": False, "parent_user": parent_user, "friends": friends})

        return friends


    def parse(self, left_user, right_user):
        # os x
        driver = webdriver.Chrome(
            executable_path='/Users/dmitrynikitin/PycharmProjects/geekbrains-ml/chromedriver/chromedriver')

        # windows
        # browser = webdriver.Chrome(
        #    executable_path='C:/Users/d.nikitin/source/repos/Geekbrains/gb-ml/geekbrains-ml/chromedriver/chromedriver.exe')

        try:
            self.login(driver)

            # safe to mongo root user
            self.save_user_friends(left_user, None, driver)

            db_context = DbContext()
            # проходим 5 рукопожатий (глубина построения иерархии)
            for hierarchy_level in range(5):
                users = db_context.get_users_for_process()
                for us in users:
                    for fr in us["friends"]:
                        target_friends = self.save_user_friends(fr, us["username"], driver)
                        if right_user in target_friends:
                            chain = db_context.get_users_chain(left_user, right_user)
                            print(' => '.join(chain))
                            return

                    db_context.update_is_process(us["username"])
        finally:
            driver.quit()


InstaParser().parse('naoncorp', 'oksy.chu')
