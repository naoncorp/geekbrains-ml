def cross_lists(a, b):
    res = []
    for i in a:
        if i in res:
            continue
        for j in b:
            if i == j:
                res.append(i)
                break
    return res
