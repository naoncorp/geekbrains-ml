from selenium import webdriver
import ssl
from pymongo import MongoClient
import time
import json

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys


class InstaParser:
    def __init__(self):
        self.users = ['stella_oriente']

    def save_to_mongo_db(self, data):
        try:
            login = 'naoncorp'
            password = ''  # todo пароль
            client = MongoClient(
                f"mongodb+srv://{login}:{password}@cluster0.hwukb.mongodb.net/test?retryWrites=true&w=majority",
                ssl=True,
                ssl_cert_reqs=ssl.CERT_NONE)

            db = client.test
            collection = db.insta_followers
            collection.insert_many(data)
        except:
            raise Exception("Ошибка подключения к MongoDB")
        finally:
            client.close()

    def login(self, browser):

        # login
        browser.get(f'https://www.instagram.com/accounts/login/')

        username_field = WebDriverWait(browser, 10) \
            .until(EC.presence_of_element_located((By.CSS_SELECTOR, "input[name=username]")))

        username_field.send_keys("") # todo логин

        password_field = browser.find_element_by_css_selector('input[name=password]')
        password_field.send_keys("") # todo пароль

        browser.find_element_by_css_selector('button[type=submit]').click()
        time.sleep(3)

    def parse_user_targets(self, user, driver):
        driver.get(f'https://www.instagram.com/{user}/?hl=ru')

        all_user_targets = []

        groups = ['following', 'followers']
        for gr in groups:
            # open modal
            followers_link = WebDriverWait(driver, 10) \
                .until(EC.presence_of_element_located((By.CSS_SELECTOR, f"a[href*={gr}]")))

            followers_link.click()

            user_targets = []
            count_prev = -1
            while count_prev != len(user_targets):
                count_prev = len(user_targets)

                WebDriverWait(driver, 10) \
                    .until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[role=dialog]")))

                WebDriverWait(driver, 10) \
                    .until(EC.presence_of_element_located((By.CSS_SELECTOR, "div[role=dialog] span a")))
                followers = driver.find_elements_by_css_selector('div[role=dialog] span a')

                for fw in followers[len(user_targets):]:
                    user_url = fw.get_attribute('href')
                    username = fw.text

                    user_targets.append(
                        {'username': user, 'target_username': username, 'target_url': user_url, 'target_type': gr})

                WebDriverWait(driver, 10) \
                    .until(EC.presence_of_element_located((By.CSS_SELECTOR, f'a[title="{user_targets[-1]["target_username"]}"]')))
                target = driver.find_element_by_css_selector(f'a[title="{user_targets[-1]["target_username"]}"]')
                target.send_keys(Keys.PAGE_DOWN)
                time.sleep(1)

            # close dialog
            driver.find_element_by_css_selector('div[role="dialog"] button').click()
            all_user_targets.extend(user_targets)

        return all_user_targets

    def parse(self):
        # os x
        browser = webdriver.Chrome(
            executable_path='/Users/dmitrynikitin/PycharmProjects/geekbrains-ml/chromedriver/chromedriver')

        # windows
        # browser = webdriver.Chrome(
        #    executable_path='C:/Users/d.nikitin/source/repos/Geekbrains/gb-ml/geekbrains-ml/chromedriver/chromedriver.exe')

        try:
            self.login(browser)

            all_user_targets = []
            for user in self.users:
                all_user_targets = self.parse_user_targets(user, browser)

            self.save_to_mongo_db(all_user_targets)

        finally:
            browser.quit()


InstaParser().parse()
