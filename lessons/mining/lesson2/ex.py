from bs4 import BeautifulSoup as bs
import requests
import json
from datetime import datetime
from pymongo import MongoClient


class MagnitParser:

    def __init__(self):

        self.base_addr = 'https://magnit.ru'

        response = requests.get(f'{self.base_addr}/promo/?geo=moskva').text
        soup = bs(response, 'html.parser')

        self.catalogItems = soup.select("div[class~=сatalogue__main] a:not(.card-sale_banner)")

        self.products = []

        self.product_template = {
            'url': 'None',
            'promo_name': 'None',
            'product_name': 'None',
            'old_price': 'None',
            'new_price': 'None',
            'image_url': 'None',
            'date_from': 'None',
            'date_to': 'None',
        }

    def get_single(self, elem, selector):
        res = elem.select(selector)
        if len(res) == 1:
            return res[0]
        elif len(res) > 1:
            raise Exception('В коллекции более одного элемента')
        else:
            return None

    def get_cost(self, elem, selector):
        cost_item = self.get_single(elem, selector)
        if cost_item is not None:
            price_integer = self.get_single(cost_item, 'span[class~=label__price-integer]')
            price_decimal = self.get_single(cost_item, 'span[class~=label__price-decimal]')

            if price_integer is None or price_decimal is None:
                return None
            else:
                return float(f'{price_integer.text}.{price_decimal.text}')

    def date_convert(self, str):
        return str.replace('\n', '') \
            .replace('Только ','')\
            .replace('с ', '') \
            .replace('до ', '') \
            .replace(' января', '.01.2020') \
            .replace(' февраля', '.02.2020') \
            .replace(' марта', '.03.2020') \
            .replace(' апреля', '.04.2020') \
            .replace(' мая', '.05.2020') \
            .replace(' июня', '.06.2020') \
            .replace(' июля', '.07.2020') \
            .replace(' августа', '.08.2020') \
            .replace(' сентября', '.09.2020') \
            .replace(' октября', '.10.2020') \
            .replace(' ноября', '.11.2020') \
            .replace(' декабря', '.12.2020')

    def get_date(self, elem, selector, is_from):
        dates = elem.select(selector)

        if len(dates) == 0:
            return None
        else:
            res = ''
            if is_from:
                res = self.date_convert(f'{dates[0].text} 00:00:00.000')
            elif len(dates) > 1:
                res = self.date_convert(f'{dates[1].text} 23:59:59.999999')
            else:
                res = self.date_convert(f'{dates[0].text} 23:59:59.999999')

            return datetime.strptime(res, '%d.%m.%Y %H:%M:%S.%f')

    def parse(self):

        for ci in self.catalogItems:
            product = self.product_template.copy()

            product['url'] = f'{self.base_addr}{ci.attrs["href"]}'

            promo_name = self.get_single(ci, 'div[class~=card-sale__header] p')
            if promo_name is not None:
                product['promo_name'] = promo_name.text

            product_name = self.get_single(ci, 'div[class~=card-sale__title] p')
            if product_name is not None:
                product['product_name'] = product_name.text

            old_price = self.get_cost(ci, 'div[class~=label__price_old]')
            if old_price is not None:
                product['old_price'] = old_price

            new_price = self.get_cost(ci, 'div[class~=label__price_new]')
            if new_price is not None:
                product['new_price'] = new_price

            image_url = self.get_single(ci, 'div[class~=card-sale__col_img] img')
            if image_url is not None:
                product['image_url'] = f'{self.base_addr}{image_url.attrs["data-src"]}'

            date_from = self.get_date(ci, 'div[class=card-sale__date] p', True)
            if date_from is not None:
                product['date_from'] = date_from

            date_to = self.get_date(ci, 'div[class=card-sale__date] p', False)
            if date_to is not None:
                product['date_to'] = date_to

            self.products.append(product)

    def print_products(self):
        for item in self.products:
            print(item)

    def save_to_mongo_db(self):
        login = 'naoncorp'
        password = '' # не скажу :)
        client = MongoClient(f"mongodb+srv://{login}:{password}@cluster0.hwukb.mongodb.net/test?retryWrites=true&w=majority")

        db = client.test
        collection = db.magnit
        collection.insert_many(self.products)

        # group by query
        mongo_items = collection.aggregate([{'$group': {'_id': "$promo_name", 'count': {'$sum': 1}}}])
        for item in mongo_items:
            print(item)


mp = MagnitParser()
mp.parse()
#mp.print_products()
mp.save_to_mongo_db()
