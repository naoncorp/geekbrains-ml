import scrapy
from itemloaders.processors import Compose, MapCompose, Join, TakeFirst
from scrapy.loader import ItemLoader

def take_first_nan(value):
    if value is None:
        return ''
    return value

class VacancyPageItem(scrapy.Item):
    # define the fields for your item here like:
    _id = scrapy.Field()
    name = scrapy.Field()
    salary = scrapy.Field()
    description = scrapy.Field()
    key_features = scrapy.Field()
    owner_url = scrapy.Field()

    pass

class VacancyLoader(ItemLoader):
    default_item_class = VacancyPageItem
    default_output_processor = TakeFirst()
    name_out = TakeFirst()
    salary_out = TakeFirst()
    description_out = TakeFirst()
    key_features_out = MapCompose()
    owner_url_out = TakeFirst()


