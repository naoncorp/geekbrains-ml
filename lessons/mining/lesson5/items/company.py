import scrapy
from itemloaders.processors import Compose, MapCompose, Join, TakeFirst
from scrapy.loader import ItemLoader

class CompanyPageItem(scrapy.Item):
    _id = scrapy.Field()
    name = scrapy.Field()
    url = scrapy.Field()
    description = scrapy.Field()
    industries = scrapy.Field()

    pass

class CompanyLoader(ItemLoader):
    default_item_class = CompanyPageItem
    default_output_processor = TakeFirst()
    name_out = TakeFirst()
    url_out = TakeFirst()
    description_out = TakeFirst()
    industries_out = MapCompose()