import math

import scrapy
from scrapy.http import HtmlResponse
from lessons.mining.lesson5.items.vacancy import VacancyLoader
from lessons.mining.lesson5.items.company import CompanyLoader
import json


class HhruSpider(scrapy.Spider):
    name = 'hhru'
    allowed_domains = ['hh.ru']
    start_urls = ['https://hh.ru/search/vacancy?schedule=remote&L_profession_id=0&area=113']

    def get_json(self, item, selectors: []):
        it = item

        for p in selectors:
            try:
                it = it[p]
            except:
                return None

        return it

    def compose_salary(self, salary_from, salary_to, currency_code):
        if salary_from is not None and salary_to is not None:
            return f"{salary_from}-{salary_to} {currency_code}"
        elif salary_from is not None and salary_to is None:
            return f"От {salary_from} {currency_code}"
        elif salary_from is None and salary_to is not None:
            return f"До {salary_to} {currency_code}"
        else:
            return ''

    def parse(self, response):
        # vacancy list
        next_page = response.css('a.HH-Pager-Controls-Next::attr(href)').extract_first()
        yield response.follow(next_page, callback=self.parse)

        # vacancy page
        vacansy = response.css('.vacancy-serp-item a.HH-LinkModifier::attr(href)').extract()
        for link in vacansy:
            yield response.follow(link, callback=self.vacancy_and_owner_parse)

    def vacancy_parse(self, response: HtmlResponse):
        data = json.loads(response.css('template#HH-Lux-InitialState::text').extract_first())

        l = VacancyLoader(selector=response)

        l.add_css('name', 'div.vacancy-title h1::text')

        salary_to = self.get_json(data, ['vacancyView', 'compensation', 'to'])
        salary_from = self.get_json(data, ['vacancyView', 'compensation', 'from'])
        currency_code = self.get_json(data, ['vacancyView', 'compensation', 'currencyCode'])
        l.add_value('salary', self.compose_salary(salary_to, salary_from, currency_code))

        l.add_value('description', self.get_json(data, ['vacancyView', 'description']))
        l.add_value('key_features', self.get_json(data, ['vacancyView', 'keySkills', 'keySkill']))

        owner_url = f"https://hh.ru/employer/{self.get_json(data, ['vacancyView', 'company', 'id'])}"
        l.add_value('owner_url', owner_url)

        yield l.load_item()

    def vacancy_and_owner_parse(self, response: HtmlResponse):
        self.vacancy_parse(response)

        # owner page
        data = json.loads(response.css('template#HH-Lux-InitialState::text').extract_first())
        owner_url = f"https://hh.ru/employer/{self.get_json(data, ['vacancyView', 'company', 'id'])}"
        yield response.follow(owner_url, callback=self.owner_parse)

    def owner_vacancy_parse(self, response):
        data = json.loads(response.body)
        for vacancy in self.get_json(data, ['vacancies']):
            yield response.follow(self.get_json(vacancy, ['links', 'desktop']), callback=self.vacancy_parse)

    def owner_parse(self, response: HtmlResponse):
        data = json.loads(response.css('template#HH-Lux-InitialState::text').extract_first())

        l = CompanyLoader(selector=response)

        l.add_value('name', self.get_json(data, ['employerInfo', 'name']))
        l.add_value('url', self.get_json(data, ['employerInfo', 'site', 'hostname']))

        industries = self.get_json(data, ['employerInfo', 'industries', 0, 'trl'])
        if industries is not None:
            industries = industries.split(', ')

        l.add_value('industries', industries)

        l.add_value('description', self.get_json(data, ['employerInfo', 'description']))

        yield l.load_item()

        # get owner vacancies
        for region_group in ['currentRegion', 'otherRegions']:
            prof_areas = self.get_json(data, ['vacanciesGroupsByRegion', region_group])
            if prof_areas is not None:
                if self.get_json(prof_areas, ['vacancies']) is None:
                    for prof_area in self.get_json(prof_areas, ['countsByProfArea']):

                        region_type = 'CURRENT'
                        if region_group != 'currentRegion':
                            region_type = 'OTHER'
                        prof_area_id = self.get_json(prof_area, ['profAreaId'])

                        owner_id = self.get_json(data, ['employerInfo', 'id'])

                        page_count = math.ceil(self.get_json(prof_area, ['count']) / 30)
                        for page in range(0, page_count):
                           yield response.follow(
                               f'https://hh.ru/shards/employerview/vacancies?page={page}&profArea={prof_area_id}&currentEmployerId={owner_id}&json=true&regionType={region_type}&disableBrowserCache=true',
                               callback=self.owner_vacancy_parse)
                else:
                    for vacancy in self.get_json(prof_areas, ['vacancies']):
                        yield response.follow(self.get_json(vacancy, ['links', 'desktop']), callback=self.vacancy_parse)

