SPIDER_MODULES = ['lessons.mining.lesson5.spiders']
NEWSPIDER_MODULE = 'lessons.mining.lesson5.spiders'

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
ROBOTSTXT_OBEY = False

LOG_ENABLED = True
LOG_LEVEL = 'DEBUG'  # INFO ERROR

ITEM_PIPELINES = {
    'lessons.mining.lesson5.pipeline.VacancyPipeline': 300
}
