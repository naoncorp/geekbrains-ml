from pymongo import MongoClient
import ssl

from lessons.mining.lesson5.items.vacancy import VacancyPageItem
from lessons.mining.lesson5.items.company import CompanyPageItem


class VacancyPipeline(object):
    def __init__(self):
        login = 'naoncorp'
        password = ''  # не скажу :)
        client = MongoClient(
            f"mongodb+srv://{login}:{password}@cluster0.hwukb.mongodb.net/test?retryWrites=true&w=majority", ssl=True, ssl_cert_reqs=ssl.CERT_NONE)
        self.db = client.test

    def process_item(self, item, spider):

        if isinstance(item, VacancyPageItem):
            collection = self.db.hh_vacancy
        elif isinstance(item, CompanyPageItem):
            collection = self.db.hh_company
        else:
            raise Exception("Неизвестный тип Item")

        collection.insert_one(item)
        return item

