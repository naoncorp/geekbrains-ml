from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings

from lessons.mining.lesson6 import settings
from lessons.mining.lesson6.spiders.instagram import InstagramSpider

# from jobparser.spiders.hhru import SjruSpider

if __name__ == '__main__':
	crawler_settings = Settings()
	crawler_settings.setmodule(settings)
	process = CrawlerProcess(settings=crawler_settings)
	process.crawl(InstagramSpider)
	# process.crawl(SjruSpider)
	process.start()
