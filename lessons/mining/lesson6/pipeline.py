import scrapy
from pymongo import MongoClient
import ssl
from scrapy.pipelines.images import ImagesPipeline
import json
from lessons.mining.lesson6.items.insta_page_item import InstaPageItem


class InstaPipeline(object):
    def __init__(self):
        login = 'naoncorp'
        password = ''  # не скажу :)
        client = MongoClient(
            f"mongodb+srv://{login}:{password}@cluster0.hwukb.mongodb.net/test?retryWrites=true&w=majority", ssl=True, ssl_cert_reqs=ssl.CERT_NONE)
        self.db = client.test

    def process_item(self, item, spider):
        collection = self.db.instagram

        collection.insert_one(item)
        return item


class InstaImagePipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        if isinstance(item, InstaPageItem):
            url = item['data']['display_url']
            try:
                yield scrapy.Request(url)
            except Exception as e:
                print(e)

    def item_completed(self, results, item, info):
        return item

