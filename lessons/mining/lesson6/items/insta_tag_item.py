import scrapy
from itemloaders.processors import Compose, MapCompose, Join, TakeFirst
from scrapy.loader import ItemLoader


class InstaTagItem(scrapy.Item):
    _id = scrapy.Field()
    date_parse = scrapy.Field()
    data = scrapy.Field()

    pass


class InstaTagItemLoader(ItemLoader):
    default_item_class = InstaTagItem
    default_output_processor = TakeFirst()
    date_parse = TakeFirst()
    data = TakeFirst()