import scrapy
from itemloaders.processors import Compose, MapCompose, Join, TakeFirst
from scrapy.loader import ItemLoader


class InstaPageItem(scrapy.Item):
    _id = scrapy.Field()
    date_parse = scrapy.Field()
    data = scrapy.Field()

    pass


class InstaPageItemLoader(ItemLoader):
    default_item_class = InstaPageItem
    default_output_processor = TakeFirst()
    date_parse = TakeFirst()
    data = TakeFirst()

