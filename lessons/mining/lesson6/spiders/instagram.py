from datetime import datetime
import math

import scrapy
from scrapy import FormRequest
from scrapy.http import HtmlResponse
from lessons.mining.lesson6.items.insta_page_item import InstaPageItemLoader
from lessons.mining.lesson6.items.insta_tag_item import InstaTagItemLoader
import urllib

import json


class InstagramSpider(scrapy.Spider):
    name = 'instagram'
    allowed_domains = ['instagram.com']
    start_urls = ['https://www.instagram.com/accounts/login/']
    login_url = 'https://www.instagram.com/accounts/login/ajax/'

    tags = [
        ['python', '9b498c08113f1e09617a1703c22b2f32']
    ]

    def get_json(self, item, selectors: []):
        it = item

        for p in selectors:
            try:
                it = it[p]
            except:
                return None

        return it

    def parse_next_tag_page(self, response, tag_name, tag_hash):
        data = json.loads(response.text)

        tag = self.get_json(data, ['data', 'hashtag'])
        edges = self.get_json(tag, ['edge_hashtag_to_media', 'edges'])

        for edge in edges:
            l = InstaPageItemLoader(selector=response)
            l.add_value('date_parse', datetime.now())
            l.add_value('data', self.get_json(edge, ['node']))
            yield l.load_item()

        if self.get_json(tag, ['edge_hashtag_to_media', 'page_info', 'has_next_page']):
            next_page_cursor = self.get_json(tag, ['edge_hashtag_to_media', 'page_info', 'end_cursor'])
            next_page = f'https://www.instagram.com/graphql/query/?query_hash={tag_hash}&variables={{"tag_name":"{tag_name}","first":2,"after":"{next_page_cursor}"}}'
            yield response.follow(next_page, callback=self.parse_next_tag_page,
                                  cb_kwargs=dict(tag_name=tag_name, tag_hash=tag_hash))

    def parse_tag_page(self, response, tag_name, tag_hash):
        str_data = response.xpath("//script[contains(text(), 'csrf')]/text()")[0]
        str_data = str_data.root.replace('window._sharedData = ', '')[:-1]
        data = json.loads(str_data)

        # save tag
        tag = self.get_json(data, ['entry_data', 'TagPage', 0, 'graphql', 'hashtag'])

        l = InstaTagItemLoader(selector=response)
        l.add_value('date_parse', datetime.now())
        l.add_value('data', tag)

        yield l.load_item()

        edges = self.get_json(tag, ['edge_hashtag_to_media', 'edges'])

        for edge in edges:
            l = InstaPageItemLoader(selector=response)
            l.add_value('date_parse', datetime.now())
            l.add_value('data', self.get_json(edge, ['node']))
            yield l.load_item()

        if self.get_json(tag, ['edge_hashtag_to_media', 'page_info', 'has_next_page']):
            next_page_cursor = self.get_json(tag, ['edge_hashtag_to_media', 'page_info', 'end_cursor'])
            next_page = f'https://www.instagram.com/graphql/query/?query_hash={tag_hash}&variables={{"tag_name":"{tag_name}","first":2,"after":"{next_page_cursor}"}}'
            yield response.follow(next_page, callback=self.parse_next_tag_page,
                                  cb_kwargs=dict(tag_name=tag_name, tag_hash=tag_hash))

    def after_login(self, response):
        for tg in self.tags:
            yield response.follow(f'https://www.instagram.com/explore/tags/{tg[0]}', callback=self.parse_tag_page,
                                  cb_kwargs=dict(tag_name=tg[0], tag_hash=tg[1]))

    def parse(self, response):
        # get token
        cookie_item = response.headers.getlist('Set-Cookie')[7].decode('UTF-8')
        csrf_token = cookie_item.split(';')[0].replace('csrftoken=', '')

        return [scrapy.FormRequest(self.login_url, method='POST',
                formdata={
                    'username': 'naoncorp@gmail.com',
                    'enc_password': '#PWD_INSTAGRAM_BROWSER:10:1604844474:AfBQAJ1nWUtXeNUFPglKQUeY2truvUQoCux+zceV36YeIwBD4F3efxO5h5YAHNsw88JbxGpY0LWKjCEcMBsveHzrtXVAnQZrwoF9FDG5wSFkMrzCL44kZ59CfHLZPMhAFAinaDH8bIxLOY2E1A=='
                },
                headers={'X-CSRFToken': csrf_token},
                callback=self.after_login)]





