from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, Session

from lessons.mining.lesson3.base import Base
from lessons.mining.lesson3.user import User
from lessons.mining.lesson3.post import Post

class Comment(Base):
    __tablename__ = 'comment'
    id = Column(Integer, primary_key=True)
    text = Column(String)
    owner_id = Column(Integer, ForeignKey(User.id))
    post_id = Column(Integer, ForeignKey(Post.id))

    user = relationship('User', back_populates="comments")
    post = relationship('Post', back_populates="comments")

    def __init__(self, text, owner_id, post_id):
        self.text = text
        self.owner_id = owner_id
        self.post_id = post_id

    def __repr__(self):
        return "<Comment('%s','%s','%s')>" % (self.text, self.owner_id, self.post_id)
