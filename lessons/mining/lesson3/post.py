from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, Session

from lessons.mining.lesson3.base import Base
from lessons.mining.lesson3.user import User


class Post(Base):
    __tablename__ = 'post'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    url = Column(String)
    image_url = Column(String)
    created_at = Column(DateTime)
    owner_id = Column(Integer, ForeignKey(User.id))

    user = relationship('User', back_populates="posts")
    comments = relationship('Comment', back_populates="post")
    tags = relationship('PostTag', back_populates="post")

    def __init__(self, title, url, image_url, created_at, owner_id):
        self.title = title
        self.url = url
        self.image_url = image_url
        self.created_at = created_at
        self.owner_id = owner_id

    def __repr__(self):
        return "<Post('%s','%s','%s','%s')>" % (self.title, self.url, self.image_url, self.created_at, self.owner_id)