from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, Session

from lessons.mining.lesson3.base import Base


# Сделал User вместо Writer, чтобы связать и с комментами
class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    image_url = Column(String)

    comments = relationship('Comment', back_populates="user")
    posts = relationship('Post', back_populates="user")

    def __init__(self, name, image_url):
        self.name = name
        self.image_url = image_url

    def __repr__(self):
        return "<User('%s','%s')>" % (self.name, self.image_url)
