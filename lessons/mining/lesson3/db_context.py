from sqlalchemy import create_engine
from sqlalchemy.orm import relationship, Session
from sqlalchemy.orm import sessionmaker

from lessons.mining.lesson3.base import Base


class DbContext:

    def __init__(self):
        engine = create_engine('sqlite:///posts.db', echo=True)
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self.__session = Session()

    def query(self, object):
        return self.__session.query(object)

    def add(self, object):
        self.__session.add(object)

    def flush(self):
        self.__session.flush()

    def save_changes(self):
        try:
            self.__session.commit()
        except:
            self.__session.rollback()
            raise Exception

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.__session.close()