from bs4 import BeautifulSoup as bs
import requests
import json
from datetime import datetime


from lessons.mining.lesson3.db_context import DbContext
from lessons.mining.lesson3.user import User
from lessons.mining.lesson3.post import Post
from lessons.mining.lesson3.comment import Comment
from lessons.mining.lesson3.tag import Tag
from lessons.mining.lesson3.post_tag import PostTag



class GeekBrainsParser:

    def __init__(self):
        self.base_url = 'https://geekbrains.ru'
        DbContext()

    def parse_date(self, str):
        str = str.replace('января', '1') \
            .replace('февраля', '2') \
            .replace('марта', '3') \
            .replace('апреля', '4') \
            .replace('мая', '5') \
            .replace('июня', '6') \
            .replace('июля', '7') \
            .replace('августа', '8') \
            .replace('сентября', '9') \
            .replace('октября', '10') \
            .replace('ноября', '11') \
            .replace('декабря', '12')

        return datetime.strptime(str, '%d %m %Y')

    def add_or_get_user(self, db, username, avatar_url):
        user = db.query(User).filter_by(name=username).first()

        if user is None:
            user = User(username, avatar_url)
            db.add(user)
            db.save_changes()

        return user

    def parse_and_save_user(self, soup, db):
        writer = soup.select('div[itemprop~=author]')[0].text
        writer_avatar = soup.select('.blogpost img.user-avatar-image:not(.gb__comment-item-user-avatar)')[0].attrs['src']

        user = self.add_or_get_user(db, writer, writer_avatar)
        return user

    def parse_and_save_post(self, soup, url, db, user):
        post_title = soup.select('article > h1')[0].text
        post_image_url = soup.select('article .content > p > img')

        if len(post_image_url) > 0:
            post_image_url = post_image_url[0].attrs['src']
        else:
            post_image_url = None

        created_at = self.parse_date(soup.select('time[itemprop~=datePublished]')[0].text)

        post = Post(post_title, f'{self.base_url}{url}', post_image_url, created_at, user.id)
        db.add(post)
        db.save_changes()

        return post

    def parse_and_save_tags(self, soup, db):
        all_tags = []

        tags = soup.select('.i-tag')
        if len(tags) > 0:
            tags = tags[0].attrs['keywords'].split(', ')

            for tg in tags:
                ex_tag = db.query(Tag).filter_by(name=tg).first()

                if ex_tag is None:
                    tag = Tag(tg)
                    db.add(tag)
                    db.save_changes()
                    all_tags.append(tag)
                else:
                    all_tags.append(ex_tag)

        return all_tags

    def parse_and_save_comments(self, soup, db, post):
        # get commentable id
        commentable_id = soup.select('comments')[0].attrs['commentable-id']

        response = requests.get(f'https://geekbrains.ru/api/v2/comments?commentable_type=Post&commentable_id={commentable_id}&order=desc').text
        comments = json.loads(response)

        all_comments = []

        for cm in comments:
            username = cm['comment']['user']['full_name']
            avatar_url = f"{self.base_url}{cm['comment']['user']['image']['url']}"
            user = self.add_or_get_user(db, username, avatar_url)

            comment_text = cm['comment']['body']
            comment_db = Comment(comment_text, user.id, post.id)
            db.add(comment_db)
            db.save_changes()
            all_comments.append(comment_db)

        return all_comments

    def parse(self):

        # get all posts urls
        page = 1
        while True:
            print(f'=======СТРАНИЦА {page}')
            response = requests.get(f'{self.base_url}/posts?page={page}').text
            soup = bs(response, 'html.parser')

            # get posts urls from page
            post_urls = soup.select("div[class~=post-item] > a")
            if len(post_urls) == 0:
                break

            for a in post_urls:
                url = a.attrs['href']

                # open post
                response = requests.get(f'{self.base_url}{url}').text
                soup = bs(response, 'html.parser')

                with DbContext() as db:
                    # add user
                    user = self.parse_and_save_user(soup, db)

                    # get&save post info
                    post = self.parse_and_save_post(soup, url, db, user)

                    # get&save tags
                    tags = self.parse_and_save_tags(soup, db)

                    # get&save comments
                    comments = self.parse_and_save_comments(soup, db, post)

                    # association table
                    for tg in tags:
                        pt = PostTag(post.id, tg.id)
                        db.add(pt)
                        db.save_changes()

            page = page + 1



gb_parser = GeekBrainsParser()
gb_parser.parse()