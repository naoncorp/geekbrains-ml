from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, Session

from lessons.mining.lesson3.base import Base


class Tag(Base):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    posts = relationship('PostTag', back_populates="tag")

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Tag('%s')>" % self.name
