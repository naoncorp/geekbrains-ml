from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship, Session

from lessons.mining.lesson3.base import Base
from lessons.mining.lesson3.post import Post
from lessons.mining.lesson3.tag import Tag


class PostTag(Base):
    __tablename__ = 'posttag'
    post_id = Column(Integer, ForeignKey(Post.id), primary_key=True)
    tag_id = Column(Integer, ForeignKey(Tag.id), primary_key=True)

    post = relationship('Post', back_populates="tags")
    tag = relationship('Tag', back_populates="posts")

    def __init__(self, post_id, tag_id):
        self.post_id = post_id
        self.tag_id = tag_id

    def __repr__(self):
        return "<PostTag('%s','%s')>" % (self.post_id, self.tag_id)
