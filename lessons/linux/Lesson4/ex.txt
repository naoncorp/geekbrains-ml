# 1. Создать пользователя user_new и предоставить ему права на редактирование файла с программой, выводящей на экран Hello, world!

chmod o+rw test.py

sudo useradd -m -s /bin/bash user_new

sudo passwd user_new

su user_new

# 2. Зайти под юзером user_new и с помощью редактора Vim поменять фразу в скрипте из пункта 1 на любую другую.

vim test.py
C
# chage text
CTRL+C
:wq

cat test.py

# 3.* Под юзером user_new зайти в его домашнюю директорию и создать программу на Python, выводящую в консоль цифры от 1 до 10 включительно с интервалом в 1 секунду.

cd ~
nano timer.py

# import time

# arr = range(1, 11)

# for item in arr:
#     time.sleep(1)
#     print(item)

python3 timer.py
